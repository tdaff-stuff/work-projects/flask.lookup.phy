"""
Lookup application wsgi script.

For plugging in to mod_wsgi with apache. A simple apache conf would
look like:

<VirtualHost *>
    ServerName server.name.com

    SetEnv LOOKUP_SETTINGS "/path/to/settings.cfg"
    SetEnv LOOKUP_SECRETS "/path/to/secrets.cfg"

    WSGIDaemonProcess lookup user=www group=www threads=8 python-home=/path/to/virtualenv
    WSGIScriptAlias / /path/to/lookup.wsgi

    <Directory /path/to/lookup/source/>
        WSGIProcessGroup lookup
        WSGIApplicationGroup %{GLOBAL}
        Require all granted
    </Directory>
</VirtualHost>
"""

import os


def application(request_environ, start_response):
    # Pass environment variables from Apache SetEnv to wsgi app
    if "LOOKUP_SETTINGS" in request_environ:
        os.environ["LOOKUP_SETTINGS"] = request_environ["LOOKUP_SETTINGS"]
    if "LOOKUP_SECRETS" in request_environ:
        os.environ["LOOKUP_SECRETS"] = request_environ["LOOKUP_SECRETS"]

    # Must be imported after the environment setting
    # otherwise it doesn't see changes
    from lookup.http.app import create_app

    return create_app()(request_environ, start_response)
