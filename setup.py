"""
Install information.
"""

from setuptools import setup, find_packages

IBISCLIENT_SRC = (
    "git+https://gitlab.developers.cam.ac.uk/uis/sysdev/devgroup/ibis/ibis-client.git"
)

setup(
    name="flask.lookup",
    version="3.3.1",
    description="lookup.phy",
    long_description=open("README.rst").read(),
    author="Tom Daff",
    author_email="tdd20@cam.ac.uk",
    license="BSD",
    url="lookup.phy.cam.ac.uk",
    packages=find_packages(exclude=["tests"]),
    entry_points={"console_scripts": ["lookup = lookup.__main__:main"]},
    package_data={
        "lookup": [
            "http/templates/*",
            "http/static/*/*",
            "model/migrations/*",
            "model/migrations/versions/*.py",
        ]
    },
    install_requires=[
        "alembic",
        "celery<5",
        "flask",
        "gevent",
        "ibisclient @ {}".format(IBISCLIENT_SRC),
        "pillow",
        "psycopg2",
        "requests",
        "sqlalchemy",
        "werkzeug",
    ],
    scripts=["lookup.wsgi"],
    extras_require={
        "test": ["pytest", "pytest-flask", "pytest_postgresql"],
        "docs": ["sphinx"],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
    ],
)
