"""
Utilities for working with geometric data.
"""


def midpoint(polygon):
    """
    Find some point near the middle of the polygon. Calculates the midpoints
    of the extents in each direction.

    GeoJSON does not allow crossing the antimeridian so should not
    wrap the position and does not check if it does anyway.

    Parameters
    ----------
    polygon: list of list of [float, float]
        Array of LinearRing coordinates.

    Returns
    -------
    longitude, latitude: float, float
        Somewhere in the middle of the polygon.
    """
    # in GeoJSON first LinearRing is outside and the rest are holes
    # so they can be ignored
    longitudes = [point[0] for point in polygon[0]]
    latitudes = [point[1] for point in polygon[0]]
    return (
        (max(longitudes) + min(longitudes)) / 2,
        (max(latitudes) + min(latitudes)) / 2,
    )
