"""
Util functions for dealing with web data.
"""
import re
from typing import Optional, List, Dict, Any

from flask import request, g, current_app
from flask import Request
from lookup.config import defaults, Site

from lookup.util.text import string_to_bool


def get_remote_user(req: Request = request) -> Optional[str]:
    """
    Determine the currently logged in Apache user from the request.

    Looks at the "REMOTE_USER" environment variable set by Apache for
    a logged in user. Failing that, it will look for the ``A-AAPrincipal``
    added by Raven (if configured, ignores any hash).

    Parameters
    ----------
    req
        A request object that has headers instance attached. Will default
        to the Flask request from the current context.

    Returns
    -------
    remote_user
        The name of the user or None if no user can be determined.

    """

    if "REMOTE_USER" in req.environ:
        # Raven sets the REMOTE_USER in the request environment
        remote_user = req.environ.get("REMOTE_USER")
    elif "X-AAPrincipal" in req.headers:
        # raven default for AAHeaders
        # crypto hash, single space, value
        remote_user = req.headers["X-AAPrincipal"].split(" ", 1)[-1]
    else:
        remote_user = None

    return remote_user


def get_common_args(req: Request = request, **defaults) -> Dict[str, Any]:
    """
    Parse common arguments from a request, i.e. query parameters that
    are used in most views.

    Return as a dictionary with all the values that have been found and
    convert them to the correct type. Unspecified values will be set to None.
    Values that cannot be converted to the correct type will be ignored.
    Booleans are True for any value that does not appear to be 'false'.

    Parameters
    ----------
    req
        A request object that contains response arguments.
    defaults: Any
        Provide default values for any parameters. These will otherwise be
        set to None.

    Returns
    -------
    parsed_args: dict
        All arguments as parsed.
    """
    known_arg_names = {
        "str": ["query", "sort"],
        "int": ["limit", "offset"],
        "bool": ["full", "recursive", "fuzzy"],
    }

    parsed_args = {}

    for arg_name in known_arg_names["str"]:
        if req.args.get(arg_name) is not None:
            parsed_args[arg_name] = req.args.get(arg_name)
        elif defaults.get(arg_name) is not None:
            parsed_args[arg_name] = defaults[arg_name]
        else:
            parsed_args[arg_name] = None

    for arg_name in known_arg_names["int"]:
        try:
            parsed_args[arg_name] = int(req.args.get(arg_name))
        except (ValueError, TypeError):  # invalid and None
            try:
                parsed_args[arg_name] = int(defaults[arg_name])
            except (KeyError, ValueError, TypeError):
                parsed_args[arg_name] = None

    for arg_name in known_arg_names["bool"]:
        if req.args.get(arg_name) is not None:
            parsed_args[arg_name] = string_to_bool(req.args.get(arg_name))
        elif defaults.get(arg_name) is not None:
            parsed_args[arg_name] = string_to_bool(defaults[arg_name])
        else:
            parsed_args[arg_name] = None

    return parsed_args


def get_current_site() -> Site:
    """
    Get the active site for the current interaction.

    In a request, the Site is determined by the hostname used to access
    the page. It will be set, before a request, on g, so is always
    available in templates, but lower level Api interactions will
    not necessarily have a request context. This function ensures
    that they can have a usable Site object on demand.

    Returns
    -------
    site
        The site for the active hostname, or the default for the package.
    """
    # Not in an application so send the application default
    if not current_app:
        return defaults.SITES["default"]

    # Not in a request, so send the default from config
    if not request:
        return current_app.config["SITES"]["default"]

    # In a request, set on g if it's not already set
    if not hasattr(g, "site"):
        g.site = current_app.config["SITES"][request.host]

    return g.site


def scope_institutions(scope: str) -> Optional[List[str]]:
    """
    Parse the scope argument and produce the appropriate list
    of institutions.

    Valid scopes are:
    * unset = local institution (full list)
    * local = local institution (full list)
    * all = all institutions
    * LIST,OF,INST = exact list of specific institutions (no children)

    Parameters
    ----------
    scope
        The text string obtained from the scope argument or cookie.

    Returns
    -------
    institutions
        If a scope is found, return the list of institution instid
        representing that scope
    """
    if scope == "all":
        return None
    elif scope in ["local", ""]:
        return current_app.config["SITES"][request.host].institutions
    else:
        return re.findall(r"([\w]+)", scope)


def get_current_institutions() -> Optional[List[str]]:
    """
    Get the institutional scope for a specific request.

    Used to determine if results should be limited to the current
    institution or show results relative to the entire database.
    Set by the scope parameter of a request.

    Returns
    -------
    institutions
        A list of institution instid to use to limit queries.
    """
    # Not in an application so no way to try anything
    if not current_app:
        return None

    # Not in a request, so send the default from config
    if not request:
        return current_app.config["SITES"]["default"].institutions

    # List is already constructed, so send it
    if hasattr(g, "current_institutions"):
        return g.current_institutions

    # Required institutions can be set for an individual request using args,
    # a session, by using cookies, or will fall back to the default based
    # on the hostname being used to access the site
    if "scope" in request.args:
        institutions = scope_institutions(request.args["scope"])
    elif "scope" in request.cookies:
        institutions = scope_institutions(request.cookies["scope"])
    else:
        institutions = scope_institutions("local")

    # Empty list case also gives all institution scope
    # Make available for the lifetime of the request
    g.current_institutions = institutions or None

    return g.current_institutions


def clean_buildings(buildings: List[str]) -> List[str]:
    """
    Remove duplicates from the list and move External at the end if present.

    Parameters
    ----------
    buildings
        The raw list of building names.

    Returns
    -------
    clean
        The cleaned list of buildings, wth no duplicates and "External"
        at the end, if present in the initial list.
    """
    clean: List[str] = []

    for building in buildings:
        if building not in clean and building != "External":
            clean.append(building)

    if "External" in buildings:
        clean.append("External")

    return clean


def scope_buildings(scope: str) -> List[str]:
    """
    Parse the scope argument and produce the appropriate list
    of buildings.

    Valid scopes are:
    * unset = local site buildings (full list)
    * local = local site buildings (full list)
    * all = all known buildings
    * LIST,OF,INST = buildings for any sites that match the buildings

    Parameters
    ----------
    scope
        The text string obtained from the scope argument or cookie.

    Returns
    -------
    buildings
        If a scope is found, return the list of institution instid
        representing that scope
    """
    if scope == "all":
        # Buildings for all sites, grouped by institution
        return clean_buildings(
            [
                building
                for site in sorted(current_app.config["SITES"].all())
                for building in site.buildings
            ]
        )
    elif scope in ["local", ""]:
        # Buildings for the viewing institution
        return current_app.config["SITES"][request.host].buildings
    else:
        # Combine the buildings for any site that includes any
        # institution in the current scope. Fall back to the
        # local list
        institutions = set(re.findall(r"([\w]+)", scope))
        return (
            clean_buildings(
                [
                    building
                    for site in current_app.config["SITES"].all()
                    for building in site.buildings
                    if not institutions.isdisjoint(site.institutions)
                ]
            )
            or current_app.config["SITES"][request.host].buildings
        )


def get_current_buildings() -> Optional[List[str]]:
    """
    Get the list of buildings that are relevant to the current scope.

    Will be buildings for the local institution or the list of all buildings.

    Returns
    -------
    buildings
        Names of all required buildings
    """
    # Not in an application so no way to try anything
    if not current_app:
        return None

    # Not in a request, so send the default from config
    if not request:
        return current_app.config["SITES"]["default"].buildings

    # List is already constructed, so send it
    if hasattr(g, "current_buildings"):
        return g.current_buildings

    # Building list will try and match the scope, if scope is set and
    # there is a suitable Site, otherwise show everything.
    if "scope" in request.args:
        buildings = scope_buildings(request.args["scope"])
    elif "scope" in request.cookies:
        buildings = scope_buildings(request.cookies["scope"])
    else:
        buildings = scope_buildings("local")

    # Building list should never be empty
    g.current_buildings = buildings

    return g.current_buildings


def generate_pages(meta, scheme="bisection", limits=None, sorts=None):
    """Generate a list of 1-indexed page numbers and their
    associated offsets based on the metadata passed in.

    Parameters
    ----------
    meta: dict
        Metadata store with keys for 'offset', 'total', 'limit'.
        Will also add 'name' or 'query' if those are used.
    scheme: str
        How to generate the distribution of pages.
    limits: list of int
        A list of values that can be selected as the limit per page.
        Put into the 'options' dict in the returned data.
    sorts: list of tuple
        A list of ('sort_key', 'Sort Description') for valid values
        for sorting the data. Put into the options dict of the
        returned data.
    """

    query = meta.get("query")
    name = meta.get("name")
    identifier = meta.get("identifier")
    fuzzy = meta.get("fuzzy", False)
    sort = meta.get("sort")
    offset = meta.get("offset", 0)
    total = meta.get("total")
    limit = meta.get("limit", 0)

    # calculate pages here since no max function in jinja2
    pagination = {
        "limit": limit,
        "offset": offset,
        "query": query,
        "fuzzy": fuzzy,
        "name": name,
        "identifier": identifier,
        "sort": sort,
        "options": {
            # Can select per page
            "limits": limits or [],
            # Available sorting options
            "sorts": sorts or [],
        },
    }

    if not limit:
        pagination["first"] = 0
        pagination["all_pages"] = [(1, 0)]
        return pagination  # can't paginate with no limit

    if total is None:  # add some extra pages, since we don't know the limit
        total = offset + limit + 1
    elif total == 0:
        pagination["all_pages"] = [(1, 0)]
        return pagination  # no results so only one blank page

    if offset != 0:
        # back one page or to beginning
        pagination["first"] = 0
        pagination["prev"] = max(offset - limit, 0)

    if offset + limit < total:
        # next page
        pagination["next"] = offset + limit
        pagination["last"] = limit * ((total - 1) // limit)  # complete pages

    current_page = offset // limit
    total_pages = (total - 1) // limit

    if not offset % limit:  # exactly on a page?
        pagination["active_page"] = current_page + 1  # pages are 1-based

    if scheme == "all":
        all_pages = list(range(total_pages + 1))
    elif scheme == "compact":
        all_pages = [current_page]
    elif scheme == "bisection":
        # bisect both sides
        all_pages = [0, current_page, total_pages]

        for start_page in [0, total_pages]:
            this_page = start_page
            while True:
                this_page = current_page + (this_page - current_page) // 2
                if this_page in all_pages:
                    break
                all_pages.append(this_page)
    else:
        raise ValueError("Unknown pagination scheme {}".format(scheme))

    pagination["all_pages"] = [
        (page + 1, page * limit) for page in sorted(set(all_pages))
    ]

    return pagination


def pagination_args(mapping, **kwargs):
    """Create a new mapping including only the arguments from the
    input mapping that have values. Used to make cleaner URLs in the
    pagination template by excluding unset or irrelevant values.

    Extra key=value pairs in the kwargs will override anything in
    the mapping.

    Processed keys are:
        ['name', 'identifier', 'query', 'offset', 'limit', 'fuzzy', 'sort']

    Parameters
    ==========
    mapping: dict
        A dict of items used for generating pagination.
    kwargs: dict of any
        Any items that will be used to overwrite values in the mapping.

    Returns
    =======
    args: dict
        A dict containing the minimal set of keys for pagination
        in the Jinja2 template.
    """

    keys = ["name", "identifier", "query", "offset", "limit", "fuzzy", "sort"]

    args = {}

    # Priority for kwargs so that negative values can
    # replace or remove args
    for key in keys:
        if key in kwargs:
            if kwargs[key] is not None and kwargs[key] is not False:
                args[key] = kwargs[key]
        elif key in mapping:
            if mapping[key] is not None and mapping[key] is not False:
                args[key] = mapping[key]

    return args
