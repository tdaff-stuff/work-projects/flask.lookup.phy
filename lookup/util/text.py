"""
Text manipulation functions.
"""


def clean_string(text: str) -> str:
    """
    Strip a string and remove any duplicate whitespaces.

    Parameters
    ----------
    text
        Text to clean.

    Returns
    -------
    clean
        The string with any leading or trailing whitespace removed
        and any duplicated whitespace removed.
    """
    return " ".join(text.split())


def string_to_bool(text, strict=False):
    """
    Interpret a string as a boolean value.

    Parameters
    ----------
    text: str
        A text string to be converted to a boolean value
    strict: bool
        If True, will raise a value error for values that
        cannot be converted, otherwise will return True
        for non-empty strings that cannot be recognised as
        False.

    Returns
    -------
    bool

    Raises
    ------
    ValueError
        The text cannot be interpreted as a boolean.
    """

    true_values = ["1", "yes", "true", "t", "on", True, 1]
    false_values = ["0", "no", "false", "f", "off", False, 0]

    if hasattr(text, "lower"):
        ltext = text.lower()
    else:
        ltext = text

    if ltext in true_values:
        return True
    elif ltext in false_values:
        return False
    elif strict:
        raise ValueError(text)
    else:
        return bool(text)
