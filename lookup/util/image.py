"""
Utilities for manipulating images
"""

import base64
from io import BytesIO

from PIL import Image


def process_user_image(img_b64, max_size=(800, 800), thumb_size=(48, 48)):
    """
    Convert a user submitted image into a reasonable sized one,
    plus a thumbnail.

    Parameters
    ==========
    img_b64: str
        The uploaded image encoded in base64 (i.e. as it is
        received from the browser).
    max_size:
        Resize the image so neither dimension exceeds these values.
    thumb_size:
        Dimensions of the thumbnail to be created.


    Returns
    =======
    img_main_b64, img_thumb_b64: str, str
        The original image, resized if required and the thumbnail,
        as base64 encoded jpeg.
    """

    if not img_b64:
        return None, None

    # load into PIL, RBG allows PNG -> JPG conversion
    im_main = Image.open(BytesIO(base64.b64decode(img_b64))).convert("RGB")

    # Create copy for thumbnail before in-place manipulation
    im_thumb = im_main.copy()

    # resize to not exceed the max dimension,
    # no change if it's already within size limits
    if max_size is not None and any(
        [im_main.size[0] > max_size[0], im_main.size[1] > max_size[1]]
    ):
        im_main.thumbnail(max_size)
        with BytesIO() as img_out_data:
            im_main.save(img_out_data, format="jpeg", quality=85, optimize=True)
            img_main_b64 = base64.b64encode(img_out_data.getvalue())
    else:
        # Don't re-encode if it's fine
        img_main_b64 = img_b64

    # repeat for thumbnail
    if thumb_size is not None:
        im_thumb.thumbnail(thumb_size)

    with BytesIO() as img_out_data:
        im_thumb.save(img_out_data, format="jpeg", quality=85, optimize=True)
        img_thumb_b64 = base64.b64encode(img_out_data.getvalue())

    return img_main_b64, img_thumb_b64
