"""
Entry point to run the flask app.

"""

from lookup.http.app import create_app


# Have main handle any extra arguments from
# environment at runtime
def main():
    """Run the app."""
    app = create_app()
    app.run()


if __name__ == "__main__":
    main()
