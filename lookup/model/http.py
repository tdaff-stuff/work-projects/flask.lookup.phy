"""
Web specific data storage.
"""

from typing import Optional, List

from sqlalchemy import Column, func
from sqlalchemy import String, DateTime
from werkzeug.useragents import UserAgent

from lookup.model import Base
from lookup.model.ext import ToDictMixin


class UserSession(Base, ToDictMixin):
    """
    A client session associated with a web request. Creates a
    session/key -> username association that can be expired
    and tracked (unlike flask session cookies).
    """

    __tablename__ = "user_sessions"

    key = Column(String, primary_key=True)
    user_id = Column(String, nullable=False)
    session_type = Column(String)
    created_agent = Column(String)
    created_address = Column(String)
    created_datetime = Column(DateTime, server_default=func.now())
    accessed_agent = Column(String)
    accessed_address = Column(String)
    accessed_datetime = Column(DateTime, server_default=func.now())
    details = Column(String)

    def to_dict(self, max_depth: int = 1, exclude: Optional[List[str]] = None) -> dict:
        """
        Return a representation of the session with only serializable
        types (i.e. datetime converted to timestamp). Parameters are
        passed to ``lookup.model.ext.ToDictMixin``.

        Parameters
        ----------
        max_depth
            Number of levels to descend into relationships.
        exclude
            List of properties to ignore.

        Returns
        -------
        as_dict
            This session as a ``dict``.

        """
        # Use the standard method
        as_dict = super(UserSession, self).to_dict(max_depth, exclude)
        # convert datetime into something that works with json
        as_dict["accessed_datetime"] = as_dict["accessed_datetime"].timestamp()
        as_dict["created_datetime"] = as_dict["created_datetime"].timestamp()
        return as_dict

    def to_web_format(self) -> dict:
        """
        Make a version of the session with subset of the properties
        and user agent parsed by werkzeug.

        Returns
        -------
        as_web_format
            Representation of the session for use in jinja2

        """
        return {
            "key": self.key,
            "user_id": self.user_id,
            "agent": UserAgent(self.accessed_agent),
            "created": self.created_datetime,
            "active": self.accessed_datetime,
        }
