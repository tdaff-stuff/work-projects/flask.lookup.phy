"""
Models for storage of person data.

Defined in this module is the sqlalchemy Base class from which
models are derived. Subclass this to ensure that the schema is
complete.

"""

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
