"""
Person specification for lookup.phy database.

Person needs essential information but should be flexible enough
to include a range of useful information not included in other
sources (for example lookup.cam)

Each person includes:
- basic information from lookup.cam
- room numbers
- tags
- managerial relationships

"""
from datetime import datetime
from typing import Optional, Union, Dict, Any, List
from urllib.parse import urlsplit, urlunsplit

from sqlalchemy import Table, Column, event
from sqlalchemy import ForeignKey
from sqlalchemy import and_, not_
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, synonym, Session, validates
from sqlalchemy.orm.attributes import Event
from sqlalchemy.orm.state import InstanceState
from sqlalchemy.sql.functions import func
from sqlalchemy.types import Integer, String, Boolean, Text, Float, DateTime

from lookup.model import Base
from lookup.model.ext import DictType, ToDictMixin, B64Binary
from lookup.tasks.database import housekeeping


# associations via secondary tables for many-to-many relationships
# One person can be assigned to several rooms;
# rooms can have multiple people
room_associations = Table(
    "room_associations",
    Base.metadata,
    Column("person_identifier", String, ForeignKey("people.identifier")),
    Column("room_identifier", Integer, ForeignKey("rooms.identifier")),
)

# One person has many self assigned tags;
# people share common tags
tag_associations = Table(
    "tag_associations",
    Base.metadata,
    Column("person_identifier", String, ForeignKey("people.identifier")),
    Column("tag_identifier", Integer, ForeignKey("tags.identifier")),
)

# Managerial relationships
management_associations = Table(
    "management_associations",
    Base.metadata,
    Column("subordinate_identifier", String, ForeignKey("people.identifier")),
    Column("line_manager_identifier", String, ForeignKey("people.identifier")),
)

# people can be staff and student
status_associations = Table(
    "status_associations",
    Base.metadata,
    Column("person_identifier", String, ForeignKey("people.identifier")),
    Column("status_name", String, ForeignKey("statuses.name")),
)

# People can be members of several institutions
institution_associations = Table(
    "institution_associations",
    Base.metadata,
    Column("institution_identifier", String, ForeignKey("institutions.identifier")),
    Column("person_identifier", String, ForeignKey("people.identifier")),
)


def subordinates_recursive(self, found=None):
    """
    Find all people that report directly or indirectly to a line manager.

    Parameters
    ----------
    self: Person
        Query this person for all subordinates.
    found: list or None
        Add subordinates to the list, or create an empty list.

    Returns
    -------
    found: list of Person
        All subordinates and their subordinates filled in recursively.

    """
    if found is None:
        found = []  # Mutable container passes to invocations
    for subordinate in self.subordinates:  # stop at leaf nodes
        if subordinate not in found:  # stop if cyclic
            found.append(subordinate)
            subordinates_recursive(subordinate, found)  # pass state

    return found


def line_managers_recursive(self, found=None):
    """
    Find all people that are considered managers directly or indirectly.

    Parameters
    ----------
    self: Person
        Query this person for all managers.
    found: list or None
        Add managers to the list, or create an empty list.

    Returns
    -------
    found: list of Person
        All managers and their managers filled in recursively.

    """
    if found is None:
        found = []  # Mutable container passes to invocations
    for line_manager in self.line_managers:  # stop at leaf nodes
        if line_manager not in found:  # stop if cyclic
            found.append(line_manager)
            line_managers_recursive(line_manager, found)  # pass state

    return found


# Define models for the data
class Person(Base, ToDictMixin):
    """
    A person within the University of Cambridge, Department of Physics.

    Some fields carried from lookup.cam
    Additional fields include room numbers and self assigned tags.

    """

    __tablename__ = "people"

    # field names are all snake case, so some will not
    # directly map to lookup.cam
    identifier = Column(String, primary_key=True)
    crsid = synonym("identifier")  # crsid is the identifier, the Raven way
    display_name = Column(String)  # name as desired
    pronouns = Column(String)  # no equivalent in .cam
    headline = Column(String)  # Short description that appears in search
    profile = Column(Text)  # long format description
    registered_name = Column(String)  # registeredName in lookup
    first_name = Column(String)  # firstName attribute in lookup
    surname = Column(String)
    hidden = Column(Boolean, nullable=False, default=False, server_default="False")
    # In lookup, gives displayName or registeredName or CRSid
    # depending on visibility
    visible_name = Column(String)
    cancelled = Column(Boolean, nullable=False, default=False, server_default="False")
    # extra fields
    photo = Column(B64Binary)  # jpeg binary data
    thumbnail = Column(B64Binary)  # tiny image
    #
    # Custom fields not part of central lookup
    #
    # The availability is like a "status" field, but that attr name
    # is already taken. Consists of an indicator, a free-form text field
    # and an expiry.
    availability_indicator = Column(String)
    availability_text = Column(String)
    availability_expiry = Column(DateTime)
    # Structured data stores - dictionary data
    # Lookup API data used as source data
    lookup_cam = Column(DictType, nullable=False, default=dict, server_default="{}")
    # Access control lists
    acl = Column(DictType, nullable=False, default=dict, server_default="{}")
    # store links to services and data
    linked_services = Column(
        DictType, nullable=False, default=dict, server_default="{}"
    )

    # missing values
    # identifiers
    # attributes
    # institutions
    # groups
    # directGroups

    #
    # One to many fields
    #
    # Since these are only person -> multivalue field relationships
    # don't need to filter "hidden" and "cancelled"
    email_addresses: List["EmailAddress"] = relationship(
        "EmailAddress",
        back_populates="person",
        cascade="all, delete-orphan",
        uselist=True,
    )

    telephone_numbers: List["TelephoneNumber"] = relationship(
        "TelephoneNumber",
        back_populates="person",
        cascade="all, delete-orphan",
        uselist=True,
    )

    titles: List["Title"] = relationship(
        "Title", back_populates="person", cascade="all, delete-orphan", uselist=True
    )

    addresses: List["Address"] = relationship(
        "Address", back_populates="person", cascade="all, delete-orphan", uselist=True
    )

    urls: List["URL"] = relationship(
        "URL", back_populates="person", cascade="all, delete-orphan", uselist=True
    )

    #
    # Many to many with bidirectional relationship
    #
    # Hidden and cancelled people can have these, but use ensure that
    # they are filtered from the relationship on the other side
    rooms = relationship("Room", secondary=room_associations, back_populates="people")

    tags = relationship(
        "Tag", secondary=tag_associations, back_populates="people", uselist=True
    )

    institutions = relationship(
        "Institution",
        secondary=institution_associations,
        back_populates="members",
        uselist=True,
    )

    #
    # Management hierarchy
    #
    # Self referential property that can be cyclic so the
    # joins need to be set up manually
    line_managers = relationship(
        "Person",
        secondary=management_associations,
        primaryjoin=(identifier == management_associations.c.subordinate_identifier),
        secondaryjoin=(
            and_(
                identifier == management_associations.c.line_manager_identifier,
                not_(cancelled),
                not_(hidden),
            )
        ),
        back_populates="subordinates",
    )

    subordinates = relationship(
        "Person",
        secondary=management_associations,
        primaryjoin=(identifier == management_associations.c.line_manager_identifier),
        secondaryjoin=(
            and_(
                identifier == management_associations.c.subordinate_identifier,
                not_(cancelled),
                not_(hidden),
            )
        ),
        back_populates="line_managers",
    )

    # staff or student
    status = relationship(
        "Status", secondary=status_associations, back_populates="people"
    )

    def __init__(self, crsid):
        """Initialise an empty person. Fill up with data from API."""
        self.identifier = crsid
        self.lookup_cam = {}
        self.acl = {}
        self.linked_services = {}

    def __repr__(self):
        return "<Person('{}')>".format(self.identifier)

    @hybrid_property
    def availability(self) -> Optional[Dict[str, Union[Optional[str], datetime]]]:
        """
        Combined availability property.

        Returns
        -------
        availability
            Dictionary with the availability properties, or None
            if availability is unset or has expired.
        """
        # Not set, or manually expired
        if not self.availability_expiry:
            return None

        # Expired in the past
        if self.availability_expiry < datetime.now():
            return None

        # all properties returned as a dict
        return {
            "indicator": self.availability_indicator,
            "text": self.availability_text,
            "expiry": self.availability_expiry,
        }

    subordinates_recursive = property(subordinates_recursive)
    line_managers_recursive = property(line_managers_recursive)


#
# Many-to-many properties
#
# Can be associated with multiple people. Ensure that we filter
# hidden and cancelled people at the join level
class Room(Base, ToDictMixin):
    """
    A room within the Cavendish.

    Can be expressed as a room number with an optional building or
    description.

    Positioning data can be added from maps to make it easier to find
    on them.
    """

    __tablename__ = "rooms"

    identifier = Column(Integer, primary_key=True)
    number = Column(String)
    building = Column(String)
    description = Column(String)
    # location data for interactive map
    floor = Column(String)  # use string for non numerical floors
    longitude = Column(Float)
    latitude = Column(Float)
    # filter hidden and cancelled from back referenced relationships
    people = relationship(
        "Person",
        secondary=room_associations,
        secondaryjoin=and_(
            room_associations.c.person_identifier == Person.identifier,
            Person.cancelled.isnot(True),
            Person.hidden.isnot(True),
        ),
        back_populates="rooms",
        uselist=True,
    )

    def __init__(
        self,
        number=None,
        building=None,
        description=None,
        floor=None,
        longitude=None,
        latitude=None,
    ):
        self.number = number
        self.building = building
        self.description = description
        self.floor = floor
        self.longitude = longitude
        self.latitude = latitude

    def __repr__(self):
        return (
            "Room({!r}, building={!r}, description={!r}, "
            "floor={!r}, longitude={!r}, latitude={!r})".format(
                self.number,
                self.building,
                self.description,
                self.floor,
                self.longitude,
                self.latitude,
            )
        )


class Tag(Base, ToDictMixin):
    """
    A tag that represents a skill or other identifying trait.
    Description is optional.
    """

    __tablename__ = "tags"

    identifier = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    description = Column(String)
    # filter hidden and cancelled from back referenced relationships
    people = relationship(
        "Person",
        secondary=tag_associations,
        secondaryjoin=and_(
            tag_associations.c.person_identifier == Person.identifier,
            Person.cancelled.isnot(True),
            Person.hidden.isnot(True),
        ),
        back_populates="tags",
        uselist=True,
    )
    # Store the pre-counted number of people and the date it was updated
    # to help keep sorting and trending simple
    people_count = Column(Integer, nullable=False, default=0, server_default="0")
    modified = Column(
        DateTime,
        default=datetime.utcnow,
        server_default=func.now(),
        onupdate=datetime.utcnow,
    )

    def __init__(self, name, description=None):
        self.name = name
        self.description = description

    def __repr__(self):
        return "Tag({!r}, description={!r})".format(self.name, self.description)


class Status(Base, ToDictMixin):
    """A staff or student status."""

    __tablename__ = "statuses"

    name = Column(String, primary_key=True)
    # filter hidden and cancelled from back referenced relationships
    people = relationship(
        "Person",
        secondary=status_associations,
        secondaryjoin=and_(
            status_associations.c.person_identifier == Person.identifier,
            Person.cancelled.isnot(True),
            Person.hidden.isnot(True),
        ),
        back_populates="status",
    )

    def __init__(self, name: str) -> None:
        self.name = name

    def __repr__(self) -> str:
        return "Status({!r})".format(self.name)


#
# One-to-many properties
#
# Basically lists
#
class EmailAddress(Base, ToDictMixin):
    """An email address"""

    __tablename__ = "email_addresses"

    identifier = Column(Integer, primary_key=True)
    address = Column(String)
    value = synonym("address")
    description = Column(String)
    person_identifier = Column(String, ForeignKey("people.identifier"))
    person = relationship("Person", back_populates="email_addresses")

    def __init__(
        self, address: Optional[str] = None, description: Optional[str] = None
    ) -> None:
        self.address = address
        self.description = description

    def update(
        self,
        address: Optional[str] = None,
        value: Optional[str] = None,
        description: Optional[str] = None,
        **_kwargs: Any
    ) -> None:
        """Modify this entry with updated values."""
        if value is not None:
            self.value = value
        if address is not None:
            self.address = address
        if description is not None:
            self.description = description


class TelephoneNumber(Base, ToDictMixin):
    """A telephone number with a described purpose."""

    __tablename__ = "telephone_numbers"

    identifier = Column(Integer, primary_key=True)
    number = Column(String)
    value = synonym("number")
    description = Column(String)
    person_identifier = Column(String, ForeignKey("people.identifier"))
    person = relationship("Person", back_populates="telephone_numbers")

    def __init__(
        self, number: Optional[str] = None, description: Optional[str] = None
    ) -> None:
        self.number = number
        self.description = description

    def update(
        self,
        number: Optional[str] = None,
        value: Optional[str] = None,
        description: Optional[str] = None,
        **_kwargs: Any
    ) -> None:
        """Modify this entry with updated values."""
        if value is not None:
            self.value = value
        if number is not None:
            self.number = number
        if description is not None:
            self.description = description


class Title(Base, ToDictMixin):
    """A generic job title or other designation."""

    __tablename__ = "titles"

    identifier = Column(Integer, primary_key=True)
    text = Column(String)
    value = synonym("text")
    person_identifier = Column(String, ForeignKey("people.identifier"))
    person = relationship("Person", back_populates="titles")

    def __init__(self, text: Optional[str] = None, *_args: Any, **_kwargs: Any) -> None:
        self.text = text

    def update(
        self, text: Optional[str] = None, value: Optional[str] = None, **_kwargs: Any
    ) -> None:
        """Modify this entry with updated values."""
        if value is not None:
            self.value = value
        if text is not None:
            self.text = text


class Address(Base, ToDictMixin):
    """Standard address field, text with newlines allowed."""

    __tablename__ = "addresses"

    identifier = Column(Integer, primary_key=True)
    address = Column(String)
    value = synonym("address")
    description = Column(String)
    person_identifier = Column(String, ForeignKey("people.identifier"))
    person = relationship("Person", back_populates="addresses")

    def __init__(
        self, address: Optional[str] = None, description: Optional[str] = None
    ) -> None:
        self.address = address
        self.description = description

    def update(
        self,
        address: Optional[str] = None,
        value: Optional[str] = None,
        description: Optional[str] = None,
        **_kwargs: Any
    ) -> None:
        """Modify this entry with updated values."""
        if value is not None:
            self.value = value
        if address is not None:
            self.address = address
        if description is not None:
            self.description = description


class URL(Base, ToDictMixin):
    """A URL with a description."""

    __tablename__ = "urls"

    identifier = Column(Integer, primary_key=True)
    url = Column(String)
    value = synonym("url")
    description = Column(String)
    person_identifier = Column(String, ForeignKey("people.identifier"))
    person = relationship("Person", back_populates="urls")

    def __init__(
        self, url: Optional[str] = None, description: Optional[str] = None
    ) -> None:
        self.url = url
        self.description = description

    @validates("url", "value")
    def check_url(self, key: str, url: Optional[str]) -> Optional[str]:
        """If a 'url' is set, check that it's an http-type web address"""
        # initialised with no value set
        if url is None:
            return None
        # Require that a URL is http(s) and minimally has a location
        components = urlsplit(url)
        if not all([components.scheme, components.netloc]):
            raise ValueError("Invalid web address: {}".format(url))
        if components.scheme not in ["http", "https"]:
            raise ValueError("Invalid URL scheme: {}".format(url))
        return urlunsplit(components)

    def update(
        self,
        url: Optional[str] = None,
        value: Optional[str] = None,
        description: Optional[str] = None,
        **_kwargs: Any
    ) -> None:
        """Modify this entry with updated values."""
        if value is not None:
            self.value = value
        if url is not None:
            self.url = url
        if description is not None:
            self.description = description


class Institution(Base, ToDictMixin):
    """
    An institutional unit within the database. Used as a division of scopes
    available to the end user, and to partition information in other places.
    """

    __tablename__ = "institutions"

    identifier = Column(String, primary_key=True)
    instid = synonym("identifier")
    name = Column(String)
    cancelled = Column(Boolean, nullable=False, default=False, server_default="False")
    members = relationship(
        "Person",
        secondary=institution_associations,
        back_populates="institutions",
        uselist=True,
    )

    def __init__(self, instid: str) -> None:
        """Create an empty institution."""
        self.instid = instid


# Union types for all attributes
AnyAttribute = Union[TelephoneNumber, Title, EmailAddress, Address, URL]
AnyAttributeList = Union[
    List[TelephoneNumber], List[Title], List[EmailAddress], List[Address], List[URL]
]


@event.listens_for(Tag.people, "append", raw=True)
@event.listens_for(Tag.people, "remove", raw=True)
def tag_people_event(target: InstanceState, value: Person, initiator: Event) -> None:
    """
    List of people on a tag has changed. Trigger a sync of the people_count
    once the change has been committed.
    """
    # Require raw=True so that the trigger can be deferred until the changes
    # have been committed to ensure that there are no race conditions and
    # the task being processed gets the correct list of people.
    # Pull the attribute from inside the InstanceState.
    identifier = target.attrs.identifier.value

    @event.listens_for(target.session, "after_commit")
    def sync_after_commit(session: Session) -> None:
        """People list has been updated, schedule a task to update the count."""
        housekeeping.sync_tag_count.delay(identifier)
