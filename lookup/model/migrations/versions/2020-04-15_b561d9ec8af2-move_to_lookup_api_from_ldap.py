"""
Move to lookup API from ldap

Revision ID: b561d9ec8af2
Revises: ebc914b9b4f9
Create Date: 2020-04-15 16:47:30.817224

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "b561d9ec8af2"
down_revision = "ebc914b9b4f9"
branch_labels = None
depends_on = None


def upgrade():
    # Add the column with the migration indicator so that fields
    # are not duplicated when switching data source.
    op.add_column(
        "people",
        sa.Column(
            "lookup_cam",
            postgresql.JSONB(astext_type=sa.Text()),
            server_default='{"migrated": true}',
            nullable=False,
        ),
    )
    op.alter_column("people", "lookup_cam", server_default="{}")
    op.drop_column("people", "ldap")
    op.drop_column("people", "directories")
    op.drop_column("people", "extra")


def downgrade():
    op.add_column(
        "people",
        sa.Column(
            "extra",
            postgresql.JSONB(astext_type=sa.Text()),
            server_default=sa.text("'{}'::jsonb"),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.add_column(
        "people",
        sa.Column(
            "directories",
            postgresql.JSONB(astext_type=sa.Text()),
            server_default=sa.text("'{}'::jsonb"),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.add_column(
        "people",
        sa.Column(
            "ldap",
            postgresql.JSONB(astext_type=sa.Text()),
            server_default=sa.text("'{}'::jsonb"),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.drop_column("people", "lookup_cam")
