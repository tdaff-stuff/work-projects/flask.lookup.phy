"""
Custom extensions to the supplied sqlalchemy types.

Implemented types:

- `DictType`: A JSON backed representation of a dict. Limited to JSON
  representation so cannot deal with numeric keys of complex types.
- `B64Binary`: A binary file that is encoded with base64 in the
  application layer, but stored as a binary object at the db level.

"""

import base64
from typing import Optional, List, Any, Dict

from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.hybrid import HYBRID_PROPERTY
from sqlalchemy.inspection import inspect
from sqlalchemy.types import TypeDecorator, LargeBinary
from sqlalchemy.ext.mutable import MutableDict


class ToDictMixin:
    def to_dict(
        self, max_depth: int = 1, exclude: Optional[List[str]] = None
    ) -> Dict[str, Any]:
        """
        Return a dictionary view of the item.

        Parameters
        ----------
        max_depth
            Descend into recursive relationship properties up to
            a maximum number of levels to prevent circular relationships.
        exclude
            A list of key names of properties to skip/ignore.

        Returns
        -------
        as_dict
            The object as a dictionary of Python types.
        """
        as_dict = {}
        exclude = exclude or []

        mapper = inspect(self).mapper

        # individual values
        for prop in mapper.column_attrs + mapper.composites:
            if prop.key in exclude:
                continue
            as_dict[prop.key] = getattr(self, prop.key)

        # descend into relationships
        if max_depth > 0:
            depth = max_depth - 1
            for prop in mapper.relationships:
                value = getattr(self, prop.key)
                if prop.uselist:  # many value relationship
                    as_dict[prop.key] = [
                        item.to_dict(max_depth=depth, exclude=exclude) for item in value
                    ]
                else:
                    # Short circuit and for the NULL case
                    as_dict[prop.key] = value and value.to_dict(
                        max_depth=depth, exclude=exclude
                    )

        # hybrid properties
        for attr, val in mapper.all_orm_descriptors.items():
            # skipped items, or the mapper itself
            if attr in exclude or attr == "__mapper__":
                continue
            if val.extension_type == HYBRID_PROPERTY:
                as_dict[attr] = getattr(self, attr)

        # aliased values
        for prop in mapper.synonyms:
            if prop.key in exclude:
                continue
            as_dict[prop.key] = as_dict.get(prop.name, None)

        return as_dict


# noinspection PyAbstractClass
class B64Binary(TypeDecorator):
    """
    A binary blob that is represented as a base64 encoded string
    in the application.
    """

    impl = LargeBinary

    def process_bind_param(self, value, dialect) -> str:
        if value is not None:
            value = base64.b64decode(value)
        return value

    def process_result_value(self, value, dialect) -> str:
        if value is not None:
            value = base64.b64encode(value).decode("ascii")
        return value

    @property
    def python_type(self):
        return str


# Column type for SQLAlchemy
DictType = MutableDict.as_mutable(JSONB)
