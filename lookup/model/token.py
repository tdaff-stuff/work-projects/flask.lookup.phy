"""
Database of reset tokens.
"""

from sqlalchemy import Column, Integer, String, DateTime

from lookup.model import Base
from lookup.model.ext import DictType


class Token(Base):
    """
    A one-time token to store a piece of information.
    Has a hexadecimal request identifier which is used
    to retrieve the stored information.
    """

    __tablename__ = "tokens"

    key = Column(String, primary_key=True)  # request identifier (use UUID!)
    value = Column(DictType)  # stored information released with the key
    expiry = Column(DateTime)  # Valid until
    uses = Column(Integer)  # token removed after so many uses
