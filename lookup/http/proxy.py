"""
A proxy to get around CORS restrictions, for example
with the lookup.cam API

"""

import requests
from flask import Blueprint
from flask import request, Response, abort
from requests.compat import urlparse


proxy_blueprint = Blueprint("p", __name__, url_prefix="/p")

whitelist = ["www.lookup.cam.ac.uk", "lookup-test.csx.cam.ac.uk"]


@proxy_blueprint.route("/<path:url>", methods=["GET"])
def proxy(url):
    """

    Parameters
    ----------
    url

    Returns
    -------

    """

    url = "https://{}".format(url)
    split_url = urlparse(url)

    if split_url.hostname not in whitelist:
        abort(403)

    resp = requests.request(
        method=request.method,
        url=url,
        headers={key: value for (key, value) in request.headers if key != "Host"},
        data=request.get_data(),
        params=request.args,
        cookies=request.cookies,
        allow_redirects=False,
    )

    excluded_headers = [
        "content-encoding",
        "content-length",
        "transfer-encoding",
        "connection",
    ]
    headers = [
        (name, value)
        for (name, value) in resp.raw.headers.items()
        if name.lower() not in excluded_headers
    ]

    return Response(resp.content, resp.status_code, headers)
