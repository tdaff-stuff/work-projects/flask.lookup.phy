"""
http endpoints for the flask app

Only public exposed access to the database. Defines all available methods
to access the database.

"""

from functools import wraps
from typing import Callable, Type, Union, Tuple, Any

from flask import Blueprint, after_this_request, current_app
from flask import request, session, g, jsonify, abort, redirect, url_for
from werkzeug import Response

from lookup.interface import UserDatabase, PeopleDatabaseSources, TokenDatabase
from lookup.interface.api import Api
from lookup.util.text import string_to_bool


api_blueprint = Blueprint("api", __name__, url_prefix="/api")


def abort_on_error(
    code: int, error: Union[Type[BaseException], Tuple[Type[BaseException]]]
) -> Callable:
    """
    Decorator to raise the appropriate http error when encountering
    the given exception.
    """

    def decorate(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            try:
                return func(*args, **kwargs)
            except error:
                return abort(code)

        return wrapper

    return decorate


def login_required(view_func: Callable) -> Callable:
    """
    A valid username is required for this view.

    Decorate a view function so that requests that have no
    logged return an error.

    Parameters
    ----------
    view_func
        A routed view function

    Returns
    -------
    wrapped_func
        The same function wrapped in a login check

    """

    @wraps(view_func)  # Flask routing requires original function names
    def wrapped_func(*args, **kwargs) -> Response:
        if not g.user:
            return abort(403)
        else:
            return view_func(*args, **kwargs)

    return wrapped_func


@api_blueprint.before_request
def setup_request_data():
    """
    Setup databases for the request in a global context.

    Set up a UserDatabase for the request and determine
    which user is invoking the request from the session key.

    Initialise an Api authenticated by the current user.

    Sets up variables in the global context:

        g.user_db: a UserDatabase instance
        g.user: the current logged in user
        g.api: an Api initialised for the logged in user
    """
    database = current_app.config["database"]
    session_key = session.get("session_key")
    g.user_db = UserDatabase(database)
    g.user = g.user_db.get_user(key=session_key)
    g.api = Api(
        database=PeopleDatabaseSources(database),
        token_database=TokenDatabase(database),
        user=g.user,
    )

    @after_this_request
    def teardown(response: Response) -> Response:
        """
        Ensure database connections are closed. Session closure
        is handled in the ``__del__`` methods of the database helpers.
        """
        del g.user_db
        del g.api
        return response


@api_blueprint.route("/sessions/<string:session_key>", methods=["DELETE"])
@login_required
def session_delete(session_key: str) -> Response:
    """
    Remove a session.
    """
    # Can't do anything without the key
    if not session_key:
        return abort(400)

    # Can only delete own sessions
    session_user = g.user_db.get_user(key=session_key)
    if g.user != session_user:
        return abort(403)

    g.user_db.logout(key=session_key)
    return jsonify({"key": session_key})


@api_blueprint.route("/sessions/<string:session_key>", methods=["GET"])
@login_required
def session_get(session_key: str) -> Response:
    """
    Retrieve a session.
    """
    if not session_key:
        return abort(400)

    user_session = g.user_db.get_session(session_key)

    # Act like nothing there is requesting the wrong user's key
    if not user_session or user_session.user_id != g.user:
        return abort(404)

    return jsonify(user_session.to_dict())


@api_blueprint.route("/sessions/new", methods=["GET"])
@login_required
def session_new() -> Response:
    """
    Create a session and send the response there.
    """

    new_key = g.user_db.login(
        user_id=g.user,
        session_type="api",
        agent=request.user_agent.string,
        address=request.remote_addr,
    )

    return redirect(url_for("api.session_get", session_key=new_key))


@api_blueprint.route("/people/", methods=["GET"])
@login_required
def people_get():
    """
    @api {get} people/ Request a list of people's CRSids
    @apiName people_get
    @apiGroup people

    @apiDescription Retrieve a list of people

    @apiParam {String}      [query='']   String to query
    @apiParam {Integer}     [limit=0]    Limit to the first x entries. If
                                         0 all people matching the request
                                         will be returned.
    @apiParam {Bool}        [full=false] Include the full information of
                                         each person instead of CRSids.

    @apiSuccess (200)       {String[]}  people  List of CRSids

    @apiSuccessExample Success-Response:
        {
            "people": ['abc12', 'def34', 'ede33']
        }

    @apiSuccess (200 Full)  {Person[]}  people  List of people with full
                                                information

    @apiSuccessExample Success-Response-Full:
        [
            {
                "id": "acb12",
                "displayName": "A.C. Barnes"
                ...
            },
            {
                "id": "bc3423",
                "displayName": "B. Charles"
                ...
            }
            ...
        ]

    """

    query = request.args.get("query", None)
    full = string_to_bool(request.args.get("full"))
    limit = int(request.args.get("limit", 0)) or None
    offset = int(request.args.get("offset", 0)) or None
    fuzzy = string_to_bool(request.args.get("fuzzy"))
    return jsonify(
        g.api.get_people(query, full=full, limit=limit, offset=offset, fuzzy=fuzzy)
    )


@api_blueprint.route("/people/", methods=["POST"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(400, ValueError)
def people_post():
    """
    @api {post} people/ Create a new person
    @apiName people_post
    @apiGroup people

    @apiDescription Create a new person. Various information about the
                    person will be accepted as data.

    @apiParam {String}      crsid   Mandatory! Person's CRSid
    @apiParam {String}      [name]  Person's chosen name

    @apiParamExample {json} People-Post
        {
            "crsid": "abc12",
            "displayName": "A. B. Cox"
        }

    @apiSuccess {String}    crsid     Person's CRSid
    @apiSuccess {Various}   person    All defined properties of the person

    @apiSuccessExample Success-Response:
        {
            "crsid": "abc12",
            "displayName": "A. B. Cox"
        }

    @apiError (400) Bad-Data Malformed data passed to create
    @apiError (403) Not Authorised

    """
    data = request.get_json(force=True)  # fails with 400 for bad JSON

    if data is None:  # empty request
        return abort(400)

    if "crsid" not in data:  # can't create without a CRSid
        return abort(400)

    new_person = g.api.new_person(**data)

    return jsonify(new_person)


@api_blueprint.route("/people/<string:crsid>", methods=["GET"])
@login_required
def people_crsid_get(crsid):
    """Return a person information."""
    person = g.api.get_person(crsid)
    if person is None:
        abort(404)
    else:
        return jsonify(person)


@api_blueprint.route("/people/<string:crsid>", methods=["PUT"])
@login_required
def people_crsid_put(crsid):
    """Replace an existing person."""
    person = g.api.get_person(crsid)
    if person is None:
        abort(404)

    data = request.get_json(force=True)  # fails with 400 for bad JSON
    if data is None:  # empty request
        abort(400)
    if "crsid" not in data:  # can't create without a CRSid
        data["crsid"] = crsid
    elif data["crsid"] != crsid:
        abort(400)

    new_person = g.api.update_person(replace=True, **data)
    return jsonify(new_person), 201


@api_blueprint.route("/people/<string:crsid>", methods=["PATCH"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_patch(crsid):
    """Update a person's information."""
    # can't update nobody
    if not crsid:
        abort(400)

    data = request.get_json(force=True)  # fails with 400 for bad JSON
    if data is None:
        abort(400)

    # remove 'crsid' since it is explicitly given in url
    # and breaks update api.
    if "crsid" in data:
        if data["crsid"] != crsid:
            abort(400)
        else:
            del data["crsid"]

    person = g.api.update_person(crsid, **data)

    if person is None:
        abort(404)
    else:
        return jsonify(person)


@api_blueprint.route("/people/<string:crsid>", methods=["DELETE"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_delete(crsid):
    """Delete a person."""
    if not crsid:
        abort(400)

    person = g.api.delete_person(crsid)

    return jsonify(person)


@api_blueprint.route("/people/<string:crsid>/tags/", methods=["DELETE"])
@login_required
def people_crsid_tags_delete(crsid):
    """Delete tags."""
    tags = request.get_json()  # JSON.stringify([tag, tag, ...])
    new_tags = g.api.delete_person_tags(crsid, tags)
    return jsonify(new_tags)


@api_blueprint.route("/people/<string:crsid>/tags/", methods=["POST"])
@login_required
def people_crsid_tags_post(crsid):
    """Add tags."""
    tags = request.get_json()  # JSON.stringify([tag, tag, ...])
    new_tags = g.api.add_person_tags(crsid, tags)
    return jsonify(new_tags)


@api_blueprint.route("/people/<string:crsid>/suggested_tags/", methods=["GET"])
@login_required
def people_crsid_suggested_tags_get(crsid):
    """
    Get a list of suggested tags for a user.
    """

    limit = int(request.args.get("limit", 0)) or None
    offset = int(request.args.get("offset", 0)) or None
    sort = request.args.get("sort", None)

    return jsonify(
        g.api.get_person_suggested_tags(crsid, limit=limit, offset=offset, sort=sort)
    )


@api_blueprint.route(
    "/people/<string:crsid>/linked_services/<string:service>/", methods=["PATCH"],
)
@login_required
def people_crsid_linked_services_service_patch(crsid, service):
    """Modify data associated with a service."""
    new_services = {service: request.get_json()}
    person = g.api.update_person(
        crsid, linked_services=new_services, operation="update"
    )
    return jsonify(person["linked_services"][service])


@api_blueprint.route("/people/<string:crsid>/<string:attr>/", methods=["DELETE"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_attr_delete(crsid, attr):
    """Delete entries in a multivalue attribute."""
    kwargs = {attr: request.get_json()}
    person = g.api.update_person(crsid, operation="remove", **kwargs)
    return jsonify(person[attr])


@api_blueprint.route("/people/<string:crsid>/<string:attr>/", methods=["PATCH"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_attr_patch(crsid, attr):
    """Add a values into a multivalue attribute."""
    kwargs = {attr: request.get_json()}
    person = g.api.update_person(crsid, **kwargs)
    return jsonify(person[attr])


@api_blueprint.route(
    "/people/<string:crsid>/<string:attr>/<int:identifier>", methods=["PUT"]
)
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_attr_identifier_put(
    crsid: str, attr: str, identifier: int
) -> Response:
    """Replace an existing attribute with an updated one."""
    data = request.get_json()
    new_attr = g.api.replace_person_attribute(
        crsid=crsid, attr=attr, identifier=identifier, data=data
    )
    return jsonify(new_attr)


@api_blueprint.route("/people/<string:crsid>/subordinates/", methods=["POST"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_subordinates_post(crsid: str) -> Response:
    """Add a subordinate to a person."""
    subordinate = request.get_json()
    person = g.api.add_person_subordinate(crsid, subordinate)
    if isinstance(person, bool):
        response = {"confirmation": person}
    else:
        response = person["subordinates"]
    return jsonify(response)


@api_blueprint.route("/people/<string:crsid>/line_managers/", methods=["POST"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_line_managers_post(crsid: str) -> Response:
    """Add a line manager to a person."""
    manager = request.get_json()
    person = g.api.add_person_manager(crsid, manager)
    if isinstance(person, bool):
        response = {"confirmation": person}
    else:
        response = person["line_managers"]
    return jsonify(response)


@api_blueprint.route("/people/<string:crsid>/subordinates/", methods=["DELETE"])
@login_required
def people_crsid_subordinates_delete(crsid: str) -> Response:
    """Remove a subordinate from a person."""
    subordinate = request.get_json()
    person = g.api.delete_person_subordinate(crsid, subordinate)
    return jsonify(person["subordinates"])


@api_blueprint.route("/people/<string:crsid>/line_managers/", methods=["DELETE"])
@login_required
def people_crsid_line_managers_delete(crsid: str) -> Response:
    """Remove a line manager from a person."""
    manager = request.get_json()
    person = g.api.delete_person_manager(crsid, manager)
    return jsonify(person["line_managers"])


@api_blueprint.route("/people/<string:crsid>/availability", methods=["PUT"])
@login_required
@abort_on_error(400, ValueError)
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_availability_put(crsid: str) -> Response:
    """Set a person's availability information."""
    availability = request.get_json()
    return jsonify(g.api.set_person_availability(crsid, availability))


@api_blueprint.route("/people/<string:crsid>/availability", methods=["DELETE"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def people_crsid_availability_delete(crsid: str) -> Response:
    """Remove a person's availability information."""
    return jsonify(g.api.set_person_availability(crsid, None))


@api_blueprint.route("/tags/", methods=["GET"])
@login_required
def tags_get():
    """List of all available tags."""
    query = request.args.get("query", None)
    limit = int(request.args.get("limit", 0)) or None
    sort = request.args.get("sort", None)
    return jsonify(g.api.get_tags(query=query, limit=limit, sort=sort))


@api_blueprint.route("/tags/", methods=["POST"])
@login_required
def tags_post():
    """
    Expect a list of [{name: 'xxx', description: 'xxx'}].
    Return newly created tags.
    """
    data = request.get_json()  # fails with 400 for bad JSON
    new_tags = g.api.add_tags(data)
    return jsonify(new_tags)


@api_blueprint.route("/tags/<path:tag>", methods=["GET"])
@login_required
def tags_tag_get(tag):
    """Return a list of CRSids associated with a given tag."""
    full = string_to_bool(request.args.get("full"))
    limit = int(request.args.get("limit", 0)) or None
    return jsonify(g.api.get_tag_people(tag, limit=limit, full=full))


@api_blueprint.route("/tags/<path:tag>", methods=["PATCH"])
@login_required
def tags_tag_patch(tag):
    """Update the data associated with a tag."""
    # can't update nobody
    if not tag:
        abort(400)

    data = request.get_json(force=True)  # fails with 400 for bad JSON

    # remove 'tag' which would break api
    if "tag" in data:
        del data["tag"]

    try:
        updated_tag = g.api.update_tag(tag, **data)
    except KeyError:
        return abort(409)

    if updated_tag is None:
        return abort(404)
    else:
        return jsonify(updated_tag)


@api_blueprint.route("/rooms/", methods=["GET"])
@login_required
def rooms_get():
    """Produce a list of all rooms"""
    query = request.args.get("query", None)
    limit = int(request.args.get("limit", 0)) or None
    return jsonify(g.api.get_rooms(query=query, limit=limit))


@api_blueprint.route("/rooms/", methods=["POST"])
@login_required
def rooms_post():
    """
    Expect a list of [{building: 'xxx', number: 'xxx', description: 'xxx'}].
    Return newly created rooms.
    """
    data = request.get_json()  # fails with 400 for bad JSON
    new_rooms = g.api.add_rooms(data)
    return jsonify(new_rooms)


@api_blueprint.route("/rooms/<string:identifier>", methods=["PATCH"])
@login_required
@abort_on_error(403, PermissionError)
@abort_on_error(404, KeyError)
def rooms_room_patch(identifier):
    """Update room information."""
    if not identifier:
        abort(400)

    data = request.get_json(force=True)

    if "identifier" in data:
        del data["identifier"]

    updated_room = g.api.update_room(identifier, **data)

    return jsonify(updated_room)


@api_blueprint.route("/rooms/<string:identifier>", methods=["GET"])
@login_required
def rooms_room_get(identifier):
    """Return a list of CRSids associated with a room"""
    full = string_to_bool(request.args.get("full"))
    limit = int(request.args.get("limit", 0)) or None
    return jsonify(g.api.get_room_people(identifier, limit=limit, full=full))


@api_blueprint.route("/rooms/<string:identifier>", methods=["DELETE"])
@login_required
@abort_on_error(403, PermissionError)
def rooms_room_delete(identifier):
    """Delete a room and return the deleted room."""
    if not identifier:
        abort(400)

    room = g.api.delete_room(identifier)

    return jsonify(room)


@api_blueprint.route("/buildings/<string:name>/rooms/", methods=["GET"])
@login_required
def building_rooms_get(name):
    """Produce a list of all rooms"""
    query = request.args.get("query", None)
    limit = int(request.args.get("limit", 0)) or None
    return jsonify(g.api.get_building_rooms(name, query=query, limit=limit))
