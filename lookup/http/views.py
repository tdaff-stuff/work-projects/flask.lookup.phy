"""
web views of the database

"""

from functools import wraps
from typing import Callable, Union
from urllib.parse import urlparse

from flask import Blueprint, g, after_this_request, current_app
from flask import request, render_template, abort, session, redirect, url_for
from werkzeug import Response

from lookup.interface import UserDatabase, PeopleDatabaseSources, TokenDatabase
from lookup.interface.api import Api
from lookup.util.web import get_remote_user, get_current_site
from lookup.util.web import get_common_args, generate_pages

web_blueprint = Blueprint("web", __name__)

# Number of results per page, shown in drop down select
LIMITS_1COL = [10, 20, 50, 100]
LIMITS_3COL = [18, 36, 72, 144]
# Available sorting options
SORTS_PEOPLE = [
    ("surname", "Surname"),
    ("display_name", "Display name"),
    ("crsid", "CRSid"),
]
SORTS_TAGS = [
    ("name", "Alphabetical"),
    ("count", "Count"),
    ("new", "New"),
    ("trending", "Trending"),
]


def login_required(view_func: Callable) -> Callable:
    """
    A valid username is required for this view.

    Decorate a view function so that requests that have no
    logged in user are redirected to the login page. Calls
    to the login page are given a argument "next" that
    points back to the original url.

    Parameters
    ----------
    view_func
        A routed view function

    Returns
    -------
    wrapped_func
        The same function wrapped in a login check

    """

    @wraps(view_func)  # Flask routing requires original function names
    def wrapped_func(*args, **kwargs) -> Response:
        if not g.user:
            return redirect(url_for("web.login", next=request.url))
        else:
            return view_func(*args, **kwargs)

    return wrapped_func


@web_blueprint.before_request
def setup_request_data():
    """
    Setup databases for the request in a global context.

    Set up a UserDatabase for the request and determine
    which user is invoking the request from the session key.

    Initialise an API authenticated by the current user.

    Sets up variables in the global context:

        g.user_db: a UserDatabase instance
        g.user: the current logged in user
        g.api: an Api initialised for the logged in user
    """
    database = current_app.config["database"]
    session_key = session.get("session_key")
    g.user_db = UserDatabase(database)
    g.user = g.user_db.get_user(
        key=session_key, agent=request.user_agent.string, address=request.remote_addr,
    )
    g.api = Api(
        database=PeopleDatabaseSources(database),
        token_database=TokenDatabase(database),
        user=g.user,
    )
    g.site = get_current_site()

    @after_this_request
    def teardown(response: Response) -> Response:
        """
        Ensure database connections are closed. Session closure
        is handled in the ``__del__`` methods of the database helpers.
        """
        del g.user_db
        del g.api
        return response


@web_blueprint.route("/")
def index() -> str:
    """Landing page with search box and some tags"""
    tags_top = g.api.get_tags(limit=20, sort="count")["data"]
    tags_new = g.api.get_tags(limit=20, sort="new")["data"]
    tags_trending = g.api.get_tags(limit=20, sort="trending")["data"]
    user_info = g.api.get_person(g.user)

    return render_template(
        "index.html",
        tags_top=tags_top,
        tags_new=tags_new,
        tags_trending=tags_trending,
        user_info=user_info,
    )


@web_blueprint.route("/login")
def login() -> Response:
    """
    Log in as the authenticated user and store in a session cookie
    Currently expects REMOTE_USER  to be set through Raven/Apache.

    Redirect to homepage after login.
    """
    session.permanent = True
    session_key = g.user_db.login(
        user_id=get_remote_user(),
        agent=request.user_agent.string,
        address=request.remote_addr,
    )
    session["session_key"] = session_key

    # Can't create a login session so error
    if not session_key:
        return abort(401)

    next_url = request.args.get("next", url_for("web.index"))
    if urlparse(next_url).netloc == urlparse(request.url).netloc:
        return redirect(next_url)
    else:
        return redirect(url_for("web.index"))


@web_blueprint.route("/logout")
def logout() -> Response:
    """
    Log out a currently logged in user and close that session.

    Redirect to homepage after logout.
    """
    g.user_db.logout(key=session["session_key"])
    session.pop("session_key", None)
    response = redirect(url_for("web.index"))
    # Make Raven re-authenticate if required
    response.delete_cookie("Ucam-WebAuth-Session")  # http (development only)
    response.delete_cookie("Ucam-WebAuth-Session-S")  # https
    return response


@web_blueprint.route("/people/")
@login_required
def people():
    args = get_common_args(request, query=None, limit=10, offset=0)

    people_data = g.api.get_people(
        query=args["query"],
        limit=args["limit"],
        offset=args["offset"],
        full=True,
        fuzzy=args["fuzzy"],
        sort=args["sort"],
    )

    # data response
    meta = people_data["meta"]
    people_data = people_data["data"]

    # calculate pages here since no max function in jinja2
    pagination = generate_pages(meta, limits=LIMITS_1COL, sorts=SORTS_PEOPLE)

    return render_template(
        "people.html", people_data=people_data, pagination=pagination
    )


@web_blueprint.route("/people+tags/")
@login_required
def people_and_tags():
    args = get_common_args(request, query=None, limit=10, offset=0)

    people_data = g.api.get_people(
        query=args["query"],
        limit=args["limit"],
        offset=args["offset"],
        full=True,
        fuzzy=args["fuzzy"],
        sort=args["sort"],
    )

    tag_data = g.api.get_tags(query=args["query"], limit=12, sort="count")

    # data response
    meta = people_data["meta"]
    people_data = people_data["data"]
    tag_data = tag_data["data"]

    # calculate pages here since no max function in jinja2
    pagination = generate_pages(meta, limits=LIMITS_1COL, sorts=SORTS_PEOPLE)

    return render_template(
        "people.html",
        people_data=people_data,
        pagination=pagination,
        tag_data=tag_data,
    )


@web_blueprint.route("/people/<string:crsid>")
@login_required
def person(crsid):
    person_data = g.api.get_person(crsid)

    if person_data is None:
        person_data = {"crsid": crsid}
        return render_template("noperson.html", person_data=person_data)
    else:
        edit = g.api.can_edit_crsid(crsid)
        return render_template("person.html", person_data=person_data, edit=edit)


@web_blueprint.route("/self")  # lookup.cam
@web_blueprint.route("/@self")
@web_blueprint.route("/me")
@web_blueprint.route("/@me")
@web_blueprint.route("/myself")
@web_blueprint.route("/@myself")
@web_blueprint.route("/people/@me")
@web_blueprint.route("/people/@self")
@web_blueprint.route("/people/@myself")
@login_required
def self() -> Response:
    """
    Aliases for the current user's profile.

    Any of the URLs will redirect the user to their own /people/crsid
    page. The `/self` URL mirrors lookup.cam, but there are a few
    other options if you think that `@me` makes things clearer.

    These will all have higher priority than any routes with arguments.
    """
    return redirect(url_for("web.person", crsid=g.user))


@web_blueprint.route("/tags/")
@login_required
def tags():
    args = get_common_args(request, query=None, limit=36, offset=0)

    tags_data = g.api.get_tags(
        query=args["query"],
        limit=args["limit"],
        offset=args["offset"],
        sort=args["sort"],
    )
    user_info = g.api.get_person(g.user)

    # data response
    meta = tags_data["meta"]
    tags_data = tags_data["data"]

    pagination = generate_pages(meta, limits=LIMITS_3COL, sorts=SORTS_TAGS)

    return render_template(
        "tags.html", tags_data=tags_data, pagination=pagination, user_info=user_info,
    )


# path deals with slashes in tag names
@web_blueprint.route("/tags/<path:name>/people/")
@login_required
def tag(name):
    args = get_common_args(request, limit=10, offset=0)

    tag_data = g.api.get_tag(name)

    if tag_data is None:
        return abort(404)

    tag_people_data = g.api.get_tag_people(
        name, limit=args["limit"], offset=args["offset"], full=True, sort=args["sort"],
    )

    meta = tag_people_data["meta"]
    tag_people_data = tag_people_data["data"]

    pagination = generate_pages(meta, limits=LIMITS_1COL, sorts=SORTS_PEOPLE)

    user_info = g.api.get_person(g.user)

    return render_template(
        "tag.html",
        tag=tag_data,
        tag_people=tag_people_data,
        pagination=pagination,
        user_info=user_info,
    )


@web_blueprint.route("/rooms/")
@login_required
def rooms():
    args = get_common_args(request, query=None, limit=36, offset=0)

    rooms_data = g.api.get_rooms(
        query=args["query"], limit=args["limit"], offset=args["offset"]
    )

    # data response
    meta = rooms_data["meta"]
    rooms_data = rooms_data["data"]

    pagination = generate_pages(meta, limits=LIMITS_3COL)

    user_info = g.api.get_person(g.user)

    return render_template(
        "rooms.html", rooms_data=rooms_data, pagination=pagination, user_info=user_info,
    )


@web_blueprint.route("/rooms/<string:identifier>")
@login_required
def room(identifier):
    args = get_common_args(request, limit=10, offset=0)

    room_data = g.api.get_room(identifier)

    if room_data is None:
        return abort(404)

    room_people_data = g.api.get_room_people(
        identifier,
        limit=args["limit"],
        offset=args["offset"],
        full=True,
        sort=args["sort"],
    )

    meta = room_people_data["meta"]
    room_people_data = room_people_data["data"]

    pagination = generate_pages(meta, limits=LIMITS_1COL, sorts=SORTS_PEOPLE)

    user_info = g.api.get_person(g.user)

    return render_template(
        "room.html",
        room=room_data,
        room_people=room_people_data,
        pagination=pagination,
        user_info=user_info,
    )


@web_blueprint.route("/buildings/<string:name>/")
@login_required
def building(name) -> Union[str, Response]:
    """View of rooms and people from a single building."""
    args = get_common_args(request, limit=10, offset=0)

    building_rooms_data = g.api.get_building_rooms(name)

    building_people_data = g.api.get_building_people(
        name, limit=args["limit"], offset=args["offset"], full=False
    )

    pagination = generate_pages(building_people_data["meta"], limits=LIMITS_1COL)

    return render_template(
        "building.html",
        rooms_meta=building_rooms_data["meta"],
        rooms=building_rooms_data["data"],
        people_meta=building_people_data["meta"],
        people=building_people_data["data"],
        pagination=pagination,
    )


@web_blueprint.route("/settings/")
@login_required
def settings() -> Response:
    """
    Settings and preferences for a logged in user. These relate to the
    browsing and interacting experience, not the lookup profile.
    """
    # Currently active sessions on any browser
    active_sessions = [
        user_session.to_web_format()
        for user_session in sorted(
            g.user_db.get_sessions(g.user, session_type="web"),
            key=lambda x: x.accessed_datetime,
            reverse=True,
        )
    ]
    # Tokens marked as API keys, most recent first
    api_keys = [
        api_key.to_web_format()
        for api_key in sorted(
            g.user_db.get_sessions(g.user, session_type="api"),
            key=lambda x: x.accessed_datetime,
            reverse=True,
        )
    ]
    return render_template(
        "settings.html", user_sessions=active_sessions, api_keys=api_keys
    )


@web_blueprint.route("/confirm/<string:key>")
@login_required
def confirm(key: str) -> Response:
    """
    Confirm an action associated with a restricted use key.
    """
    # Expired and invalid keys raise errors
    try:
        data = g.api.token_database.get_value(key)
    except KeyError:
        return render_template("action.html", error="Unknown or expired key.")

    if data.get("type", None) == "api":
        result = g.api.deferred_action(data)
        return render_template("action.html", data=data, result=result)
    else:
        return render_template("action.html", error="Unknown action.")


@web_blueprint.route("/maps/")
@login_required
def maps():
    return render_template("maps.html")


@web_blueprint.route("/privacy/")
def privacy() -> Union[str, Response]:
    return render_template("privacy.html")
