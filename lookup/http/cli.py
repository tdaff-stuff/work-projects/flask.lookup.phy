"""
Lookup CLI

Commands to manage lookup. Ensure that settings files are configured
in the environment running the commands.

FLASK_APP=lookup.http.app
LOOKUP_SETTINGS=/path/to/settings.cfg
LOOKUP_SECRETS=/path/to/secrets.cfg
"""
import logging

import click
from alembic import command, migration
from alembic.config import Config
from flask import current_app
from flask.cli import AppGroup

from lookup.interface import LookupDatabase

FILE_TEMPLATE = "%%(year)d-%%(month)02d-%%(day)02d_%%(rev)s-%%(slug)s"

db_cli = AppGroup("database", help="Commands to manage the database")


@db_cli.command("migrate")
@click.option("--verbose", "-v", count=True)
def migrate(verbose: int = 0) -> None:
    """
    Carry out any migrations, including initialisation.

    Parameters
    ----------
    verbose
        Turn on all logging levels.
    """
    # Output is minimal without logging
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    # Configured database is attached to the current app object.
    database = current_app.config["database"]  # type: LookupDatabase
    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("script_location", "lookup:model/migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Check whether the database is initialised by alembic
    context = migration.MigrationContext.configure(database.engine.connect())
    current_rev = context.get_current_revision()

    # Database does not exist, or not yet migrated
    if current_rev is None:
        # Will create a database or any missing tables
        database.initialise()
        # Stamp the current alembic revision
        command.stamp(config, "head")

    command.upgrade(config, "head")
    # Show the final rev if it's changed
    final_rev = context.get_current_revision()
    if verbose > 0 or final_rev != current_rev:
        print("{} -> {}".format(current_rev, final_rev))


@db_cli.command("makemigrations")
@click.argument("message")
def revision(message: str) -> None:
    """
    Use autogenerate to create migrations for the current database.

    Parameters
    ----------
    message
        Description of changes, also appears as a slug in the filename.
    """
    # Configured database is attached to the current app object.
    database = current_app.config["database"]  # type: LookupDatabase
    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("script_location", "lookup:model/migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Mostly sequential migration files
    config.set_main_option("file_template", FILE_TEMPLATE)

    # Run the command
    command.revision(config, message=message, autogenerate=True)


@db_cli.command("downgrade", context_settings={"ignore_unknown_options": True})
@click.argument("rev")
@click.option("--verbose", "-v", count=True)
def downgrade(rev: str, verbose: int = 0) -> None:
    """
    Downgrade the database to an earlier revision. Most common
    is probably `-1`, but an exact revision can also be specified.

    Parameters
    ----------
    rev
        Revision slug or relative change, e.g. -1.
    verbose
        Turn on all logging levels.
    """
    # `ignore_unknown_options` required to stop negative numbers getting
    # eaten by the parser

    # Output is minimal without logging
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    # Configured database is attached to the current app object.
    database = current_app.config["database"]  # type: LookupDatabase
    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("script_location", "lookup:model/migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Run the command
    command.downgrade(config, rev)
