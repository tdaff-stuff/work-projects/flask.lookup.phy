"""
Semi static files, e.g. maps stored in memcache
"""


from flask import Blueprint, current_app
from flask import jsonify
from flask.helpers import send_from_directory

from lookup.interface import PeopleDatabase, TokenDatabase
from lookup.interface.api import Api


resources_blueprint = Blueprint("resources", __name__, url_prefix="/resources")


@resources_blueprint.route("/maps/<string:building>_<string:floor>.geojson")
def maps(building, floor):
    """
    Return the map json merged with database information.
    """
    database = current_app.config["database"]
    maps_dir = current_app.config["MAPS_DIR"]

    api = Api(database=PeopleDatabase(database), token_database=TokenDatabase(database))

    return jsonify(api.get_merged_map(building, floor, maps_dir=maps_dir))


@resources_blueprint.route("/maps/<string:building>_<string:floor>.lines.geojson")
def map_lines(building, floor):
    """
    Send the lines file (doors)
    """
    maps_dir = current_app.config["MAPS_DIR"]

    filename = "{building}_{floor}.lines.geojson".format(building=building, floor=floor)

    return send_from_directory(maps_dir, filename)
