//
// lookup.phy
//

/* Helpful things */

/**
 * Trim whitespace from the ends, and duplicate spaces between words.
 * @returns {string}
 */
String.prototype.clean = function () {
  return this.replace(/\s+/g, " ").replace(/^\s+|\s+$/g, "");
};

/* Utility actions */

function goToSpanHref() {
  // make span elements with a href property act like a links
  var url = $(this).attr("href");
  if (url.length > 0) {
    window.location.href = url.toString();
  }
  return false;
}

function goToOptionHref() {
  // where options of a select have href attached to them
  // go there when they get selected
  var url = $(this).find(":selected").attr("href");
  if (url && url.length > 0) {
    window.location.href = url.toString();
  }
  return false;
}

/**
 * Generate an @cam.ac.uk address for the given person by prepending
 * their CRSid. Assumes that everyone has a CRSid and that @cam is a
 * valid address.
 * @param {Object} person - Person returned from the API
 * @param {string} person.crsid - Person's CRSid, will always exist
 */
function personToAtCam(person) {
  return person.crsid + "@cam.ac.uk";
}

/**
 * Generate a formatted email address for the given person. Format will be
 * "Display Name" <email@address.here>
 * @param {Object} person
 * @param {string} person.crsid - The persons CRSid will always exist
 * @param {string} [person.display_name] - Person's chosen name
 * @param {string} [person.registered_name] - Person' stored name
 */
function personToEmail(person) {
  // At least one of these will exist
  let name = person.display_name || person.registered_name || person.crsid;
  // Quote name and remove some control characters that upset mail clients
  return (
    JSON.stringify(name.replace(/[,";\\]/g, "")) +
    "<" +
    personToAtCam(person) +
    ">"
  );
}

/**
 * Create a modal popup containing a message to be displayed
 * with a header and a single "Close" button in the footer.
 *
 * Default header will be "Alert"
 *
 * @param {jQuery|string} body Text or object to use as modal body
 * @param {jQuery|string} [header] Text or object to use as modal header (<h5>)
 */
function modalAlert(body, header) {
  if (header === undefined) {
    header = "Alert";
  }
  $("<div>", { class: "modal fade", role: "dialog", tabindex: "-1" })
    .append(
      $("<div>", {
        class: "modal-dialog modal-dialog-centered",
        role: "document",
      }).append(
        $("<div>", { class: "modal-content" })
          .append(
            $("<div>", { class: "modal-header" })
              .append($("<h5>", { class: "modal-title" }).append(header))
              .append(
                $("<button>", {
                  type: "button",
                  class: "close",
                  "data-dismiss": "modal",
                }).append($("<span>", { text: "×" }))
              )
          )
          .append($("<div>", { class: "modal-body" }).append(body))
          .append(
            $("<div>", { class: "modal-footer" }).append(
              $("<button>", {
                class: "btn btn-secondary",
                "data-dismiss": "modal",
                text: "Close",
              })
            )
          )
      )
    )
    .modal()
    .on("hidden.bs.modal", function () {
      // Destroy after it's dismissed
      $(this).remove();
    });
}

/**
 * Create a modal popup asking for confirmation for an action.
 * Will have a "Cancel" button and a "Confirm" which will execute
 * the action function.
 *
 * Default header will be "Confirm"
 *
 * @param {function} action
 * @param {jQuery|string} body Text or object to use as modal body
 * @param {jQuery|string} [header] Text or object to use as modal header (<h5>)
 */
function modalConfirm(action, body, header) {
  if (header === undefined) {
    header = "Confirm";
  }
  $("<div>", { class: "modal fade", role: "dialog", tabindex: "-1" })
    .append(
      $("<div>", {
        class: "modal-dialog modal-dialog-centered",
        role: "document",
      }).append(
        $("<div>", { class: "modal-content" })
          .append(
            $("<div>", { class: "modal-header" })
              .append($("<h5>", { class: "modal-title" }).append(header))
              .append(
                $("<button>", {
                  type: "button",
                  class: "close",
                  "data-dismiss": "modal",
                }).append($("<span>", { text: "×" }))
              )
          )
          .append($("<div>", { class: "modal-body" }).append(body))
          .append(
            $("<div>", { class: "modal-footer" })
              .append(
                $("<button>", {
                  class: "btn btn-secondary",
                  "data-dismiss": "modal",
                  text: "Cancel",
                })
              )
              .append(
                $("<button>", {
                  class: "btn btn-primary",
                  "data-dismiss": "modal",
                  text: "Confirm",
                }).on("click", action)
              )
          )
      )
    )
    .modal()
    .on("hidden.bs.modal", function () {
      // Destroy after it's dismissed
      $(this).remove();
    });
}

function buildListPerson(person, cls) {
  // construct a link to a person for a dropdown menu or list
  return $("<a>", {
    href: "/people/" + person.crsid,
    class: cls + " person-list d-flex",
    tabindex: 0,
  })
    .append(
      $("<div>", { class: "thumbnail-container-sm mr-2" }).append(
        person.thumbnail
          ? $("<img>", {
              class: "thumbnail-image-sm rounded",
              src: "data:image/png;base64, " + person.thumbnail,
            })
          : $("<span>", { class: "fas fa-user thumbnail-image-sm" })
      )
    )
    .append(
      $("<div>", { class: "w-100 d-flex justify-content-between" })
        .append(
          $("<span>", {
            class: "mr-3 text-dark",
            text: person.display_name,
          })
        )
        .append(
          $("<code>", {
            class: "text-danger",
            text: person.crsid,
          })
        )
    );
}

function renderPerson(person) {
  return $("<a>", {
    href: "/people/" + person.crsid,
    class: "card list-group-item text-dark",
  })
    .append(
      $("<div>", {
        class: "d-flex justify-content-between card-title",
      })
        .append(
          $("<span>", {
            class: "mr-3",
            text: person.display_name,
          })
        )
        .append(
          $("<code>", {
            class: "text-danger",
            text: person.crsid,
          })
        )
    )
    .append(
      $("<div>", {
        class: "d-flex card-text",
      })
        .append(
          person.thumbnail
            ? $("<div>", {
                class: "thumbnail-container text-center align-middle mr-2",
              }).append(
                $("<img>", {
                  class: "thumbnail-image rounded",
                  src: "data:image/png;base64, " + person.thumbnail,
                })
              )
            : null
        )
        .append(
          $("<div>")
            .append(
              person.tags.map(function (tag) {
                return $("<span>", {
                  class: "badge badge-tag mr-1",
                  "data-tag": tag.name,
                  href: "/tags/" + tag.name + "/people/",
                  text: tag.name,
                  title: tag.description,
                }).on("click", goToSpanHref);
              })
            )
            .append(
              person.rooms.map(function (room) {
                return $("<span>", {
                  class: "badge badge-room mr-1",
                  "data-item": JSON.stringify(room),
                  href: "/rooms/" + room.identifier + "/",
                  text: room.building
                    ? room.building + " - " + room.number
                    : room.number,
                  title: room.description,
                }).on("click", goToSpanHref);
              })
            )
        )
    );
}

function buildListAddRoom(room) {
  return $("<a>", {
    href: "#",
    class: "dropdown-item d-flex justify-content-between",
    tabindex: 0,
    "data-item": JSON.stringify(room),
  })
    .on("click", addRoom)
    .append(
      $("<button>", {
        type: "button",
        class: "mr-3 btn btn-room",
        text: room.building ? room.building + " - " + room.number : room.number,
        title: room.description,
      })
    )
    .append(
      $("<small>", {
        text: room.description,
      })
    );
}

function buildListRoom(room) {
  return $("<a>", {
    href: "/rooms/" + room.identifier + "/",
    class: "dropdown-item d-flex justify-content-between",
    tabindex: 0,
    "data-item": JSON.stringify(room),
  })
    .append(
      $("<button>", {
        type: "button",
        class: "mr-3 btn btn-room",
        text: room.building ? room.building + " - " + room.number : room.number,
        title: room.description,
      })
    )
    .append(
      $("<small>", {
        text: room.description,
      })
    );
}

function buildListAddTag(tag) {
  return $("<a>", {
    href: "#",
    class: "dropdown-item d-flex justify-content-between",
    tabindex: 0,
    "data-tag": tag.name,
  })
    .on("click", addTag)
    .append(
      $("<button>", {
        type: "button",
        class: "mr-3 btn btn-tag",
        text: tag.name,
        title: tag.description,
      })
    )
    .append(
      $("<small>", {
        text: tag.description,
      })
    );
}

function buildListTag(tag) {
  return $("<a>", {
    href: "/tags/" + tag.name + "/people/",
    class: "dropdown-item d-flex justify-content-between",
    tabindex: 0,
    "data-tag": tag.name,
  })
    .append(
      $("<button>", {
        type: "button",
        class: "mr-3 btn btn-tag",
        text: tag.name,
        title: tag.description,
      })
    )
    .append(
      $("<small>", {
        text: tag.description,
      })
    );
}

/**
 * Construct a new entry for a multi-valued list
 * @param {Object} data - The object representing the attribute
 * @param {string} field - The name of the field that represents the display value
 * @param {string} [descField] - The name of the field containing the description, if any
 * @param {string} [special] - Type of attribute if not just a text item
 */
function buildMultiAttr(data, field, descField, special) {
  let item = $("<div>", { class: "mb-2" });
  let attr = $("<div>", {
    class: "multi-attribute text-nowrap",
    "data-item": JSON.stringify(data),
  });
  let value = $("<span>", { class: "attr-value" });
  let description = $("<span>", { class: "attr-desc" });

  let buttons = [
    $("<button>", {
      class:
        "btn btn-sm btn-outline-secondary btn-mod-attr hide-on-view border-0",
      title: "Modify this entry",
    })
      .on("click", modifyListItem)
      .append($("<span>", { class: "fas fa-pencil-alt" })),
    $("<button>", {
      class:
        "btn btn-sm btn-outline-secondary btn-del-attr hide-on-view border-0",
      title: "Delete this entry",
    })
      .on("click", deleteListItem)
      .append($("<span>", { class: "far fa-trash-alt" })),
  ];

  // Value field can be more than text sometimes
  if (special === "email") {
    value
      .append(
        $("<a>", {
          href: "mailto:" + data[field],
          title: "Compose email in default mail client",
          text: " " + data[field] + " ",
        })
      )
      .append(
        $("<a>", {
          href:
            "https://webmail.hermes.cam.ac.uk/?_task=mail&_action=compose&_to=" +
            data[field],
          title: "Compose new email in Hermes Webmail",
          target: "_blank",
          text: " ✉ ",
        })
      )
      .append(
        $("<a>", {
          href:
            "https://outlook.office.com/owa/?path=/mail/action/compose&to=" +
            data[field],
          title: "Compose new email in Exchange Online",
          target: "_blank",
        }).append($("<span>", { class: "owa-logo", text: " " }))
      );
  } else if (special === "address") {
    data[field].split(/[\r\n]+/).forEach(function (line) {
      value.append(line).append($("<br>"));
    });
  } else if (special === "url") {
    if (descField !== undefined && data[descField].length !== 0) {
      value.append(
        $("<a>", {
          href: data[field],
          class: "hidden-interact",
          text: data[descField],
        }).append(
          $("<span>", { class: "hidden-interact-popup", text: data[field] })
        )
      );
    } else {
      value.append($("<a>", { href: data[field], text: data[field] }));
    }
    // hidden description added to link
    description.addClass("d-none");
  } else {
    value.text(data[field]);
  }

  // Description field is usually text in brackets
  if (descField !== undefined && data[descField].length !== 0) {
    description.text(" (" + data[descField] + ") ");
  }

  return item.append(attr.append(value).append(description).append(buttons));
}

// Tag deletion button, only works in edit mode
function deleteTag() {
  var mode = $(this).closest("[data-mode]").data("mode");
  if (mode === "edit") {
    var crsid = $("#person-info").data("crsid");
    var tag = $(this).closest(".tag");
    $.ajax({
      type: "DELETE",
      url: "/api/people/" + crsid + "/tags/",
      data: JSON.stringify([tag.data("tag")]),
      contentType: "application/json",
      dataType: "json",
      success: function () {
        tag.remove();
      },
      error: function (e) {
        console.log(e);
      },
    });
    return false; // prevent click going to href
  } else {
    return true; // default action go to href
  }
}

// Manager deletion button, only works in edit mode
function deleteManager() {
  var mode = $(this).closest("[data-mode]").data("mode");
  if (mode === "edit") {
    var crsid = $("#person-info").data("crsid");
    var manager = $(this).closest(".manager");
    $.ajax({
      type: "DELETE",
      url: "/api/people/" + crsid + "/line_managers/",
      data: JSON.stringify(manager.data("crsid")),
      contentType: "application/json",
      dataType: "json",
      success: function () {
        manager.remove();
      },
      error: function (e) {
        console.log(e);
      },
    });
    return false; // prevent click going to href
  } else {
    return true; // default action go to href
  }
}

// Subordinate deletion button, only works in edit mode
function deleteSubordinate() {
  var mode = $(this).closest("[data-mode]").data("mode");
  if (mode === "edit") {
    var crsid = $("#person-info").data("crsid");
    var subordinate = $(this).closest(".subordinate"); // scoped
    $.ajax({
      type: "DELETE",
      url: "/api/people/" + crsid + "/subordinates/",
      data: JSON.stringify(subordinate.data("crsid")),
      contentType: "application/json",
      dataType: "json",
      success: function () {
        subordinate.remove();
      },
      error: function (e) {
        console.log(e);
      },
    });
    return false; // prevent click going to href
  } else {
    return true; // default action go to href
  }
}

// Room delete button, only works in edit mode
function deleteRoom() {
  var mode = $(this).closest("[data-mode]").data("mode");
  if (mode === "edit") {
    var crsid = $("#person-info").data("crsid");
    var room = $(this).closest(".room");
    $.ajax({
      type: "DELETE",
      url: "/api/people/" + crsid + "/rooms/",
      data: JSON.stringify([room.data("item")]),
      contentType: "application/json",
      dataType: "json",
      success: function () {
        room.remove();
      },
      error: function (e) {
        console.log(e);
      },
    });
    return false; // prevent click going to href
  } else {
    return true; // default action go to href
  }
}

// List items deleted when clicking the bin
function deleteListItem() {
  var mode = $(this).closest("[data-mode]").data("mode");
  if (mode === "edit") {
    var crsid = $("#person-info").data("crsid");
    var propertyList = $(this).closest(".multi-property-list");
    var attr = propertyList.data("attr");
    var attribute = $(this).closest(".multi-attribute");
    var item = attribute.data("item");
    $.ajax({
      type: "DELETE",
      url: "/api/people/" + crsid + "/" + attr + "/",
      data: JSON.stringify([item]),
      contentType: "application/json",
      dataType: "json",
      success: function () {
        attribute.parent().remove(); // inside a <p>
      },
    });
  } else {
    return true;
  }
}

/**
 * When user inputs any data into the add attr form, reset the
 * button to "Add" in case it was still inactive or showing success
 * from the last action.
 */
function resetAddButton() {
  var btn = $(this).parent().find("button");
  btn
    .text("Add")
    .addClass("btn-success")
    .removeClass("btn-outline-secondary text-success");
}

/**
 * Modify an existing attribute. Updates the linked property
 * rather than needing to create a new one.
 */
function modifyListItem() {
  let btn = $(this);
  let current = $(this).closest(".multi-attribute");
  let item = current.data("item");
  let crsid = $("#person-info").data("crsid");
  let propertyList = $(this).closest(".multi-property-list");
  let attr = propertyList.data("attr");
  let description = propertyList.data("description");
  let field = propertyList.data("field");
  let descField = propertyList.data("desc-field");
  let special = propertyList.data("special");

  // Input field is always present
  let valueInput = $("<input>", {
    class: "form-control",
    value: item[field],
  });
  // Description field sometimes used
  let descriptionInput = $("<input>", {
    class: "form-control",
    value: item[descField],
  });

  if (special === "email") {
    valueInput.attr("type", "email");
  } else if (special === "url") {
    valueInput.attr("type", "url");
  } else if (special === "address") {
    valueInput = $("<textarea>", {
      class: "form-control",
      text: item[field],
    })
      .autoHeight()
      .height(current.height());
  }

  let modifyForm = $("<form>").append(
    $("<div>", { class: "form-group" })
      .append($("<label>", { text: description }))
      .append(valueInput)
  );

  if (descField) {
    modifyForm.append(
      $("<div>", { class: "form-group" })
        .append($("<label>", { text: "Description" }))
        .append(descriptionInput)
    );
  }

  let updateAction = function () {
    let data = {};
    data[field] = valueInput.val();
    if (descField) {
      data[descField] = descriptionInput.val();
    }
    $.ajax("/api/people/" + crsid + "/" + attr + "/" + item["identifier"], {
      type: "PUT",
      data: JSON.stringify(data),
      contentType: "application/json",
      dataType: "json",
    })
      .done(function (newItem) {
        current
          .parent()
          .replaceWith(buildMultiAttr(newItem, field, descField, special));
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        modalAlert(errorThrown, "Error modifying attribute!");
        return false;
      });
  };
  modalConfirm(updateAction, modifyForm, "Modify value");
}

/**
 * Create a new attribute in a multi attribute field.
 * Add it to the list once it's been created. Should be
 * triggered from a form within a .multi-property-list.
 */
function addListItem() {
  let btn = $(this);
  let crsid = $("#person-info").data("crsid");
  let propertyList = $(this).closest(".multi-property-list");
  let attr = propertyList.data("attr");
  let field = propertyList.data("field");
  let descField = propertyList.data("desc-field");
  let special = propertyList.data("special");
  let attrInput = $("#add-" + attr + "-" + field);
  let formElem = $(this).closest("form");
  let data = {}; // object that is sent to the api
  data[field] = attrInput.val();
  // empty, do nothing
  if (data[field].length === 0) {
    return false;
  }
  if (descField !== undefined) {
    data[descField] = $("#add-" + attr + "-" + descField).val();
  }
  $.ajax("/api/people/" + crsid + "/" + attr + "/", {
    type: "PATCH",
    data: JSON.stringify([data]),
    contentType: "application/json",
    dataType: "json",
  })
    .done(function (values) {
      // only add items that are not already there
      let existingItems = propertyList
        .find(".multi-attribute")
        .map(function () {
          return $(this).data("item").identifier;
        })
        .get();
      values.forEach(function (value) {
        if ($.inArray(value.identifier, existingItems) === -1) {
          formElem.before(buildMultiAttr(value, field, descField, special));
        }
      });
      // Clear the form and focus on the first element
      formElem.find("input, textarea").val("").trigger("input").first().focus();
      // Feedback on submit button
      btn
        .removeClass("btn-success")
        .text("Added ✔")
        .addClass("btn-outline-secondary text-success");
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      modalAlert(errorThrown, "Error adding new attribute!");
    });
  return false;
}

// Add a tag to a person
function addTag() {
  var crsid = $("#person-info").data("crsid");
  var tag = $(this);
  var tagList = $("#personal-tags");
  var tag_name = tag.data("tag");
  $.ajax({
    type: "POST",
    url: "/api/people/" + crsid + "/tags/",
    data: JSON.stringify([tag_name]),
    contentType: "application/json",
    dataType: "json",
    success: function (tags) {
      // only add tags that are not already there
      var existingTags = tagList
        .find(".tag")
        .map(function () {
          return $(this).data("tag");
        })
        .get();
      tags.data.forEach(function (tag) {
        if ($.inArray(tag.name, existingTags) === -1) {
          // new tag
          $("<a>", {
            href: "/tags/" + tag.name + "/people/",
            class: "list-inline-item btn btn-tag my-2 tag",
            "data-tag": tag.name,
            text: tag.name + " ",
            title: tag.description,
          })
            .append(
              $("<button>", {
                class:
                  "btn btn-sm btn-outline-light btn-del-attr hide-on-view border-0",
                title: "Remove tag from profile",
              })
                .on("click", deleteTag)
                .append($("<span>", { class: "far fa-trash-alt" }))
            )
            .insertBefore($("#tag-quick-search"));
        }
      });
    },
    error: function (e) {
      console.log(e);
    },
  });
  return false;
}

function addTagInline() {
  // Add a tag to logged in user from the 👤+ button
  var btn = $(this); // scoped
  var container = btn.parent();
  var crsid = btn.data("user");
  var tag = btn.data("tag");
  $.ajax({
    type: "POST",
    url: "/api/people/" + crsid + "/tags/",
    data: JSON.stringify([tag]),
    contentType: "application/json",
    dataType: "json",
    success: function (tags) {
      // delete button if tag has been added
      if (
        $.inArray(
          tag,
          $.map(tags.data, function (elem) {
            return elem.name;
          })
        ) > -1
      ) {
        btn
          .removeClass("text-muted fas fa-user-plus")
          .addClass("text-success")
          .text("✔");
        setTimeout(function () {
          if (container.hasClass("hidden-interact-popup")) {
            container.remove();
          } else {
            btn.remove();
          }
        }, 3000);
      }
    },
    error: function (e) {
      console.log(e);
    },
  });
  return false;
}

function addRoomInline() {
  // Add a room to logged in user from the 👤+ button
  var btn = $(this); // scoped
  var crsid = btn.data("user");
  var roomIdentifier = btn.data("room-identifier");
  $.ajax({
    type: "PATCH",
    url: "/api/people/" + crsid + "/rooms/",
    data: JSON.stringify([{ identifier: roomIdentifier }]),
    contentType: "application/json",
    dataType: "json",
    success: function (rooms) {
      // delete button if tag has been added
      if (
        $.inArray(
          roomIdentifier,
          $.map(rooms, function (elem) {
            return elem.identifier;
          })
        ) > -1
      ) {
        btn
          .removeClass("text-muted fas fa-user-plus")
          .addClass("text-success")
          .text("✔");
        setTimeout(function () {
          btn.remove();
        }, 3000);
      }
    },
    error: function (e) {
      console.log(e);
    },
  });
  return false;
}

// Create a room and add it to the view
function addRoom() {
  var crsid = $("#person-info").data("crsid");
  var room = $(this);
  var roomList = $("#room-list");
  var data = { identifier: room.data("item").identifier }; // the room
  $.ajax({
    type: "PATCH",
    url: "/api/people/" + crsid + "/rooms/",
    data: JSON.stringify([data]),
    contentType: "application/json",
    dataType: "json",
    success: function (new_rooms) {
      // only add rooms that are not already there
      var existingRooms = roomList
        .find(".room")
        .map(function () {
          return $(this).data("item").identifier;
        })
        .get();
      new_rooms.forEach(function (new_room) {
        if ($.inArray(new_room.identifier, existingRooms) === -1) {
          // new item
          var text = new_room.number;
          if (new_room.building) {
            text = new_room.building + " - " + new_room.number;
          }
          $("<a>", {
            href: "/rooms/" + new_room.identifier + "/",
            class: "list-inline-item btn btn-room room my-1",
            "data-item": JSON.stringify(new_room),
            text: text + " ", // whitespace needed before button
            title: new_room.description,
          })
            .append(
              $("<button>", {
                class:
                  "btn btn-sm btn-outline-dark btn-del-attr hide-on-view border-0",
                title: "Remove room from profile",
              })
                .on("click", deleteRoom)
                .append($("<span>", { class: "far fa-trash-alt" }))
            )
            .insertBefore($("#room-quick-search"));
        }
      });
    },
  });
  return false;
}

function addManager() {
  var crsid = $("#person-info").data("crsid");
  var manager = $(this);
  var manager_crsid = manager.data("crsid");
  var managerList = $("#line-managers");
  $.ajax({
    type: "POST",
    url: "/api/people/" + crsid + "/line_managers/",
    data: JSON.stringify(manager_crsid),
    contentType: "application/json",
    dataType: "json",
    success: function (managers) {
      if (managers.hasOwnProperty("confirmation")) {
        if (managers.confirmation) {
          modalAlert(
            $("<p>")
              .append("Email has been sent to ")
              .append($("<code>", { text: manager_crsid }))
              .append(" to confirm them as your manager."),
            "Add manager"
          );
        } else {
          modalAlert(
            "Error encountered sending confirmation email. " +
              "Please contact IT support for more information.",
            "Error"
          );
        }
      } else {
        // manager has been added without confirmation
        // only add managers that are not already there
        var existingManagers = managerList
          .find(".manager")
          .map(function () {
            return $(this).data("crsid");
          })
          .get();
        managers.forEach(function (new_manager) {
          if ($.inArray(new_manager.crsid, existingManagers) === -1) {
            // new manager
            $("<a>", {
              href: "/people/" + new_manager.crsid + "/",
              class: "list-inline-item btn btn-primary manager my-1",
              "data-crsid": new_manager.crsid,
              text: new_manager.display_name + " ", // whitespace before bin
            })
              .append(
                $("<button>", {
                  class:
                    "btn btn-sm btn-outline-light btn-del-attr border-0 hide-on-view",
                  title: "Remove managerial relationship",
                })
                  .on("click", deleteManager)
                  .append($("<span>", { class: "far fa-trash-alt" }))
              )
              .insertBefore($("#manager-quick-search"));
          }
        });
      }
      $("#manager-quick-search-input").val("").trigger("input");
    },
    error: function (e) {
      console.log(e);
    },
  });
  return false;
}

function addSubordinate() {
  var crsid = $("#person-info").data("crsid");
  var subordinate = $(this);
  var subordinate_crsid = subordinate.data("crsid");
  var subordinateList = $("#subordinates");
  $.ajax({
    type: "POST",
    url: "/api/people/" + crsid + "/subordinates/",
    data: JSON.stringify(subordinate_crsid),
    contentType: "application/json",
    dataType: "json",
    success: function (subordinates) {
      if (subordinates.hasOwnProperty("confirmation")) {
        if (subordinates.confirmation) {
          modalAlert(
            $("<p>")
              .append("Email has been sent to ")
              .append($("<code>", { text: subordinate_crsid }))
              .append(" to confirm you as their manager."),
            "Add manager"
          );
        } else {
          modalAlert(
            "Error encountered sending confirmation email. " +
              "Please contact IT support for more information.",
            "Error"
          );
        }
      } else {
        // subordinate has been added without confirmation
        // only add subordinates that are not already there
        var existingSubordinates = subordinateList
          .find(".subordinate")
          .map(function () {
            return $(this).data("crsid");
          })
          .get();
        subordinates.forEach(function (new_subordinate) {
          if ($.inArray(new_subordinate.crsid, existingSubordinates) === -1) {
            // new subordinate
            $("<a>", {
              href: "/people/" + new_subordinate.crsid + "/",
              class: "list-inline-item btn btn-primary subordinate my-1",
              "data-crsid": new_subordinate.crsid,
              text: new_subordinate.display_name + " ", // whitespace before bin
            })
              .append(
                $("<button>", {
                  class:
                    "btn btn-sm btn-outline-light btn-del-attr border-0 hide-on-view",
                  title: "Remove managerial relationship",
                })
                  .on("click", deleteSubordinate)
                  .append($("<span>", { class: "far fa-trash-alt" }))
              )
              .insertBefore($("#subordinate-quick-search"));
          }
        });
      }
      $("#subordinate-quick-search-input").val("").trigger("input");
    },
    error: function (e) {
      console.log(e);
    },
  });
  return false;
}

function resetSubmitButton() {
  $(this)
    .parent()
    .find("button")
    .text("Update")
    .addClass("btn-success")
    .removeClass("btn-outline-secondary text-success");
}

function updateField() {
  let btn = $(this);
  let personInfo = $("#person-info");
  let tagInfo = $("#tag-info");
  let roomInfo = $("#room-info");
  let newData = {};
  if (personInfo.length) {
    // text info for a person
    let crsid = personInfo.data("crsid");
    let attr = $(this).closest(".single-value-property").data("attr");
    newData[attr] = $("#input-" + attr).val();
    $.ajax({
      type: "PATCH",
      url: "/api/people/" + crsid + "/",
      data: JSON.stringify(newData),
      contentType: "application/json",
      dataType: "json",
      success: function () {
        btn
          .removeClass("btn-success")
          .text("Updated ✔")
          .addClass("btn-outline-secondary text-success");
      },
    });
  } else if (tagInfo.length) {
    // Edit tag in tag view page
    let tagName = tagInfo.data("tag").name;
    newData["name"] = tagInfo.find(".btn-tag").text().clean();
    newData["description"] = tagInfo.find("input").val();
    $.ajax({
      type: "PATCH",
      url: "/api/tags/" + encodeURIComponent(tagName),
      data: JSON.stringify(newData),
      contentType: "application/json",
      dataType: "json",
      success: function (response) {
        if (response.name !== tagName) {
          // tag has been updated! Redirect to new version
          window.location.href =
            "/tags/" + encodeURIComponent(response.name) + "/people";
        }
        btn
          .removeClass("btn-success")
          .text("Updated ✔")
          .addClass("btn-outline-secondary text-success");
      },
      error: function (response) {
        if (response.status === 409) {
          // Tag with this name already exists
          // pop up a warning that disappears when edited
          let tagBtn = tagInfo.find(".btn-tag");
          let errorBox = $("<div>", { class: "position-absolute" })
            .append(
              $("<span>", {
                class: "badge badge-danger",
                text: "Tag name already exists!",
              })
            )
            .css({ top: "-1.2em", left: "-1em" });
          // sits above the tag box
          tagBtn.before(errorBox);
          tagBtn.removeClass("btn-tag").addClass("btn-danger");
          tagBtn.on("input", function () {
            tagBtn.removeClass("btn-danger").addClass("btn-tag");
            errorBox.remove();
          });
        }
      },
    });
  } else if (roomInfo.length) {
    // Edit room in room view page
    let oldData = roomInfo.data("room");
    newData["building"] = $("#room-building-select").val();
    newData["number"] = $("#room-number-input").val();
    newData["description"] = $("#input-room-description").val();
    $.ajax({
      type: "PATCH",
      url: "/api/rooms/" + oldData.identifier,
      data: JSON.stringify(newData),
      contentType: "application/json",
      dataType: "json",
      success: function (response) {
        if (
          response.building !== oldData.building ||
          response.number !== oldData.number
        ) {
          // forced reload to re-apply disabled state
          window.location.reload(true);
        } else {
          btn
            .removeClass("btn-success")
            .text("Updated ✔")
            .addClass("btn-outline-secondary text-success");
        }
      },
    });
  }
  return false; // preventDefault
}

function btnToggle() {
  var crsid = $("#person-info").data("crsid");
  var btn = $(this);
  var sList = btn.closest(".select-value-property");
  var attr = sList.data("attr");
  var field = sList.data("field");
  var linkedService = sList.data("linked-service");
  var data = {};
  var method = "PATCH"; // boolean toggle
  var url = "/api/people/" + crsid + "/";
  if (field) {
    // multi value toggles
    data[field] = btn.val();
    data = [data];
    url += attr + "/";
    if (btn.is(":checked")) {
      method = "PATCH"; // has become checked
    } else {
      method = "DELETE"; // remove if has been unchecked
    }
  } else if (linkedService) {
    data[attr] = btn.is(":checked");
    url += "linked_services/" + linkedService + "/";
  } else {
    data[attr] = btn.is(":checked");
  }
  $.ajax({
    type: method,
    url: url,
    data: JSON.stringify(data),
    contentType: "application/json",
    dataType: "json",
    error: function () {
      // Undo change of state
      btn.prop("checked", !btn.prop("checked"));
    },
  });
}

function sessionLogout() {
  // Deactivate a session using the API method
  var btn = $(this);
  var key = btn.data("session-key");
  var sessionDiv = btn.closest("div.active-session");
  $.ajax({
    type: "DELETE",
    url: "/api/sessions/" + key,
    dataType: "json",
    success: function () {
      if (btn.data("this-session")) {
        // active token, will need to re-login to do anything
        // so go to home
        window.location.href = "/";
      } else {
        // gone, remove from the list
        sessionDiv.remove();
      }
    },
  });
}

function apiKeyGenerate() {
  // the `new` route generates a new api key then redirects
  // to it's page, which returns the information. Generate
  // a new line using the returned info.
  $.ajax({
    type: "GET",
    url: "/api/sessions/new",
    dataType: "json",
    success: function (res) {
      var newKey = $("<div>", { class: "d-flex active-session" })
        .append(
          $("<div>", { class: "mx-2" }).append($("<code>", { text: res.key }))
        )
        .append($("<div>", { class: "mx-2", text: "New!" }))
        .append(
          $("<div>", { class: "mx-2" }).append(
            $("<a>", {
              href: "#",
              class: "badge badge-danger session-logout",
              title: "logout session",
              "data-session-key": res.key,
              text: "logout",
            }).on("click", sessionLogout)
          )
        );
      // place at the top since most recent first
      $("#api-keys").prepend(newKey);
    },
  });
}

/**
 * User status is set using the pre-constructed modal. Process
 * the update when the user clicks the Save button, then update
 * the UI to show the new status and the expiry from the reply.
 */
function saveAvailability() {
  let crsid = $("#person-info").data("crsid");
  let btn = $(this);
  // Get the information from the form
  let indicator = $(
    "#input-availability-indicator [name=indicator]:checked"
  ).val();
  let text = $("#input-availability-text").val();
  let duration =
    Number($("#input-availability-days").val()) * 24 * 60 * 60 +
    Number($("#input-availability-hours").val()) * 60 * 60 +
    Number($("#input-availability-minutes").val()) * 60;
  // Some data validation
  if (!indicator) {
    modalAlert("Select an indicator.", "Error");
    return false;
  } else if (!duration || duration < 0) {
    modalAlert("Duration must be set for availability.", "Error!");
    return false;
  }
  // Set value through the API
  let data = { indicator: indicator, text: text, duration: duration };
  $.ajax("/api/people/" + crsid + "/availability", {
    type: "PUT",
    data: JSON.stringify(data),
    contentType: "application/json",
  })
    .done(function (data) {
      // Success
      if (!data) {
        // Already expired, or cleared
        $("#availability-text").text("");
        $("#availability-indicator")
          .attr("class", "availability-indicator offline")
          .attr("title", null);
        $("#availability-info").attr("title", "Availability unset");
        $("#input-availability-expiry").addClass("d-none");
      } else {
        // Update the UI with set values
        $("#availability-text").text(data.text);
        $("#availability-indicator")
          .attr("class", "availability-indicator " + data.indicator)
          .attr("title", "Availability until " + data.expiry);
        $("#availability-info").attr(
          "title",
          "Availability until " + data.expiry
        );
        $("#input-availability-until").text(data.expiry);
        $("#input-availability-expiry").removeClass("d-none");
      }
      btn.closest(".modal").modal("hide");
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      modalAlert(errorThrown, "Error setting availability!");
      return false;
    });
}

/**
 * Clear the availability information and update the display.
 */
function clearAvailability() {
  let crsid = $("#person-info").data("crsid");
  let btn = $(this);
  $.ajax("/api/people/" + crsid + "/availability", { type: "DELETE" })
    .done(function () {
      $("#availability-text").text("");
      $("#availability-indicator")
        .attr("class", "availability-indicator offline")
        .attr("title", null);
      $("#availability-info").attr("title", "Availability unset");
      $("#input-availability-expiry").addClass("d-none");

      btn.closest(".modal").modal("hide");
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      modalAlert(errorThrown, "Error clearing availability!");
      return false;
    });
}

/**
 * Use one of the suggested availability suggestions to fill in
 * the free text field.
 */
function fillAvailabilitySuggestion() {
  $("#input-availability-text").val($(this).text());
  return false;
}

function importProfile() {
  var crsid = $("#person-info").data("crsid");
  var data = { crsid: crsid, from_lookup_cam: true };
  $.ajax({
    type: "POST",
    url: "/api/people/",
    data: JSON.stringify(data),
    contentType: "application/json",
    dataType: "json",
    success: function () {
      window.location.reload();
    },
    statusCode: {
      403: function () {
        alert("Only administrators can create profiles.");
      },
      400: function () {
        alert("Error importing profile. Does this person exist?");
      },
    },
  });
}

// get suggested tags on-demand
function tagSuggest() {
  // Create a random selection of suggested tags that might be relevant
  var crsid = $("#person-info").data("crsid");
  var output = $("#tag-suggestions");
  $.ajax({
    type: "GET",
    url: "/api/people/" + crsid + "/suggested_tags/",
    data: { limit: 10, sort: "weighted-random" },
    contentType: "application/json",
    success: function (response) {
      output.empty();
      output.append(
        $("<h6>", { class: "list-inline-item", text: "Suggested tags" })
      );
      response["data"].forEach(function (tag) {
        output.append(
          $("<span>", {
            class: "list-inline-item btn btn-outline-tag my-2 tag",
            text: tag.name,
            title: tag.description,
            "data-tag": tag.name,
          })
            .append($("<span>", { class: "fas fa-user-plus ml-2" }))
            .on("click", function () {
              // addTag method works fine since we set data-tag
              addTag.apply(this);
              // but we must remove this element once we're done with it
              this.remove();
            })
        );
      });
    },
    error: function (err) {
      console.log(err);
    },
  });
}

// tag cloud - get ranked list of tags from ajax and
// add to the DOM
function tagCloud(id) {
  if ($(id).length) {
    $.ajax({
      type: "GET",
      url: "/api/tags/",
      data: {
        sort: "count",
        limit: 24,
      },
      dataType: "json",
      success: function (tags) {
        tags.data.forEach(function (tag) {
          $(id).append(
            $("<div>", {
              class: "col-lg-3 col-md-4 col-sm-6 col-xs-2 p-1 text-center",
            }).append(
              $("<a>", {
                class: "btn btn-tag",
                href: "/tags/" + tag.name + "/people/",
                text: tag.name,
              }).append(
                $("<span>", {
                  class: "ml-3 badge badge-secondary",
                  text: tag.people_count,
                })
              )
            )
          );
        });
      },
      error: function (data) {
        console.log(data);
      },
    });
  }
}

/**
 * When an email menu is opened on a page, generate the required
 * links from an API call that retrieves everyone from the query (e.g. tag).
 */
function generateMailtoLinks() {
  let itemName, getPeople;
  let menu = $(this);
  // Don't run again if the menu has already been initialised
  if (menu.data("seen")) {
    return;
  } else {
    menu.data("seen", true);
  }
  // Get the full list of people from the API, each menu item should add a
  // deferred function to process the list of people when it's returned.
  if (menu.data("type") === "tag") {
    // Tag name is used for the API url
    let tag = menu.closest("[data-tag]").data("tag");
    itemName = tag.name;
    getPeople = $.ajax("/api/tags/" + encodeURIComponent(tag.name));
  } else if (menu.data("type") === "room") {
    // Room identifier used for the API url
    let room = menu.closest("[data-room]").data("room");
    itemName = (room.building + " " + room.number).trim();
    getPeople = $.ajax("/api/rooms/" + room.identifier);
  } else {
    return false;
  }
  // Indication of where the compose window appeared from
  let subject =
    "[" + encodeURIComponent(itemName.replace(/[?&]/g, "")) + "@lookup]";
  // Working animations are spinners added at the start of the item
  let working = function (item) {
    let indicator = $("<span>", {
      class: "spinner-border spinner-border-sm mr-2",
      role: "status",
    });
    $(item).prepend(indicator);
    return indicator;
  };
  // Desktop client modal offers a choice between "mailto:" links with
  // different separators (to be compatible with various clients)
  // and a copy-paste list of people (with names) for any client.
  $(".desktop-client-link", menu).each(function () {
    let elem = $(this);
    let spinner = working(elem);
    getPeople.done(function (people) {
      let mailtoComma =
        "mailto:?bcc=" +
        people["data"].map(personToAtCam).join(",") +
        "&subject=" +
        subject;
      let mailtoSemicolon =
        "mailto:?bcc=" +
        people["data"].map(personToAtCam).join(";") +
        "&subject=" +
        subject;
      let copyPaste = people["data"].map(personToEmail).join("; ");
      // Create the modal when clicked
      elem
        .on("click", function () {
          modalAlert(
            $("<ul>", { class: "list-group" })
              .append(
                $("<li>", { class: "list-group-item" })
                  .append(
                    $("<label>", {
                      text: "Outlook compatible",
                    })
                  )
                  .append(
                    $("<div>").append(
                      $("<a>", {
                        href: mailtoSemicolon,
                        text: "mailto: <" + itemName + "@lookup>",
                      })
                    )
                  )
              )
              .append(
                $("<li>", { class: "list-group-item" })
                  .append(
                    $("<label>", {
                      text: "Other clients",
                    })
                  )
                  .append(
                    $("<div>").append(
                      $("<a>", {
                        href: mailtoComma,
                        text: "mailto: <" + itemName + "@lookup>",
                      })
                    )
                  )
              )
              .append(
                $("<li>", { class: "list-group-item" })
                  .append($("<label>", { text: "Copy/Paste list" }))
                  .append(
                    $("<input>", { class: "form-control" })
                      .val(copyPaste)
                      .on("click", function () {
                        this.select();
                      })
                  )
              ),
            "Select the option for your mail client"
          );
        })
        .prop("disabled", false);
      spinner.remove();
    });
  });
  // Link to outlook web compose screen, added as a href attribute
  // once it's been generated. Note the & in the mailto needs to be
  // encoded for the outlook link to work
  $(".outlook-link", menu).each(function () {
    let elem = $(this);
    let spinner = working(elem);
    getPeople.done(function (people) {
      let outlookMailto =
        "https://outlook.office.com/mail/deeplink/compose/?mailtouri=mailto:?bcc=" +
        people["data"].map(personToAtCam).join(";") +
        "%26subject=" +
        subject;
      elem
        .attr("href", outlookMailto)
        .attr("target", "_blank")
        .removeClass("disabled")
        .append(
          $("<span>", { class: "fas fa-external-link-alt ml-2 icon-link" })
        );
      spinner.remove();
    });
  });
  $(".hermes-link", menu).each(function () {
    let elem = $(this);
    let spinner = working(elem);
    getPeople.done(function (people) {
      let hermesMailto =
        "https://webmail.hermes.cam.ac.uk/?_task=mail&_action=compose&_to=mailto:?bcc=" +
        people["data"].map(personToAtCam).join(";") +
        "%26subject=" +
        subject;
      elem
        .attr("href", hermesMailto)
        .attr("target", "_blank")
        .removeClass("disabled")
        .append(
          $("<span>", { class: "fas fa-external-link-alt ml-2 icon-link" })
        );
      spinner.remove();
    });
  });
  // Teams deep links:
  // https://docs.microsoft.com/en-us/microsoftteams/platform/concepts/build-and-test/deep-links
  $(".teams-chat-link", menu).each(function () {
    let elem = $(this);
    let spinner = working(elem);
    getPeople.done(function (people) {
      let teamsChatLink =
        "https://teams.microsoft.com/l/chat/0/0?users=" +
        people["data"].map(personToAtCam).join(",") +
        "&topicName=" +
        subject;
      elem
        .attr("href", teamsChatLink)
        .attr("target", "_blank")
        .removeClass("disabled")
        .append($("<span>", { class: "far fa-comment-alt ml-2 icon-link" }));
      spinner.remove();
    });
  });
  $(".teams-meeting-link", menu).each(function () {
    let elem = $(this);
    let spinner = working(elem);
    getPeople.done(function (people) {
      let teamsMeetingLink =
        "https://teams.microsoft.com/l/meeting/new?attendees=" +
        people["data"].map(personToAtCam).join(",") +
        "&subject=" +
        subject;
      elem
        .attr("href", teamsMeetingLink)
        .attr("target", "_blank")
        .removeClass("disabled")
        .append($("<span>", { class: "far fa-calendar-alt ml-2 icon-link" }));
      spinner.remove();
    });
  });
}

// When the dropdown is changed, set the cookie to change the scope
// for future requests. Clear any scope args from the current page.
function changeScopeSelect(event) {
  // Only get options with a value
  let scope = $(":selected", event.target).attr("value");
  if (scope) {
    document.cookie = "scope=" + scope + "; max-age=43200; path=/";
  } else {
    document.cookie = "scope=local; max-age=0; path=/";
  }
  window.location =
    location.pathname +
    location.search.replace(/[?&]scope=[^&]+/, "").replace(/^&/, "?");
}

// When clicking an institution button set that as the scope and
// follow the link anyway.
function changeScopeBtn(event) {
  let btn = $(event.target);
  let scope = btn.data("instid");
  document.cookie = "scope=" + scope + "; path=/";
  let url = btn.attr("href");
  if (url && url.length > 0) {
    window.location.href = url.toString();
  }
  return false;
}

/**
 * Delete the person currently being viewed. Asks for confirmation
 * with a modal.
 */
function deletePerson() {
  let crsid = $("#person-info").data("crsid");
  let deleteAction = function () {
    $.ajax("/api/people/" + crsid, {
      type: "DELETE",
    })
      .done(function () {
        window.location.reload();
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        modalAlert("Error encountered: " + errorThrown);
      });
  };
  modalConfirm(
    deleteAction,
    $("<p>")
      .append("Are you sure you would like to delete the user: ")
      .append($("<code>", { text: crsid }))
      .append(". All properties and associations will be removed. ")
      .append(
        $("<span>", {
          class: "text-danger",
          text: "This action cannot be undone! ",
        })
      )
      .append(
        "Note that members of included institutions will be " +
          "recreated during lookup.cam synchronisation, " +
          "but the empty profiles can later be hidden."
      ),
    "Confirm deletion"
  );
  return false;
}

jQuery.fn.extend({
  addLookup: function (query) {
    // find how many results we'd get from lookup and
    // append to an element. Add to an existing list.
    var currentList = $(this);
    $.ajax({
      type: "GET",
      url: "/p/www.lookup.cam.ac.uk/api/v1/person/search-count/",
      dataType: "json",
      data: {
        query: query,
        format: "json",
      },
      success: function (result) {
        var resultCount = parseInt(result.result.value);
        var text = "No results found at lookup.cam.ac.uk";
        if (resultCount === 1) {
          text = "See " + resultCount + " result at lookup.cam";
        } else if (resultCount > 1) {
          text = "See " + resultCount + " results at lookup.cam";
        }
        currentList.append(
          $("<a>", {
            href: "https://www.lookup.cam.ac.uk/search?query=" + query,
            target: "_blank",
            class: "list-group-item text-info text-center",
            text: text,
          })
        );
      },
      error: function (error) {
        console.log(error);
      },
    });
  },
});

// textarea resizing to fit contents
jQuery.fn.extend({
  autoHeight: function () {
    function autoHeight_(element) {
      // Height is set to auto first to determine the correct scrollHeight
      // then set with .css to avoid border-box stuff
      return $(element)
        .css("height", "auto")
        .css("overflow-y", "hidden")
        .height(function () {
          let lineHeight = parseInt($(element).css("line-height"));
          if (element.rows <= 1 && element.value.length === 0) {
            // Empty single row is exactly one line tall
            return lineHeight;
          } else {
            return element.scrollHeight + lineHeight / 2;
          }
        });
    }
    return this.each(function () {
      autoHeight_(this).on("input auto:resize", function () {
        autoHeight_(this);
      });
    });
  },
});

// document loaded - preventDefault
$(function () {
  // search people
  $("#quick-search").on("input", function () {
    var query = $("#quick-search").val();
    if (query.length === 0) {
      // hide dropdown box completely
      $("#quick-search-output").removeClass("show");
      return;
    }
    // update where "return" sends us
    $("#quick-search-form").attr("action", "/people+tags/");
    $.ajax({
      type: "GET",
      url: "/api/people/",
      data: {
        query: query,
        limit: 10,
        fuzzy: true,
        full: false, // to get names and CRSid
      },
      dataType: "json",
      success: function (people) {
        var peopleList = $("<div>", {
          class: "dropdown-menu dropdown-menu-right show",
          id: "quick-search-output", // replacing this element
        });
        if (people.data.length === 0) {
          // no results
          peopleList.append(
            $("<a>", {
              href: "#",
              class: "dropdown-item person-list",
              text: "No results!",
            })
          );
        } else if (people.data.length === 1) {
          peopleList.append(buildListPerson(people.data[0], "dropdown-item"));
          // go to person page on submit if only one result,
          // otherwise go to search page
          $("#quick-search-form").attr(
            "action",
            "/people/" + people.data[0].crsid
          );
        } else {
          // build a list of all the people found
          people.data.forEach(function (person) {
            peopleList.append(buildListPerson(person, "dropdown-item"));
          });
        }
        if ($("#quick-search").val() === query) {
          $("#quick-search-output").replaceWith(peopleList);
          // append tags
          $.ajax({
            type: "GET",
            url: "/api/tags/",
            data: {
              query: query,
              limit: 6,
              sort: "count", // count doesn't include tags with 0
            },
            dataType: "json",
            success: function (tags) {
              var tagList = $("<div>", {
                class: "dropdown-item text-center",
                id: "quick-search-tag-results",
              });
              if (tags.data.length !== 0) {
                // do nothing if empty
                tags.data.forEach(function (tag, idx) {
                  if (idx > 0 && idx % 2 === 0) {
                    tagList.append($("<br>"));
                  }
                  tagList.append(
                    $("<a>", {
                      class: "btn btn-tag m-1",
                      href: "/tags/" + tag.name + "/people/",
                      text: tag.name,
                    }).append(
                      $("<span>", {
                        class: "ml-2 badge badge-secondary",
                        text: tag.people_count,
                      })
                    )
                  );
                });
                peopleList.append(tagList);
              }
            },
          });
        }
      },
      error: function (data) {
        console.log(data); // ajax failed
      },
    });
  });

  // search for everything
  $("#omni-search").on("input", function () {
    var query = $("#omni-search").val();
    if (query.length === 0) {
      // hide results box completely
      $("#omni-search-output").empty();
      // Reset button
      $("#omni-search-submit").addClass("disabled").text("Search");
      return;
    }
    // update where "return" sends us
    $("#omni-search-form").attr("action", "/people+tags/");
    $.ajax({
      type: "GET",
      url: "/api/people/",
      data: {
        query: query,
        limit: 10,
        fuzzy: true,
        full: false, // to get names and CRSid
      },
      dataType: "json",
      success: function (people) {
        var peopleList = $("<div>", {
          class: "list-group mb-3",
          id: "omni-search-output", // replacing this element
        });
        if (people.data.length === 0) {
          // no results
          $("#omni-search-submit").addClass("disabled").text("Search");
          peopleList.append(
            $("<a>", {
              href: "#",
              class: "list-group-item person-list text-dark text-center",
              text: "No people found!",
            })
          );
        } else if (people.data.length === 1) {
          $("#omni-search-submit").removeClass("disabled").text("Go");
          peopleList.append(renderPerson(people.data[0]));
          // go to person page on submit if only one result,
          // otherwise go to search page
          $("#omni-search-form").attr(
            "action",
            "/people/" + people.data[0].crsid
          );
        } else {
          // build a list of all the people found
          $("#omni-search-submit").removeClass("disabled").text("Search");
          people.data.forEach(function (person) {
            peopleList.append(buildListPerson(person, "list-group-item"));
          });
          peopleList.append(
            $("<a>", {
              href: "/people/?fuzzy=true&query=" + query,
              class: "list-group-item text-info text-center",
              text: "See all " + people.meta.total + " results",
            })
          );
        }
        if ($("#omni-search").val() === query) {
          // check that results are still relevant
          $("#omni-search-output").replaceWith(peopleList);
          // prepend tags
          $.ajax({
            type: "GET",
            url: "/api/tags/",
            data: {
              query: query,
              limit: 10,
              sort: "count", // count doesn't include tags with 0
            },
            dataType: "json",
            success: function (tags) {
              var tagList = $("<div>", {
                class: "list-group-item text-center",
                id: "omni-search-tag-results",
              });
              if (tags.data.length !== 0) {
                // do nothing if empty
                tags.data.forEach(function (tag) {
                  tagList.append(
                    $("<a>", {
                      class: "btn btn-tag m-1",
                      href: "/tags/" + tag.name + "/people/",
                      text: tag.name,
                    }).append(
                      $("<span>", {
                        class: "ml-2 badge badge-secondary",
                        text: tag.people_count,
                      })
                    )
                  );
                });
                peopleList.prepend(tagList);
              }
            },
          });

          if (query.length >= 2) {
            // no results if less than two characters
            peopleList.addLookup(query);
          }
        }
      },
      error: function (data) {
        console.log(data); // ajax failed
      },
    });
  });

  // search for tags
  $("#tag-search").on("input", function () {
    var query = $("#tag-search").val();
    if (query.length === 0) {
      // hide results box completely
      $("#tag-search-output").removeClass("show").empty();
      // Reset button
      $("#tag-search-submit").addClass("disabled").text("Search");
      return;
    }
    // update where "return" sends us
    $("#tag-search-form").attr("action", "/tags/");
    $.ajax({
      type: "GET",
      url: "/api/tags/",
      data: {
        query: query,
        limit: 10,
        full: false, // to get names and CRSid
      },
      dataType: "json",
      success: function (tags) {
        var tagList = $("<div>", {
          class: "dropdown-menu show",
          id: "tag-search-output", // replacing this element
        });
        if (tags.data.length === 0) {
          // no results
          $("#tag-search-submit").addClass("disabled").text("Search");
          tagList.append(
            $("<a>", {
              href: "#",
              class: "dropdown-item text-dark",
              text: "No results!",
            })
          );
        } else if (tags.data.length === 1) {
          $("#tag-search-submit").removeClass("disabled").text("Go");
          tagList.append(buildListTag(tags.data[0]));
          // go to tag page on submit if only one result,
          // otherwise go to search page
          $("#tag-search-form").attr(
            "action",
            "/tags/" + tags.data[0].name + "/people/"
          );
        } else {
          // build a list of all the tags found
          $("#tag-search-submit").removeClass("disabled").text("Search");
          tags.data.forEach(function (tag) {
            tagList.append(buildListTag(tag));
          });
          tagList.append(
            $("<a>", {
              href: "/tags/?query=" + query,
              class: "dropdown-item text-info text-center",
              text: "See all " + tags.meta.total + " results",
            })
          );
        }
        if ($("#tag-search").val() === query) {
          // check that results are still relevant
          $("#tag-search-output").replaceWith(tagList);
        }
      },
      error: function (data) {
        console.log(data); // ajax failed
      },
    });
  });

  // quick search for rooms
  $("#room-search").on("input", function () {
    var query = $("#room-search").val();
    if (query.length === 0) {
      // hide results box completely
      $("#room-search-output").removeClass("show").empty();
      // Reset button
      $("#room-search-submit").addClass("disabled").text("Search");
      return;
    }
    // update where "return" sends us
    $("#room-search-form").attr("action", "/rooms/");
    $.ajax({
      type: "GET",
      url: "/api/rooms/",
      data: {
        query: query,
        limit: 10,
        full: false, // to get names and CRSid
      },
      dataType: "json",
      success: function (rooms) {
        var roomList = $("<div>", {
          class: "dropdown-menu show",
          id: "room-search-output", // replacing this element
        });
        if (rooms.data.length === 0) {
          // no results
          $("#room-search-submit").addClass("disabled").text("Search");
          roomList.append(
            $("<a>", {
              href: "#",
              class: "dropdown-item text-dark",
              text: "No results!",
            })
          );
        } else if (rooms.data.length === 1) {
          $("#room-search-submit").removeClass("disabled").text("Go");
          roomList.append(buildListRoom(rooms.data[0]));
          // go to room page on submit if only one result,
          // otherwise go to search page
          $("#room-search-form").attr(
            "action",
            "/rooms/" + rooms.data[0].identifier
          );
        } else {
          // build a list of all the rooms found
          $("#room-search-submit").removeClass("disabled").text("Search");
          rooms.data.forEach(function (room) {
            roomList.append(buildListRoom(room));
          });
          roomList.append(
            $("<a>", {
              href: "/rooms/?query=" + query,
              class: "dropdown-item text-info text-center",
              text: "See all " + rooms.meta.total + " results",
            })
          );
        }
        if ($("#room-search").val() === query) {
          // check that results are still relevant
          $("#room-search-output").replaceWith(roomList);
        }
      },
      error: function (data) {
        console.log(data); // ajax failed
      },
    });
  });

  $("#room-building-select").on("change", function () {
    // Trigger an update of search list when changing building
    // for the search line only
    $("#room-quick-search-input").trigger("input");
  });

  $("#room-quick-search-input").on("input focus", function () {
    var query = $(this).val();
    var buildingInput = $("#room-building-select");
    var selectedBuilding;
    var queryUrl = "/api/rooms/";
    var queryData = { query: query, limit: 10 };
    // hide dropdown box completely if nothing to do
    if (query.length === 0) {
      $("#room-quick-search-output").removeClass("show");
      return;
    }
    // check if we are searching within a building
    if (buildingInput.prop("selectedIndex") > 0) {
      // jquery returns text if value is not set
      // have to ensure we use a selected one.
      selectedBuilding = buildingInput.val();
      queryUrl = "/api/buildings/" + selectedBuilding + "/rooms/";
    }
    $.ajax({
      type: "GET",
      url: queryUrl,
      data: queryData,
      dataType: "json",
      success: function (rooms) {
        var roomList = $("<div>", {
          class: "dropdown-menu show",
          id: "room-quick-search-output", // replacing this element
        });
        if (rooms.data.length === 0) {
          roomList.append(
            $("<a>", {
              href: "#",
              class: "dropdown-item",
              text: "No results! Create a new room?",
            })
          );
        } else {
          rooms.data.forEach(function (room) {
            roomList.append(buildListAddRoom(room));
          });
        }
        // Build the "form" to create a new room
        var newRoomBox = $("<div>", {
          class: "dropdown-item mt-3",
          id: "new-room-edit",
        })
          .append(
            $("<div>", { class: "input-group my-1" })
              .append(
                $("<select>", {
                  class: "custom-select custom-room",
                  placeholder: "Tag",
                  id: "new-room-building-select",
                })
                  .on("change", function () {
                    // keep other select in sync
                    var selectedIndex = $(this).prop("selectedIndex");
                    $("#room-building-select").prop(
                      "selectedIndex",
                      selectedIndex
                    );
                    // clear any warning if a room is now selected
                    if (selectedIndex > 0) {
                      $(this)
                        .removeClass("custom-danger")
                        .addClass("custom-room");
                    }
                  })
                  // copy the list of rooms from the other select
                  .append(buildingInput.clone().children())
                  // manually set the selected item (not included with clone)
                  .prop("selectedIndex", buildingInput.prop("selectedIndex"))
              )
              .append(
                $("<input>", {
                  class: "form-control custom-room",
                  title: "new-room-number",
                  placeholder: "Room Number",
                  name: "number",
                  id: "new-room-number",
                  value: query,
                }).on("input", function () {
                  // copy value from search
                  $("#room-quick-search-input").val(this.value);
                })
              )
          )
          .append(
            $("<div>", { class: "input-group my-1" })
              .append(
                $("<input>", {
                  class: "form-control",
                  placeholder: "Description",
                  id: "new-room-description",
                })
              )
              .append(
                $("<div>", { class: "input-group-append" }).append(
                  $("<button>", {
                    class: "btn btn-outline-room text-secondary",
                    text: "New Room",
                  }).on("click", function () {
                    // Create a new room and add it to the list
                    var newRoomBuildingSelect = $("#new-room-building-select");
                    if (newRoomBuildingSelect.prop("selectedIndex") === 0) {
                      // Building has not been selected
                      // required to make a new room so
                      // alert the user
                      newRoomBuildingSelect
                        .removeClass("custom-room")
                        .addClass("custom-danger");
                      return;
                    }
                    var room_building = newRoomBuildingSelect.val();
                    var room_number = $("#new-room-number").val();
                    var room_desc = $("#new-room-description").val();
                    $.ajax({
                      type: "POST",
                      url: "/api/rooms/",
                      data: JSON.stringify([
                        {
                          building: room_building,
                          number: room_number,
                          description: room_desc,
                        },
                      ]),
                      dataType: "json",
                      contentType: "application/json",
                      success: function (data) {
                        if (data.length === 1) {
                          // defer adding to the addRoom function
                          $("<div>")
                            .data("item", data[0])
                            .on("once", addRoom)
                            .trigger("once");
                          $("#room-quick-search-input")
                            .val("")
                            .trigger("input");
                        }
                      },
                      error: function (e) {
                        console.log(e);
                      },
                    });
                  })
                )
              )
          );
        roomList.append(newRoomBox);
        $("#room-quick-search-output").replaceWith(roomList);
        // check bounding box and reposition if necessary
        if (roomList[0].getBoundingClientRect().right > window.innerWidth) {
          roomList.addClass("dropdown-menu-right");
        }
      },
      error: function (data) {
        console.log(data);
      },
    });
  });

  $("#tag-quick-search-input").on("input", function () {
    var query = $(this).val().clean();
    if (query.length === 0) {
      // hide dropdown box completely
      $("#tag-quick-search-output").removeClass("show");
      return;
    }
    $.ajax({
      type: "GET",
      url: "/api/tags/",
      data: {
        query: query,
        limit: 10,
        sort: "name",
        scope: "all",
      },
      dataType: "json",
      success: function (tags) {
        var tagList = $("<div>", {
          class: "dropdown-menu show",
          id: "tag-quick-search-output", // replacing this element
        });
        if (tags.data.length === 0) {
          tagList.append(
            $("<a>", {
              href: "#",
              class: "dropdown-item",
              text: "No results! Create a new tag?",
            })
          );
        } else {
          tags.data.forEach(function (tag) {
            tagList.append(buildListAddTag(tag));
          });
        }
        var newTagBox = $("<div>", {
          class: "dropdown-item input-group",
          id: "new-tag-edit",
        })
          .append(
            $("<input>", {
              class: "form-control badge-tag",
              placeholder: "Tag",
              id: "new-tag-tag",
              value: query,
            }).on("input", function () {
              $("#tag-quick-search-input").val(this.value);
            })
          )
          .append(
            $("<input>", {
              class: "form-control",
              placeholder: "Description",
              id: "new-tag-description",
            })
          )
          .append(
            $("<div>", { class: "input-group-append" }).append(
              $("<button>", {
                class: "btn btn-outline-tag",
                text: "New Tag",
              }).on("click", function () {
                // Create a new tag and add it to the list
                var tag_name = $("#new-tag-tag").val();
                var tag_desc = $("#new-tag-description").val();
                $.ajax({
                  type: "POST",
                  url: "/api/tags/",
                  data: JSON.stringify([
                    { name: tag_name.clean(), description: tag_desc },
                  ]),
                  dataType: "json",
                  contentType: "application/json",
                  success: function (data) {
                    if (data.length === 1) {
                      $("<div>", { "data-tag": data[0].name })
                        .on("once", addTag)
                        .trigger("once");
                      $("#tag-quick-search-input").val("").trigger("input");
                    } else {
                      modalAlert(
                        "Does tag already exist or contain invalid characters?",
                        "Error creating tag!"
                      );
                    }
                  },
                  error: function (e) {
                    console.log(e);
                  },
                });
              })
            )
          );
        tagList.append(newTagBox);
        $("#tag-quick-search-output").replaceWith(tagList);
        // check bounding box and reposition if necessary
        if (tagList[0].getBoundingClientRect().right > window.innerWidth) {
          tagList.addClass("dropdown-menu-right");
        }
      },
      error: function (data) {
        console.log(data);
      },
    });
  });

  // manager search
  $("#manager-quick-search-input").on("input", function () {
    var query = $(this).val();
    if (query.length === 0) {
      // hide dropdown box completely
      $("#manager-quick-search-output").removeClass("show");
      return;
    }
    $.ajax({
      type: "GET",
      url: "/api/people/",
      data: {
        query: query,
        limit: 10,
        fuzzy: true,
        full: false,
      },
      dataType: "json",
      success: function (people) {
        var peopleList = $("<div>", {
          class: "dropdown-menu show",
          id: "manager-quick-search-output", // replacing this element
        });
        if (people.data.length === 0) {
          peopleList.append(
            $("<a>", {
              href: "#",
              class: "dropdown-item",
              text: "No results!",
            })
          );
        } else {
          people.data.forEach(function (person) {
            peopleList.append(
              buildListPerson(person, "dropdown-item")
                .on("click", addManager)
                .data("crsid", person.crsid)
            );
          });
        }
        $("#manager-quick-search-output").replaceWith(peopleList);
      },
      error: function (data) {
        console.log(data);
      },
    });
  });

  // subordinate search
  $("#subordinate-quick-search-input").on("input", function () {
    var query = $(this).val();
    if (query.length === 0) {
      // hide dropdown box completely
      $("#subordinate-quick-search-output").removeClass("show");
      return;
    }
    $.ajax({
      type: "GET",
      url: "/api/people/",
      data: {
        query: query,
        limit: 10,
        fuzzy: true,
        full: false,
      },
      dataType: "json",
      success: function (people) {
        var peopleList = $("<div>", {
          class: "dropdown-menu show",
          id: "subordinate-quick-search-output", // replacing this element
        });
        if (people.data.length === 0) {
          peopleList.append(
            $("<a>", {
              href: "#",
              class: "dropdown-item",
              text: "No results!",
            })
          );
        } else {
          people.data.forEach(function (person) {
            peopleList.append(
              buildListPerson(person, "dropdown-item")
                .on("click", addSubordinate)
                .data("crsid", person.crsid)
            );
          });
        }
        $("#subordinate-quick-search-output").replaceWith(peopleList);
      },
      error: function (data) {
        console.log(data);
      },
    });
  });

  //
  // Render markdown
  //
  // renderer for when javascript is blocked; pass text
  var md = {
    render: function (text) {
      return $("<pre>").text(text);
    },
  };
  try {
    md = markdownit({
      linkify: true,
      typographer: true,
    });
  } catch (e) {
    if (e instanceof ReferenceError) {
      console.log("Unable to load markdown library");
    } else {
      console.log("Encountered unknown error loading markdown library");
    }
  }

  $("div.markdown")
    .on("markdown:refresh", function () {
      var attr = $(this).data("attr");
      $("#rendered-" + attr).html(md.render($("#input-" + attr).val()));
    })
    .trigger("markdown:refresh");

  /** View -> Edit mode switching **/
  $("#edit-toggle").on("click", function () {
    let btn = $(this);
    /* Button is within a view/edit container */
    let context = $(this).closest("[data-mode]");
    if (context.data("mode") === "view") {
      // Convert to edit mode
      context.data("mode", "edit").attr("data-mode", "edit");
      context.find(".readonly-on-view").prop("readonly", false);
      context.find(".disable-on-view").prop("disabled", false);
      context.find(".contenteditable-on-edit").attr("contenteditable", true);
      $("textarea").trigger("auto:resize");
      tagSuggest();
    } else {
      // Convert to view mode
      context.data("mode", "view").attr("data-mode", "view");
      context.find(".readonly-on-view").prop("readonly", true);
      context.find(".disable-on-view").prop("disabled", true);
      context.find(".contenteditable-on-edit").attr("contenteditable", false);
      $("div.markdown").trigger("markdown:refresh");
    }
  });

  // custom file doesn't update; https://github.com/twbs/bootstrap/issues/23994
  $(".custom-file-input").on("change", function () {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });

  $("#photo-upload")
    .on("drag dragstart dragover dragenter", function () {
      $(this).addClass("drag-over");
      return false;
    })
    .on("dragend dragleave drop", function () {
      $(this).removeClass("drag-over");
      return false;
    })
    .on("drop", function (event) {
      if ($("#photo").closest("[data-mode]").data("mode") === "view") {
        return false;
      }
      this.droppedFiles = event.originalEvent.dataTransfer.files;
      $(this).trigger("submit");
    })
    .on("submit", function (evt) {
      evt.preventDefault();
      var reader = new FileReader();
      reader.onload = function () {
        var crsid = $("#person-info").data("crsid");
        var photoData = {};
        photoData["photo"] = this.result.split(",")[1];
        $.ajax({
          url: "/api/people/" + crsid + "/",
          type: "PATCH",
          data: JSON.stringify(photoData),
          contentType: "application/json",
          dataType: "json",
          success: function (person) {
            // noinspection HtmlRequiredAltAttribute
            $("#photo-container")
              .empty()
              .append(
                $("<img>", {
                  class: "rounded right w-100",
                  src: "data:image/jpeg;base64, " + person.photo,
                  alt: "photo",
                  id: "person-photo",
                })
              );
          },
        });
      };
      var inputPhoto = undefined;
      if (this.droppedFiles) {
        inputPhoto = this.droppedFiles[0];
      } else {
        inputPhoto = $("#photo-upload-input")[0].files[0];
      }
      reader.readAsDataURL(inputPhoto);
      this.droppedFiles = undefined;
    });

  $("#photo-delete").on("click", function () {
    let crsid = $("#person-info").data("crsid");
    $.ajax({
      url: "/api/people/" + crsid + "/",
      type: "PATCH",
      data: JSON.stringify({ photo: "" }),
      contentType: "application/json",
      dataType: "json",
      success: function () {
        $("#photo-container")
          .empty()
          .append($("<div>", { class: "fas fa-user photo-placeholder" }));
      },
    });
  });

  // Expand an element to be the size of the contents
  $(".text-expander")
    .on("input", function () {
      var txtBox = $("<span>", { class: "px-3" })
        .css({
          "font-family": $(this).css("font-family"),
          "font-size": $(this).css("font-size"),
          "font-weight": $(this).css("font-weight"),
          visibility: "hidden",
          "white-space": "pre",
        })
        .insertBefore($(this));
      txtBox.text($(this).val() || $(this).prop("placeholder"));
      $(this).css("max-width", txtBox.outerWidth());
      txtBox.remove();
    })
    .trigger("input");

  // delete a room
  $("#delete-room-btn").on("click", function () {
    var doubleCheck = confirm(
      "Are you sure you would like to delete this room?"
    );
    if (doubleCheck === true) {
      var identifier = $(this).data("identifier");
      $.ajax({
        url: "/api/rooms/" + identifier,
        type: "DELETE",
        dataType: "json",
        success: function (room) {
          alert("Deleted room: " + JSON.stringify(room));
          window.location.href = "/rooms/";
        },
      });
    }
  });

  // Update display name as it's changed - input field only appears when
  // editing a person.
  $("#input-display_name").on("input", function () {
    let newDisplayName = this.value;
    // The h3 field in the view
    $("#display-name").text(newDisplayName);
    // Page title is short_name - display_name, so replace everything
    // after a dash. (.* doesn't work ove multiple lines so match all
    // whitespace and all non-whitespace.
    $("title").text(function (idx, prev) {
      return prev.replace(/-[\s\S]*/m, "- " + newDisplayName);
    });
  });

  // links within links
  $("span[href]").on("click", goToSpanHref);

  // links in a select
  $(".select-href").on("change", goToOptionHref);

  // Institution scope selection dropdown
  $("#select-scope").on("change", changeScopeSelect);

  // Institution scope changing buttons when clicked
  $("a[data-instid]").on("click", changeScopeBtn);

  // Remove various items from profile in edit mode
  $("#personal-tags .btn-del-attr").on("click", deleteTag);
  $("#line-managers .btn-del-attr").on("click", deleteManager);
  $("#subordinates .btn-del-attr").on("click", deleteSubordinate);
  $("#room-list .btn-del-attr").on("click", deleteRoom);

  // Editing text fields in edit mode
  $(".editable-field button").on("click", updateField);
  // single value fields change button appearance on edit and submit
  $(".editable-field input, .editable-field textarea").on(
    "input",
    resetSubmitButton
  );

  // Delete or modify an attribute from an attribute list
  $(".multi-attribute .btn-del-attr").on("click", deleteListItem);
  $(".multi-attribute .btn-mod-attr").on("click", modifyListItem);

  // multiple value fields
  $(".btn-add-attr").on("click", addListItem);
  $("div.add-attr input").on("input", resetAddButton);

  $("input.toggle").on("change", btnToggle);

  // Buttons in the availability modal
  $("#save-availability").on("click", saveAvailability);
  $("#clear-availability").on("click", clearAvailability);
  $(".availability-suggestion").on("click", fillAvailabilitySuggestion);

  $("#import-profile").on("click", importProfile);

  $("#person-delete").on("click", deletePerson);

  $(".add-user-tag").on("click", addTagInline);

  $(".add-user-room").on("click", addRoomInline);

  $(".mailto-select").on("show.bs.dropdown", generateMailtoLinks);

  $(".session-logout").on("click", sessionLogout);

  $(".api-key-generate").on("click", apiKeyGenerate);

  $("textarea").autoHeight();

  // generate a tag cloud
  tagCloud("#tag-cloud");
});
