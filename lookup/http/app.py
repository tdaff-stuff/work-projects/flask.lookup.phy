"""
Landing point for application.

Create an application that can be passed to a wsgi server.

Also imports Blueprints from the endpoints and webpages.

"""

from flask import Flask
from flask import render_template, request, jsonify, g

import lookup.config.defaults
from lookup.http.cli import db_cli
from lookup.http.endpoints import api_blueprint
from lookup.http.views import web_blueprint
from lookup.http.resources import resources_blueprint
from lookup.http.proxy import proxy_blueprint
from lookup.interface import config_database
from lookup.util.web import pagination_args, get_current_buildings


def create_app() -> Flask:
    """Create a flask app to run the service."""
    # Flask
    app = Flask(__name__)
    app.url_map.strict_slashes = False  # do not need trailing slash

    # reduce whitespace
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    # set up defaults and apply configuration
    app.config.from_object(lookup.config.defaults)
    app.config.from_envvar("LOOKUP_SETTINGS", silent=True)
    app.config.from_envvar("LOOKUP_SECRETS", silent=True)

    # create config["database"] with a connected engine
    config_database(app)

    @app.context_processor
    def template_globals():
        """
        Inject variables and functions needed by many templates
        """
        # wrap functions in closure to take request
        def get_current_user():
            return g.get("user", None)

        return dict(
            get_current_user=get_current_user,
            get_current_buildings=get_current_buildings,
            pagination_args=pagination_args,
            all_statuses=app.config["STATUSES"],
        )

    @app.errorhandler(401)
    @app.errorhandler(403)
    @app.errorhandler(404)
    def template_error(e):
        """Navigable error page."""
        req_json = (
            request.accept_mimetypes["application/json"]
            > request.accept_mimetypes["text/html"]
        )
        if req_json:
            response = {
                "error": {"code": e.code, "name": e.name, "description": e.description}
            }
            return jsonify(response), e.code
        else:
            # Site required for error pages
            g.site = app.config["SITES"][request.host]
            return render_template("error.html", e=e), e.code

    app.register_blueprint(api_blueprint)
    app.register_blueprint(web_blueprint)
    app.register_blueprint(resources_blueprint)
    app.register_blueprint(proxy_blueprint)

    # Adds migration commands for the database
    app.cli.add_command(db_cli)

    return app
