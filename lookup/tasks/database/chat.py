"""
Links between the database and an external Zulip instance.
"""

import re
import requests

from flask import current_app

from lookup.interface.people import PeopleDatabase
from lookup.tasks import app
from lookup.tasks.notifications.zulip import notify

# Something that looks like a CRSid
re_crsid = re.compile("^(?P<crsid>.+)@cam.ac.uk$")


def _format_url(user_id: str, crsid: str, server: str):
    """Format a Zulip DM url"""
    url = "https://{server}/#narrow/pm-with/{user_id}-{crsid}".format(
        server=server, user_id=user_id, crsid=crsid
    )
    return url


@app.task
def usernames_from_zulip() -> None:
    """
    Scrape all users from chat and add their corresponding
    username to the database.
    """
    # Configuration from flask app context
    config = current_app.config
    zulip_credentials = config["ZULIP_CREDENTIALS"]
    zulip_server = config["ZULIP_SERVER"]

    # Chat is not set up, do nothing
    if not all([zulip_credentials, zulip_server]):
        print("No credentials for chat server, skipping update")
        return

    # Standard format for Zulip API
    # Bot must be generic to get the user list
    url = "https://{}/api/v1/users".format(zulip_server)
    auth = tuple(zulip_credentials.split(":"))

    api_response = requests.get(url=url, auth=auth).json()
    if api_response["result"] != "success":
        print("Chat API error: {}".format(api_response["msg"]))
        return

    # Use database connection initialised by flask
    database = PeopleDatabase(config["database"])

    # Keep track of who's changed
    updated_members = []

    for member in api_response["members"]:
        if member["is_bot"] or not re_crsid.match(member["email"]):
            # not a person we can deal with
            continue

        crsid = re_crsid.match(member["email"]).group("crsid")

        chat_url = _format_url(
            user_id=member["user_id"], crsid=crsid, server=zulip_server
        )

        person = database.get_person(crsid)
        if person is None:
            # Not in lookup.phy
            continue

        current_url = person.linked_services.get("chat", {"url": None})["url"]

        if current_url != chat_url:
            new_services = {"chat": {"url": chat_url}}
            database.update_person(
                crsid, linked_services=new_services, operation="update"
            )
            updated_members.append("{} - {}".format(person.display_name, chat_url))

    count = len(updated_members)
    # Construct a message with links to the pm
    message = "Linked {} people to chat:\n".format(count)
    message += "\n".join(updated_members)
    # notify only if people are updated
    if count:
        print(message)
        notify.delay(message)
