"""
Links between the database and an external GitLab instance.
"""

import re
import requests

from flask import current_app

from lookup.interface.people import PeopleDatabase
from lookup.tasks import app
from lookup.tasks.notifications.zulip import notify

# Something that looks like a CRSid
re_crsid = re.compile("^(?P<crsid>.+)@cam.ac.uk$")


def _get_crsid(member):
    """Get the CRSid using shibboleth identifier, None if not found"""
    crsid = None
    for identity in member["identities"]:
        if identity["provider"] == "shibboleth":
            crsid_match = re_crsid.match(identity["extern_uid"])
            if crsid_match is not None:
                crsid = crsid_match.group("crsid")

    return crsid


@app.task
def usernames_from_gitlab() -> None:
    """
    Scrape all users from a GitLab instance and add their corresponding
    url to the database.
    """
    # Configuration from flask app context
    config = current_app.config
    gitlab_server = config["GITLAB_SERVER"]
    gitlab_token = config["GITLAB_TOKEN"]

    # Gitlab is not set up, do nothing
    if not all([gitlab_server, gitlab_token]):
        print("No credentials for GitLab server, skipping update")
        return

    # Need a personal access token to access api
    url = "https://{}/api/v4/users?per_page=999999".format(gitlab_server)

    api_response = requests.get(url=url, headers={"Private-Token": gitlab_token})

    if api_response.status_code != 200:
        print("GitLab API error: {}".format(api_response.json()["message"]))
        return

    # Ensure celery has the same environment as flask!!
    database = PeopleDatabase(config["database"])

    # Keep track of who's changed
    updated_members = []

    # it's a list of user profiles
    for member in api_response.json():
        # Use shibboleth identity as it will be @cam like Raven
        crsid = _get_crsid(member)
        gitlab_url = member["web_url"]

        if not all([crsid, gitlab_url]):
            # doesn't have all the required information
            continue

        person = database.get_person(crsid)
        if person is None:
            # Not in lookup.phy
            continue

        current_url = person.linked_services.get("gitlab", {"url": None})["url"]

        if current_url != gitlab_url:
            new_services = {"gitlab": {"url": gitlab_url}}
            database.update_person(
                crsid, linked_services=new_services, operation="update"
            )
            updated_members.append("{} - {}".format(person.display_name, gitlab_url))

    count = len(updated_members)
    # Construct a message with links to the pm
    message = "Linked {} people to GitLab:\n".format(count)
    message += "\n".join(updated_members)
    # notify only if people are updated
    if count:
        print(message)
        notify.delay(message)
