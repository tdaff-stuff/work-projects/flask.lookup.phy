"""
General tasks for maintaining the databases and removing cruft.
"""


from typing import Optional

from flask import current_app

from lookup.interface.users import UserDatabase
from lookup.tasks import app, log


@app.task
def expire_login_sessions(
    inactive_days: float = 7,
    max_age_days: float = 31,
    session_type: Optional[str] = "web",
) -> None:
    """
    Call cleanup method of the user session database.
    """
    database = UserDatabase(current_app.config["database"])
    expired_sessions = database.expire_sessions(
        inactive_days=inactive_days,
        max_age_days=max_age_days,
        session_type=session_type,
    )
    print("Expired {} session keys".format(expired_sessions))


@app.task
def sync_tag_count(identifier: int) -> None:
    """
    Update the people count on a tag.

    Parameters
    ----------
    identifier
        Database id of the tag to update
    """
    from lookup.interface import PeopleDatabase

    database = PeopleDatabase(current_app.config["database"])
    try:
        database.sync_tag_count(identifier)
    except AttributeError:
        log.warning("Failed to update tag with id {}".format(identifier))


@app.task
def sync_all_tags() -> None:
    """
    Run the people_count sync and orphan cleanup on the database.
    """
    from lookup.interface import PeopleDatabase

    database = PeopleDatabase(current_app.config["database"])
    database.sync_all_tags()


@app.task
def clean_all_availability_expiry() -> None:
    """
    Run the cleaning task for the expiry properties of availabilities.
    """
    from lookup.interface import PeopleDatabase

    database = PeopleDatabase(current_app.config["database"])
    cleaned = database.clean_availability_expiry()
    if cleaned:
        log.info("Cleaned availability expiry for {} people".format(cleaned))
