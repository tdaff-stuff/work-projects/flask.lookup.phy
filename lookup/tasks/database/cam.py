"""
Tasks that update information in the database.

"""

from typing import Optional, Set, Dict, Any, List, Collection

from flask import current_app
from ibisclient import InstitutionMethods, PersonMethods
from ibisclient import createConnection

from lookup.interface import PeopleDatabaseSources
from lookup.interface.sources import FETCH_PERSON, FETCH_ALL_MEMBERS
from lookup.model.person import Person
from lookup.tasks import app, log
from lookup.tasks.notifications.zulip import notify
from lookup.tasks.notifications.email import goodbye


def check_join_leave(
    person: Optional[Person], changes: Dict[str, Any], institutions: Set[str]
) -> Optional[str]:
    """
    Check if the given set of changes include a change of membership
    either of the university, or an institution.

    For any changes of membership, construct a notification message.
    For users that have moved institution, send the goodbye message.

    Parameters
    ----------
    person
        The user whose details are being checked. Used to construct messages
        to or about them.
    changes
        The reported changes from the update method.
    institutions
        The list of institutions that are covered by the application's
        membership.

    Returns
    -------
    message
        A message formatted as a markdown list item (for Zulip)
    """
    # Catch if the person doesn't exist
    if person is None:
        return None

    # Just a pair of symbols to represent the changes
    icons = [" ", " "]

    cancelled = changes["updated_single_values"].get("cancelled")
    if cancelled is True:
        icons[0] = "X"
    elif cancelled is False:
        icons[0] = "O"

    if "institutions" in changes:
        out_before = set(changes["institutions"]["from"]).isdisjoint(institutions)
        out_after = set(changes["institutions"]["to"]).isdisjoint(institutions)
        if out_after and not out_before:
            # person has moved out from a member institution
            icons[1] = "-"
        elif out_before and not out_after:
            # person has moved into a member institution
            icons[1] = "+"

    if icons == [" ", "-"]:
        # person moved away from a known institution with no change
        # in cancellation, send goodbye message from one of their former
        # institutions.
        for site in current_app.config["SITES"].all():
            if site.instid in changes["institutions"]["from"]:
                goodbye.delay(person.crsid, vars(site), person.display_name)
                break

    # No changes
    if icons == [" ", " "]:
        return None

    # Construct the message with links to profiles for checking info
    return (
        " * **`{icons}`** {name}"
        " - [{crsid}@cam](https://www.lookup.cam.ac.uk/person/crsid/{crsid})"
        " - [{crsid}@lookup](https://lookup.phy.cam.ac.uk/people/{crsid})"
        "".format(icons="".join(icons), name=person.display_name, crsid=person.crsid)
    )


@app.task
def import_from_lookup_cam(update_all: bool = True) -> None:
    """
    Import or update data for all people in known institutions.

    Uses data from the lookup.cam api, pulling in all members of known
    institutions and adding/updating their data.

    Parameters
    ----------
    update_all
        If True, request the job for all other database members after.
    """
    log.info("Importing all data for active institution members lookup.cam.")
    # Database connection configured by flask context
    database = PeopleDatabaseSources(current_app.config["database"])

    # lookup.cam api working with members of institutions
    api = InstitutionMethods(createConnection())

    # All institutions from all included sites
    institutions: Set[str] = set()
    for site in current_app.config["SITES"].all():
        institutions.update(site.institutions)

    # List of users that have been recently checked. Can be passed to the
    # full db scan task to avoid re-checking them immediately.
    seen: Set[str] = set()
    messages: List[str] = []
    for institution in institutions:
        cam_institution = api.getInst(instid=institution, fetch=FETCH_ALL_MEMBERS)
        if cam_institution is None:
            log.warning("Unknown institution {}".format(institution))
            continue
        for person in cam_institution.members:
            if person.identifier.value in seen:
                continue
            # noinspection PyBroadException
            try:
                changes: Dict[str, Any] = {}
                updated = database.update_from_lookup(
                    person, create=True, changes=changes
                )
                change_message = check_join_leave(updated, changes, institutions)
                if change_message is not None:
                    messages.append(change_message)
                seen.add(person.identifier.value)
            except Exception:
                log.warning("Error updating {}".format(person), exc_info=True)

    count = len(messages)
    # Construct a message with links to the profile
    report = "Membership changed for {} entries:\n{}".format(count, "\n".join(messages))
    # Always report the outcome to terminal (celery logging)
    log.info(report)

    # notify only if people are cancelled
    if count:
        notify.delay(report)

    # Also update anyone no longer a member of an active institution
    if update_all:
        # Must pass a list as set causes JSON errors
        update_all_from_lookup_cam.delay(skip=list(seen))


@app.task
def update_all_from_lookup_cam(skip: Optional[Collection[str]] = None) -> None:
    """
    Check all users stored in the database for changed details.

    In the default configuration, every user is queried in the lookup.cam
    API and their details updated with the returned data. Takes about
    50 seconds to do 1400 entries due to all the API calls.

    The preferred method of calling is after the import function, passing
    the list of users that have already been processed. This is used to
    find people that are no longer members of included institutions that
    need to be removed or cancelled since they will not be pulled in from
    the institution based API calls.

    """
    if not skip:
        skip = []

    log.info("Updating all people from lookup.cam API.")
    log.info("Skipping {} people.".format(len(skip)))

    # Database connection configured by flask context
    database = PeopleDatabaseSources(current_app.config["database"])

    # All currently known sites
    all_sites = current_app.config["SITES"].all()

    # All configured institutions to check membership changes
    institutions: Set[str] = set()
    for site in all_sites:
        institutions.update(site.institutions)

    log.info("Using institutions: {}".format(institutions))

    # Lookup API
    api = PersonMethods(createConnection())

    # Keep track of who has been removed
    messages: List[str] = []

    # returns (people, _meta)
    all_people, _meta = database.get_people(sort="crsid", hidden=True, cancelled=True)
    for person in all_people:
        # Can skip people that have already been checked
        if person.crsid in skip:
            continue

        remote = api.getPerson("crsid", person.crsid, fetch=FETCH_PERSON)
        if remote is None:
            log.warning("Unknown person {}".format(person))
            continue

        # noinspection PyBroadException
        try:
            changes: Dict[str, Any] = {}
            updated = database.update_from_lookup(remote, create=False, changes=changes)
            change_message = check_join_leave(updated, changes, institutions)
            if change_message is not None:
                messages.append(change_message)
        except Exception:
            log.warning("Error updating {}".format(person), exc_info=True)

    count = len(messages)
    # Construct a message with links to the profile
    report = "Membership changed for {} entries:\n{}".format(count, "\n".join(messages))
    # Always report the outcome to terminal (celery logging)
    log.info(report)

    # notify only if people are cancelled
    if count:
        notify.delay(report)


@app.task
def update_institutions() -> None:
    """
    Update all known institutions with details from lookup.cam API.

    Retrieve the institution data and pass to the update methods.
    """
    log.info("Updating institution information from lookup.cam")

    # Database connection configured by flask context
    database = PeopleDatabaseSources(current_app.config["database"])

    # Lookup API
    api = InstitutionMethods(createConnection())

    seen = 0
    for institution in database.get_institutions():
        # noinspection PyBroadException
        try:
            remote = api.getInst(institution.instid)

            if remote is None:
                raise KeyError("Unknown institution {}".format(institution))

            database.update_institution_from_lookup(remote)
            seen += 1
        except Exception:
            log.warning("Error updating {}".format(institution), exc_info=True)

    log.info("Checked {} institutions".format(seen))
