"""
Background and non interactive tasks.

Seems like this setup doesn't play well with multiprocessing, so
use threads or gevent Pool option when running celery.
"""
from logging import Logger

from celery import Celery
from celery.schedules import crontab
from celery.utils.log import get_task_logger
from flask import Config

import lookup.config.defaults

# Import configuration in the same way as flask, but don't import
# flask app to prevent circular dependencies. Only used to find the
# broker configuration.
flask_config = Config("")
flask_config.from_object(lookup.config.defaults)
flask_config.from_envvar("LOOKUP_SETTINGS", silent=True)
flask_config.from_envvar("LOOKUP_SECRETS", silent=True)

# Celery app configured here and used to define tasks
app = Celery("tasks", broker=flask_config["BROKER_DB"])
log: Logger = get_task_logger(__name__)

# General configuration of the
app.autodiscover_tasks(
    [
        "lookup.tasks.database.cam",
        "lookup.tasks.database.chat",
        "lookup.tasks.database.gitlab",
        "lookup.tasks.database.housekeeping",
        "lookup.tasks.notifications.zulip",
        "lookup.tasks.notifications.email",
    ]
)

app.conf.beat_schedule = {
    # Execute regularly, it's just one API call
    "periodic-imports": {
        "task": "lookup.tasks.database.cam.import_from_lookup_cam",
        "schedule": 2.3 * 60 * 60,  # seconds
    },
    # Institution information shouldn't change often at all
    "check-institutions": {
        "task": "lookup.tasks.database.cam.update_institutions",
        "schedule": 5.6 * 60 * 60,  # seconds
    },
    # Less frequent as it's a lot of lookup AP calls
    "full-scan": {
        "task": "lookup.tasks.database.cam.update_all_from_lookup_cam",
        "schedule": 8.7 * 60 * 60,  # seconds
    },
    # Every hour; fairly cheap so minimise lag
    "scrape-zulip": {
        "task": "lookup.tasks.database.chat.usernames_from_zulip",
        "schedule": 58.3 * 60,  # every hour
    },
    # Every hour; fairly cheap so minimise lag
    "scrape-gitlab": {
        "task": "lookup.tasks.database.gitlab.usernames_from_gitlab",
        "schedule": 59.2 * 60,
    },
    # Sync tag data (counts) regularly
    "sync-tags": {
        "task": "lookup.tasks.database.housekeeping.sync_all_tags",
        "schedule": 14.23 * 60,
    },
    # Sync tag data (counts) regularly
    "clean-availability-expiry": {
        "task": "lookup.tasks.database.housekeeping.clean_all_availability_expiry",
        "schedule": 21.11 * 60 * 60,
    },
    # Expire login tokens for web users
    "expire-sessions": {
        "task": "lookup.tasks.database.housekeeping.expire_login_sessions",
        "schedule": crontab(hour=0, minute=5),
        "kwargs": {"inactive_days": 7, "max_age_days": 31, "session_type": "web"},
    },
}


# Tasks are designed to make use of a flask context but can't create_app
# yet as it leads to circular dependencies. The first time that we run a
# task, create a fully configured Flask and attach it to the app. Use
# that app to push a new context for each task being run so it can access
# fully configured current_app environment for database, configuration etc.
class FlaskTask(app.Task):
    abstract = True

    def __call__(self, *args, **kwargs):
        # Only create one app and register it on the Celery instance
        if not hasattr(app, "flask"):
            from lookup.http.app import create_app

            app.flask = create_app()

        # Execute each in a separate flask context
        with app.flask.app_context():
            return super().__call__(*args, **kwargs)


# All tasks get a flask context
app.Task = FlaskTask
