"""
Send messages via email.
"""
import smtplib
from email.headerregistry import Address
from email.message import EmailMessage
from typing import Optional

from flask import current_app

from lookup.tasks import app, log


def send_mail(
    msg: EmailMessage,
    site: Optional[dict] = None,
    server: Optional[str] = None,
    bounce_addr: Optional[str] = None,
) -> bool:
    """
    Decorate a message with the "From" header and and
    send it using the outgoing mail server.

    Parameters
    ==========
    msg
        A complete email message with headers already added for
        "Subject" and "To". "From" will be added before sending.
    site
        A dictionary representation of a Site, used to determine
        the display name of the sender. If unset the name defaults
        to "Lookup".
    server
        Address of the SMTP server to use for sending messages.
        If set to None will default to the value configured in
        SMTP_SERVER for the flask app.
    bounce_addr
        If set, messages to unknown email addresses will be
        redirected to the specified address. If unset unknown
        addresses will be ignored

    Returns
    =======
    sent
        True if the mail was sent, False if a problem was
        encountered.
    """
    # SMTP server is configured globally in flask app
    if server is None:
        server = current_app.config["SMTP_SERVER"]
    # Sender can appear site specific
    if site is None:
        display_name = "Lookup"
    else:
        display_name = site["short_name"]
    # Sending address is also a global definition
    sender = current_app.config["EMAIL_SENDER"]

    msg["From"] = Address(display_name=display_name, addr_spec=sender)
    # Will be not valid if bounced, but start as okay
    valid = True

    # debugging, don't spam with bounces
    if server is None:
        log.info(msg)
        # Has been "Sent"
        return valid

    # Check the recipient can be resolved to a real address
    with smtplib.SMTP(server) as s:
        # empty payload required before testing address
        s.mail("")
        # probe for email validity
        if s.rcpt(msg["To"])[0] not in [250, 251]:
            # Make it into a bounce instead.
            new_subject = "Bounce: ({}) {}".format(msg["To"], msg["Subject"])
            if bounce_addr is None:
                log.warning(new_subject)
                return False  # don't bother to bounce, just finish
            # __setitem__ is append for emails so replace headers
            bounce_address = Address("Lookup Bounces", addr_spec=bounce_addr)
            msg.replace_header("Subject", new_subject)
            msg.replace_header("To", bounce_address)
            msg.replace_header("From", bounce_address)
            valid = False

    # Send the message via whichever SMTP server is chosen.
    # Use clean connection as send_message expects it.
    with smtplib.SMTP(server) as s:
        try:
            s.send_message(msg)
        except smtplib.SMTPException:
            # some error when sending mail
            valid = False

    return valid


@app.task
def confirm(
    crsid: str, message: str, url: str, site: dict, name: Optional[str] = None
) -> bool:
    """
    Send a email to confirm an action.

    Parameters
    ----------
    crsid
        User's CRSid, used to make an @cam.ac.uk email address
    message
        The message to describe the action
    url
        Link to the confirmation page, get this from
        url_for('view.confirm', key=key, _external=True)
    site
        The dictionary representation of the site used to initiate
        the request (use `vars()).
    name
        If supplied, this will be added to personalise the contents
        of the email.

    Returns
    -------
    sent
        True if email was sent successfully, False if a problem was
        encountered (e.g. bounced).
    """
    log.info("Sending manager confirmation to {}\n{}".format(crsid, message))
    # Configured template
    templates = current_app.config["EMAIL_TEMPLATES"]["CONFIRM"]
    # Ignore blanked names
    name = (name or "").strip() or crsid

    content = {
        subtype: text.format(name=name, message=message, url=url, site=site)
        for subtype, text in templates.items()
    }

    msg = EmailMessage()
    msg.set_content(content["PLAIN"])
    msg.add_alternative(content["HTML"], subtype="html")
    msg["Subject"] = content["SUBJECT"]
    msg["To"] = Address(name, crsid, "cam.ac.uk")

    # Don't bounce these as might not exist
    return send_mail(msg, site=site)


@app.task(rate_limit="40/h")
def goodbye(crsid: str, site: dict, name: Optional[str] = None) -> bool:
    """
    Send an email to someone that has left the scope of participating
    institutions.

    Apply a rate limit to this particular task in case mass updates cause
    too many emails to be sent at once and leave wiggle room for other
    emails tasks if required.

    Parameters
    ----------
    crsid
        User's CRSid, used to make an @cam.ac.uk email address.
    site
        The site where the user previously had membership. Use
        `vars()` to make it into a dict.
    name
        If supplied, this will be added to personalise the contents
        of the email.

    Returns
    -------
    sent
        True if email was sent successfully, False if a problem was
        encountered (e.g. bounced).
    """
    log.info("Sending goodbye message to {}".format(crsid))
    # Configured template
    templates = current_app.config["EMAIL_TEMPLATES"]["GOODBYE"]
    # Ignore blanked names
    name = (name or "").strip() or crsid

    content = {
        subtype: text.format(name=name, site=site)
        for subtype, text in templates.items()
    }

    msg = EmailMessage()
    msg.set_content(content["PLAIN"])
    msg.add_alternative(content["HTML"], subtype="html")
    msg["Subject"] = content["SUBJECT"]
    msg["To"] = Address(name, crsid, "cam.ac.uk")

    # Don't bounce these as might not exist
    return send_mail(msg, site=site)
