"""
Send messages to a Zulip server.
"""

import requests

from flask import current_app

from lookup.tasks import app


@app.task
def notify(message, subject="lookup"):
    """
    Send a message to the globally defined Zulip server.
    """
    # Configuration from flask app context
    config = current_app.config
    zulip_credentials = config["ZULIP_CREDENTIALS"]
    zulip_server = config["ZULIP_SERVER"]
    zulip_stream = config["ZULIP_STREAM"]

    # No notifications set up, bail
    if not all([zulip_credentials, zulip_server, zulip_stream]):
        return

    # Standard format for Zulip API
    url = "https://{}/api/v1/messages".format(zulip_server)
    auth = tuple(zulip_credentials.split(":"))

    data = {
        "type": "stream",
        "to": zulip_stream,
        "subject": subject,
        "content": message,
    }

    requests.post(url=url, data=data, auth=auth, timeout=3.05)
