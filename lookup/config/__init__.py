"""
Configuration attributes.
"""
from typing import List, Optional, Tuple, Set, Mapping

from ibisclient.connection import createConnection, IbisException
from ibisclient.methods import InstitutionMethods


def children(
    instid: str,
    all_children: Optional[List[str]] = None,
    api: Optional[InstitutionMethods] = None,
) -> List[str]:
    """
    Find all the child institutions of the given institution and add them
    to a list. Also add their children recursively.

    Pulls the list from the lookup.cam with API calls for each institution
    so it's best to cache the result.

    Parameters
    ----------
    instid
        Target institution to be queried.
    all_children
        Working list of children. Modified in place with the results. Leave
        unset to create an empty list at the top level. Pass an empty
        list to include the parent.
    api
        If an API has already been created, it can be reused here.
        If not, one will be created and passed to any recursive calls.

    Returns
    -------
    all_children
        The full ist of all children of the institution.
    """
    # Lookup API
    if api is None:
        api = InstitutionMethods(createConnection())

    # Find the given institution
    institution = api.getInst(instid, fetch="child_insts")

    # If the institution does not exist, then we skip it
    if institution is None:
        return all_children or []

    # Create a working list here if one is not given so a user does not
    # need to supply one.
    if all_children is None:
        all_children = []
    else:
        all_children.append(instid)

    for child in institution.childInsts:
        if not child.cancelled and child.instid not in all_children:
            children(child.instid, all_children=all_children, api=api)

    return all_children


class Site:
    """
    Individual preferences that apply to a single site/institution.
    """

    def __init__(
        self,
        name: str,
        short_name: str,
        instid: str,
        hostname: str,
        logo_text: Tuple[str, str],
        support_email: str,
        administrators: List[str],
        buildings: List[str],
    ) -> None:
        self.name = name
        self.short_name = short_name
        self.instid = instid
        self.hostname = hostname
        self.logo_text = logo_text
        self.support_email = support_email
        self.administrators = administrators
        self.buildings = buildings
        self.__children: Optional[List[str]] = None

    def __lt__(self, other: "Site") -> bool:
        """Sort on the instid."""
        return self.instid < other.instid

    @property
    def children(self) -> List[str]:
        """Recursive list of instid of child institutions."""
        if self.__children is None:
            try:
                self.__children = children(self.instid)
            except IbisException:
                print("Error connecting to lookup.cam API, skipping child institutions")
                self.__children = []
        return self.__children

    @property
    def institutions(self) -> List[str]:
        """The instid of the institution and all children (recursive)."""
        return [self.instid] + self.children


class Sites(dict):
    """
    Manage all sites as a dictionary, using their hostname as the key
    and providing a fallback if the site is not recognised.
    """

    def __init__(self, *args: Mapping[str, Site], default: Site, **kwargs):
        self.__default = default
        super().__init__(*args, **kwargs)

    def __missing__(self, key) -> Site:
        return self.__default

    def all(self) -> Set[Site]:
        """All members, including the default"""
        return set(self.values()).union([self.__default])
