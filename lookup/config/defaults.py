"""
Configuration defaults.

Everything defined here so it can be assumed to exist.

"""

from typing import Optional, List

from lookup.config import Site, Sites


# Databases
LOOKUP_DB = "postgresql://lookup@localhost:5432/lookup"
BROKER_DB = "pyamqp://guest@localhost//"

# Services
ZULIP_CREDENTIALS: Optional[str] = None
ZULIP_SERVER: Optional[str] = None
ZULIP_STREAM: Optional[str] = None

GITLAB_TOKEN: Optional[str] = None
GITLAB_SERVER: Optional[str] = None

SMTP_SERVER: Optional[str] = None
EMAIL_SENDER: Optional[str] = None

SECRET_KEY = "not a very secret key"

MAPS_DIR = "/var/www/lookup/maps/"

site_phy = Site(
    name="Department of Physics",
    short_name="lookup.phy",
    instid="PHY",
    hostname="lookup.phy.cam.ac.uk",
    logo_text=("lookup", ".phy"),
    support_email="it.helpdesk@phy.cam.ac.uk",
    administrators=[],
    buildings=[
        "Mott",
        "Bragg",
        "Link",
        "Rutherford",
        "PoM",
        "Maxwell",
        "Kapitza",
        "MRC",
        "Battcock",
        "External",
    ],
)

# Sites
SITES = Sites(default=site_phy)

# Non site-specific settings
SUPER_ADMINISTRATORS: List[str] = []

STATUSES = ["staff", "student", "visitor"]

AVAILABILITY_SUGGESTIONS = [
    "Available",
    "Office hours",
    "In the lab",
    "Out of office",
    "Working from home",
    "Working remotely",
    "On leave",
    "Sick leave",
    "In a meeting",
    "Do not disturb",
]

# Some fields optionally require confirmation when others
# change their information. Disable this if email is not
# configured
REQUIRE_CONFIRMATION = ["line_managers", "subordinates"]


# Generic email for an action that requires confirmation. Includes
# a link to be clicked. /confirm is login_required so shouldn't get
# triggered by an email service.
_EMAIL_CONFIRM_SUBJECT = "Please confirm a change on {site[short_name]}"

_EMAIL_CONFIRM = """
Dear {name},

Your confirmation has been requested for the following action:

{message}

Please follow the link below to confirm this action:

{url}

If you do not approve the change, do not click the link and
it will expire in 72 hours.

If you have any questions, your local contacts will be happy to help
using their support email: {site[support_email]}.

Regards,

{site[short_name]}
{site[name]}
"""

_EMAIL_CONFIRM_HTML = """
<html>
  <head></head>
  <body>
    <p>Dear {name},</p>
    <p>Your confirmation has been requested for the following action:</p>
    <p>{message}</p>
    <p>Please follow the link below to confirm this action:</p>
    <p><a href="{url}">{url}</a></p>
    <p>If you do not approve the change, do not click the link and
       it will expire in 72 hours.</p>
    <p>If you have any questions, your local contacts will be happy to help
       using their support email:
       <a href="mailto:{site[support_email]}">{site[support_email]}</a>.</p>
    <p>Regards,</p>
    <p>{site[short_name]}<br />
       {site[name]}</p>
  </body>
</html>
"""


# Let a user know their profile is still around if they've left a member
# institution but are not cancelled.
_EMAIL_GOODBYE = """
Dear {name},

As a member of {site[name]} you were given a profile on {site[short_name]}.
The information on lookup.cam indicates that you have now left that
institution but remain a member of the University.

You can view your profile here: https://{site[hostname]}/@me

Your profile will remain active as long as you are a member of the
University. You will no longer appear in searches for a particular
institution, but can be found when viewing the entire organisation.

If you would like to keep your profile, please make sure that the
information correctly reflects your current status.

If you wish to remove your profile, use the "Delete" button at the
bottom of your profile page and all information relating to you will
be purged from the system.

If you have any questions, your previous institution's contacts will
be happy to help using their support email: {site[support_email]}.

Regards,

{site[short_name]}
{site[name]}
"""

_EMAIL_GOODBYE_HTML = """
<html>
  <head></head>
  <body>
    <p>Dear {name},</p>
    <p>As a member of {site[name]} you were given a profile on
       <a href="https://{site[hostname]}">{site[short_name]}</a>. The
       information on lookup.cam indicates that you have now left that
       institution but remain a member of the University.</p>
    <p>You can view your profile here:
       <a href="https://{site[hostname]}/@me">https://{site[hostname]}/@me</a>.
       </p>
    <p>Your profile will remain active as long as you are a member of the
       University. You will no longer appear in searches for a particular
       institution, but can be found when viewing the entire organisation.</p>
    <p>If you would like to keep your profile, please make sure that the
       information correctly reflects your current status.</p>
    <p>If you wish to remove your profile, use the "Delete" button at the
       bottom of your profile page and all information relating to you will
       be purged from the system.</p>
    <p>If you have any questions, your previous institution's contacts will
       be happy to help using their support email:
       <a href="mailto:{site[support_email]}">{site[support_email]}</a>.
    <p>Regards,</p>
    <p>{site[short_name]}<br />
       {site[name]}</p>
  </body>
</html>
"""

_EMAIL_GOODBYE_SUBJECT = "Your profile on {site[short_name]}"


EMAIL_TEMPLATES = {
    "CONFIRM": {
        "PLAIN": _EMAIL_CONFIRM,
        "HTML": _EMAIL_CONFIRM_HTML,
        "SUBJECT": _EMAIL_CONFIRM_SUBJECT,
    },
    "GOODBYE": {
        "PLAIN": _EMAIL_GOODBYE,
        "HTML": _EMAIL_GOODBYE_HTML,
        "SUBJECT": _EMAIL_GOODBYE_SUBJECT,
    },
}
