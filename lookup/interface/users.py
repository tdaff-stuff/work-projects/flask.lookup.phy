"""
Managing the sessions of "logged in" users and Api keys.

"""

from datetime import datetime, timedelta
from typing import Optional, List
from uuid import uuid4

from sqlalchemy import or_

from lookup.model.http import UserSession
from lookup.interface.base import LookupDatabase

SESSION_TYPES = ["web", "api", "impersonation"]


class UserDatabase:
    """
    Database of session keys and their associated users.
    """

    def __init__(self, database: LookupDatabase):
        """Initialise against an active database connection."""
        self.database = database
        self.session = database.Session()

    def __del__(self):
        """Clear any active sessions."""
        self.database.Session.remove()

    def login(
        self,
        user_id: str,
        session_type: str = "web",
        agent: Optional[str] = None,
        address: Optional[str] = None,
    ) -> Optional[str]:
        """
        Generate a new login session for the given user and return
        the session key required to access that session.

        Parameters
        ----------
        user_id
            The identifier for the logged in user (probably a CRSid)
        session_type
            What type of key this is, e.g. a web token or api key
        agent
            User agent of the creating request
        address
            IP address or location of the login request

        Returns
        -------
        key
            The session key used to access this login session
        """
        # Don't do anything without a user
        if not user_id:
            return None

        key = uuid4().hex  # random string
        new_session = UserSession(
            key=key,
            user_id=user_id,
            session_type=session_type,
            created_agent=agent,
            created_address=address,
            accessed_agent=agent,
            accessed_address=address,
        )
        self.session.add(new_session)
        self.session.commit()
        return new_session.key

    def logout(self, key: str) -> None:
        """
        Log out of an open session and remove it from the active
        sessions for that user. Will silently do nothing if the
        session can't be found.

        Parameters
        ----------
        key
            The session/api key to remove

        """
        active_session = self.session.query(UserSession).get(key)

        if active_session is not None:
            self.session.delete(active_session)
            self.session.commit()

        return None

    def get_session(self, key: str) -> Optional[UserSession]:
        """
        Return the full record of the requested session. Will
        return None for a non-existent session.

        Parameters
        ----------
        key
            The key associated with the requested session.

        Returns
        -------
        session
            The session as a UserSession record
        """
        return self.session.query(UserSession).get(key)

    def get_user(
        self,
        key: Optional[str] = None,
        agent: Optional[str] = None,
        address: Optional[str] = None,
    ) -> Optional[str]:
        """
        Return the user associated with the session key. If the key
        is not found, or not given, will return None.

        Parameters
        ----------
        key
            The session key to search for
        agent
            User agent of the accessing request
        address
            IP address or location of the accessing request

        Returns
        -------
        user_id
            The identifier for the user associated with the given key

        """
        # empty key or None
        if not key:
            return None

        active_session = self.session.query(UserSession).get(key)

        if active_session is not None:
            if agent or address:
                active_session.accessed_agent = agent
                active_session.accessed_address = address
                active_session.accessed_datetime = datetime.now()
            self.session.add(active_session)
            self.session.commit()
            return active_session.user_id
        else:
            return None

    def get_sessions(self, user: str, session_type: str = "web") -> List[UserSession]:
        """
        Return the list of all active login sessions for the
        given user.

        Parameters
        ----------
        user
            The user's ID (i.e. CRSid)
        session_type
            The category of sessions to search for
            (i.e. "web", "api" or "impersonation")

        Returns
        -------
        sessions
            All currently active sessions

        """

        sessions = []

        if not user:
            return sessions

        # Logged in through the web
        sessions.extend(
            self.session.query(UserSession)
            .filter(UserSession.user_id == user)
            .filter(UserSession.session_type == session_type)
        )

        return sessions

    def expire_sessions(
        self,
        inactive_days: float = 7,
        max_age_days: float = 31,
        session_type: Optional[str] = "web",
    ) -> int:
        """
        Remove all sessions that are inactive or older than the given
        cutoff. If session_type is not given, it will apply to all
        session types!

        Parameters
        ----------
        inactive_days
            Time in days since the last recorded activity of a
            session to consider it inactive
        max_age_days
            Time in days since the creation of a session to
            consider it old
        session_type
            If given, only process sessions of the given type,
            if None, check all sessions

        Returns
        -------
        expired
            Number of sessions expired

        Raises
        ------
        KeyError
            If an invalid session type is given
        """
        inactive_datetime = datetime.now() - timedelta(days=inactive_days)
        old_datetime = datetime.now() - timedelta(days=max_age_days)

        # Satisfies either criteria
        query = self.session.query(UserSession).filter(
            or_(
                (UserSession.accessed_datetime < inactive_datetime),
                (UserSession.created_datetime < old_datetime),
            )
        )

        # Filter by session type if it is given
        if session_type in SESSION_TYPES:
            query = query.filter(UserSession.session_type == session_type)
        elif session_type is not None:
            raise KeyError("Unknown session type {}".format(session_type))

        expired_count = query.count()
        # Remove individually (bulk delete would be fine, but do
        # individual deletes in case relationships are added later.
        for expired in query:
            self.session.delete(expired)
        self.session.commit()

        return expired_count
