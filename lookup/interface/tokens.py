"""
Database of reset tokens.
"""

from datetime import datetime, timedelta
from typing import Optional, Dict, Any
from uuid import uuid4


from lookup.model.token import Token
from lookup.interface.base import LookupDatabase

DEFAULT_VALIDITY = 72
DEFAULT_USES = 1


class TokenDatabase:
    """
    Database of confirmation keys and associated actions.
    """

    def __init__(self, database: LookupDatabase):
        self.database = database
        self.session = database.Session()

    def __del__(self):
        self.database.Session.remove()

    def generate_token(
        self,
        value: Dict[str, Any],
        valid_hours: Optional[float] = DEFAULT_VALIDITY,
        max_uses: Optional[int] = DEFAULT_USES,
    ) -> Optional[str]:
        """
        Generate a new token and return the id.

        Parameters
        ==========
        value
            Information to be stored.
        valid_hours
            If set, token will remain valid for only for the specified
            length of time.
        max_uses
            If set, will expire the token once it has been accessed
            the set number of times.
        """
        token_key = uuid4().hex

        # collision detection, should be 1 in 10^37 or something improbable
        if self.session.query(Token).get(token_key):
            return self.generate_token(value, valid_hours, max_uses)

        # timed expiry
        if valid_hours is not None and valid_hours > 0:
            expiry = datetime.now() + timedelta(hours=valid_hours)
        else:
            expiry = None

        # usage expiration
        if max_uses is not None and max_uses > 0:
            uses = max_uses
        else:
            uses = None

        new_token = Token(key=token_key, value=value, expiry=expiry, uses=uses)

        self.session.add(new_token)
        self.session.commit()

        return new_token.key

    def get_value(self, key: str) -> Dict[str, Any]:
        """
        Retrieve the value associated with the key.

        Parameters
        ==========
        key
            Hexadecimal key associated with the value.

        Returns
        =======
        value
            The value stored in the database.

        Raises
        ======
        KeyError
            No value associated with the key
        """

        # Pre-remove any expired tokens
        # other methods can assume all remaining tokens are valid
        self.flush_expired_tokens()

        db_token = self.session.query(Token).get(key)

        if db_token is None:
            raise KeyError("Token not found or expired")

        if db_token.uses is not None:
            if db_token.uses <= 1:
                self.session.delete(db_token)
            else:
                db_token.uses -= 1
            self.session.commit()

        return db_token.value

    def flush_expired_tokens(self) -> int:
        """
        Remove all tokens that are expired. Returns number of
        expired tokens.
        """
        expired = (
            self.session.query(Token)
            .filter(Token.expiry.isnot(None))
            .filter(Token.expiry < datetime.now())
            .delete()
        )
        used = (
            self.session.query(Token)
            .filter(Token.uses.isnot(None))
            .filter(Token.uses < 1)
            .delete()
        )

        self.session.commit()

        return expired + used

    def expire_token(self, key: str) -> bool:
        """
        Remove a specific key, even if it has not expired yet.

        Parameters
        ==========
        key
            Hexadecimal key associated with the value to be removed.

        Returns
        =======
        removed
            True if something has been removed.
        """

        expired = self.session.query(Token).filter(Token.key == key).delete()
        self.session.commit()

        return expired
