"""
Outward facing access to the database.

Assume we are dealing with external sources so put
checks for permissions and data validity in here.
"""
from typing import Any, Optional, Union, List, Dict

from flask import url_for, current_app

from lookup.interface.sources import PeopleDatabaseSources
from lookup.interface.tokens import TokenDatabase
from lookup.interface.resources import MapsMixin
from lookup.tasks.notifications import email
from lookup.util.text import clean_string
from lookup.util.web import get_current_site, get_current_institutions
from lookup.util.web import get_current_buildings, scope_buildings


class Api(MapsMixin):
    """
    External interface to people database.

    Wraps SQLAlchemy database and deals with conversion to Python
    JSON-able types.
    """

    # Bulky fields that can be skipped in lightweight responses
    default_exclude = [
        "availability",
        "acl",
        "lookup_cam",
        "photo",
        "profile",
        "linked_services",
    ]

    def __init__(
        self,
        database: PeopleDatabaseSources,
        token_database: TokenDatabase,
        user: Optional[str] = None,
    ) -> None:
        """
        Create an interface to a database.

        Parameters
        ----------
        database
            An instance of a connected PeopleDatabase.
        token_database
            An instance of a connected token management database.
        user
            A CRSid for the user that is accessing the API. If not
            given, permissions will be that of an unprivileged user.

        """
        self.database = database
        self.token_database = token_database
        self.user = user

    def can_edit_crsid(self, crsid: str) -> bool:
        """
        Check if the API user has permissions to edit the current person,
        where person is identified by the crsid of that person.

        Parameters
        ----------
        crsid
            The crsid of the person being edited.

        Returns
        -------
        can_edit
            True if user has edit permissions, otherwise False
        """
        # Simple identity checks
        if self.is_super_admin():
            return True
        elif not self.user:
            return False
        elif self.user == crsid:
            return True

        # Need the person from the database to check institution permissions
        person = self.database.get_person(crsid, create=False)
        if person is None:
            raise KeyError("Permissions check against non existent person")

        # An administrator in any of the Person's institutions can edit,
        # regardless of the site where they're being viewed
        orphan = True  # person is outside the scope of any institution
        institutions = set(institution.instid for institution in person.institutions)
        for site in current_app.config["SITES"].all():
            if not institutions.isdisjoint(site.institutions):
                orphan = False
                if self.user in site.administrators:
                    return True

        # If the person is not in any managed institution then an admin
        # from any institution can modify them.
        if orphan:
            for site in current_app.config["SITES"].all():
                if self.user in site.administrators:
                    return True

        # No edit permissions found so read-only
        return False

    def is_super_admin(self) -> bool:
        """
        Determine if the current user has superuser permissions, and
        can make any site-wide changes.

        Returns
        admin
            True if the Api user is an admin, otherwise False
        """
        return self.user in current_app.config["SUPER_ADMINISTRATORS"]

    @property
    def institutions(self) -> Optional[List[str]]:
        """
        The list of institutions that define the scope of API results for
        the lifetime of the object where no explicit list is given.

        Defaults to the scope defined by the hostname unless told
        something else by args or cookies.

        Returns
        -------
        institutions
            The list of instid values of the API scope.
        """
        return get_current_institutions()

    @property
    def buildings(self) -> Optional[List[str]]:
        """
        The list of buildings that define the scope of the current API
        results.

        Defaults to the scope of the hostname unless args or cookies
        request something else.

        Returns
        -------
        buildings
            The list of building names for the current API scope.
        """
        return get_current_buildings()

    def full_to_exclude(self, full: bool) -> List[str]:
        """
        Conversion between "full" argument for API calls and a
        list of fields to be excluded in `to_dict` calls.

        Will use `self.default_excludes` as the fields when
        not full is requested.

        Parameters
        ----------
        full
            Whether full or not full is required.

        Returns
        -------
        excludes
            A list of fields to exclude that correspond to the full
            or not full description.
        """
        if full:
            return []
        else:
            return self.default_exclude

    def get_people(
        self,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        full: bool = False,
        fuzzy: bool = False,
        sort: Optional[str] = None,
        institutions: Optional[List[str]] = None,
    ) -> Dict[str, Union[dict, List[dict]]]:
        """
        Retrieve a list of people.

        Parameters
        ----------
        query
            Search term to filter results. Searches on names. If not
            set, results will not be filtered.
        limit
            Maximum number of results to return. If not set, all
            results will be returned.
        offset
            Only return results from this index onwards. If omitted,
            start from the beginning.
        full
            Include all of a person's data fields in the output,
            otherwise remove the larger fields (e.g. photo, dicts...)
        fuzzy
            Use a fuzzy search algorithm.
        sort
            Order results according to the specified quantity. Available
            methods are `surname`, `crsid`, `display_name`. If not set,
            results will be ranked by similarity for fuzzy sort or
            default to `surname` for substring search.
        institutions
            If given, limit results to only include the given institutions.
            If unset, defaults to the institutions of the current API
            context; use an an empty list to force a full university
            scope.

        Returns
        -------
        people
            A dictionary that contains a `meta` value that describes
            the query and results, and a `data` value that is a list
            of people as dicts.
        """

        # Unset institutions uses default, empty list is all institutions
        if institutions is None:
            institutions = self.institutions
        elif not institutions:
            institutions = None

        if fuzzy:
            people, meta = self.database.get_people_similarity(
                query=query,
                limit=limit,
                offset=offset,
                sort=sort,
                institutions=institutions,
            )
        else:
            people, meta = self.database.get_people(
                query=query,
                limit=limit,
                offset=offset,
                sort=sort,
                institutions=institutions,
            )

        # Exclude fields if it's not the "full" representation
        exclude = self.full_to_exclude(full)

        content: Dict[str, Union[dict, List[dict]]] = {
            "meta": meta,
            "data": [person.to_dict(exclude=exclude) for person in people],
        }

        return content

    def get_person(self, crsid: str, full: bool = True) -> Optional[dict]:
        """
        Retrieve a single person's entry from the database. If the
        owner of the Api is not authorised

        Parameters
        ----------
        crsid
            The CRSid of the person being retrieved.
        full
            Include all of a person's data fields in the output,
            otherwise remove the larger fields (e.g. photo, dicts...)

        Returns
        -------
        person
            The dictionary representation of the person. Will recurse
            relationship properties to the default depth (1). If person
            is not found, return None instead.

        """
        person = self.database.get_person(crsid)

        # Person not found
        if person is None:
            return None

        # Only visible by self or admins
        if person.hidden or person.cancelled:
            if not self.can_edit_crsid(crsid):
                return None

        person_dict = person.to_dict(exclude=self.full_to_exclude(full))

        return person_dict

    def new_person(
        self, crsid: str, from_lookup_cam: bool = True, **kwargs: Any
    ) -> Optional[dict]:
        """
        Create a new person using the properties supplied.

        Only a super administrator can create a user.

        Parameters
        ----------
        crsid
            The CRSid of the person to be created.
        from_lookup_cam
            Whether to use lookup.cam as the source for initial data.
        kwargs
            Any properties to add to the person, if not sourcing from
            lookup.cam

        Returns
        -------
        person
            The dictionary representation of the person. Will recurse
            relationship properties to the default depth (1). If person
            is not found, return None instead.

        Raises
        ------
        PermissionError
            If current user does not have required privileges to make
            edits for the new person.
        ValueError
            If person already exists
        KeyError
            Person does not exist in lookup.cam
        """
        if not self.is_super_admin():
            # Only admins can force profile creations
            raise PermissionError

        if from_lookup_cam:
            person = self.database.add_from_lookup(crsid)
        else:
            person = self.database.add_person(crsid, **kwargs)

        return person.to_dict()

    def update_person(self, crsid: str, **kwargs) -> Dict:
        """
        Modify a person's details.

        Only the person or an admin can edit their details.

        Parameters
        ----------
        crsid
            The CRSid of the person to be updated.
        kwargs
            Any properties to add to the person

        Returns
        -------
        person
            The dictionary representation of the person. Will recurse
            relationship properties to the default depth (1). If person
            is not found, return None instead.

        Raises
        ------
        PermissionError
            If current user does not have required privileges to make
            edits for the new person.
        """
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        person = self.database.update_person(crsid, **kwargs)

        return person.to_dict()

    def replace_person_attribute(
        self, crsid: str, attr: str, identifier: int, data: dict
    ) -> Dict[str, str]:
        """
        In-place update of an entry in a multi-attribute field.

        The CRSid given must be the owner of the attribute and the account
        making the request must have permission to edit the user.

        Parameters
        ----------
        crsid
            The CRSid of the person that must own the attribute.
        attr
            The name of the attribute type being replaced as it is on
            the person object, e.g. email_addresses.
        identifier
            Identifier in the database of the attribute.
        data
            Full representation of the attribute to be used for the update.

        Returns
        -------
        item
            The updated attribute, converted to a dictionary.
        """
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        updated = self.database.replace_person_attribute(crsid, attr, identifier, data)

        return updated.to_dict()

    # Management hierarchy is managed separately to enable confirmation
    # when creating relationships but not for removing
    def add_person_manager(self, crsid: str, manager: str) -> Union[Dict, bool]:
        """
        Add a manager to a person.

        Admins will add without confirmation, other users require
        confirmation by email from the added party.

        Returns
        -------
        person|sent
            If the manager is added, the person is returned, otherwise
            True is returned for a successful email confirmation being
            sent.
        """

        if self.can_edit_crsid(crsid) and self.can_edit_crsid(manager):
            # Someone with permission to edit both parties can add a
            # relationship without confirmation.
            _db_manager, db_person = self.database.link_manager_subordinate(
                manager=manager, subordinate=crsid
            )
            return db_person.to_dict()
        elif self.can_edit_crsid(crsid):
            # If there is only permission to edit the subordinate, it will
            # require confirmation
            if "line_managers" not in current_app.config["REQUIRE_CONFIRMATION"]:
                # Can also edit without confirmation
                _db_manager, db_person = self.database.link_manager_subordinate(
                    manager=manager, subordinate=crsid
                )
                return db_person.to_dict()

            # Email confirmation required for the change
            (db_manager, person, exists,) = self.database.check_manager_subordinate(
                manager=manager, subordinate=crsid
            )
            # Stop if the action would not be possible
            if db_manager is None or person is None or exists:
                return False
            # Requires confirmation, so generate a token for the action
            # and send an email
            message = (
                "{person.display_name} ({person.crsid}) "
                "has requested for you to be added as their manager."
                "".format(person=person)
            )
            # Success message for person confirming the action
            success = (
                "You ({manager.display_name} ({manager.crsid})) "
                "have been added as a manager of "
                "{person.display_name} ({person.crsid}) "
                "".format(manager=db_manager, person=person)
            )
            # Store the data with the one time key
            token_data = {
                "type": "api",
                "action": "link_manager_subordinate",
                "args": (),
                "kwargs": {"manager": manager, "subordinate": crsid},
                "message": success,
            }
            # Limited use key
            key = self.token_database.generate_token(
                value=token_data, valid_hours=72, max_uses=1
            )
            # Send the information
            email.confirm.delay(
                crsid=manager,
                message=message,
                url=url_for("web.confirm", key=key, _external=True),
                site=vars(get_current_site()),
                name=db_manager.display_name,
            )
            return True
        else:
            # No permissions to edit!
            raise PermissionError

    def add_person_subordinate(self, crsid: str, subordinate: str) -> Union[Dict, bool]:
        """
        Add a subordinate to a person.

        Admins will add without confirmation, other users require
        confirmation by email from the added party.

        Returns
        -------
        person|sent
            If the subordinate is added, the person is returned, otherwise
            True is returned for a successful email confirmation being
            sent.
        """
        if self.can_edit_crsid(crsid) and self.can_edit_crsid(subordinate):
            # Someone with permission to edit both parties can add a
            # relationship without confirmation.
            db_person, _subordinate = self.database.link_manager_subordinate(
                manager=crsid, subordinate=subordinate
            )
            return db_person.to_dict()
        elif self.can_edit_crsid(crsid):
            # If there is only permission to edit the manager, it will
            # require confirmation
            if "subordinates" not in current_app.config["REQUIRE_CONFIRMATION"]:
                # Can also edit without confirmation
                db_person, _subordinate = self.database.link_manager_subordinate(
                    manager=crsid, subordinate=subordinate
                )
                return db_person.to_dict()

            # Email confirmation required for the change
            (person, db_subordinate, exists,) = self.database.check_manager_subordinate(
                manager=crsid, subordinate=subordinate
            )
            # Stop if the action would not be possible
            if person is None or db_subordinate is None or exists:
                return False
            # Requires confirmation, so generate a token for the action
            # and send an email
            message = (
                "{person.display_name} ({person.crsid}) "
                "has requested to add themselves as your manager."
                "".format(person=person)
            )
            # Success message for person confirming the action
            success = (
                "{person.display_name} ({person.crsid}) "
                "has been added as your "
                "({subordinate.display_name} ({subordinate.crsid})) manager."
                "".format(person=person, subordinate=db_subordinate)
            )
            # Store the data with the one time key
            token_data = {
                "type": "api",
                "action": "link_manager_subordinate",
                "args": (),
                "kwargs": {"manager": crsid, "subordinate": subordinate},
                "message": success,
            }
            # Limited use key
            key = self.token_database.generate_token(
                value=token_data, valid_hours=72, max_uses=1
            )
            # Send the information
            email.confirm.delay(
                crsid=subordinate,
                message=message,
                url=url_for("web.confirm", key=key, _external=True),
                site=vars(get_current_site()),
                name=db_subordinate.display_name,
            )
            return True
        else:
            # No permissions to edit!
            raise PermissionError

    def delete_person_manager(self, crsid: str, manager: str) -> Dict:
        """Remove a manager from a person."""
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        _manager, person = self.database.unlink_manager_subordinate(
            manager=manager, subordinate=crsid
        )

        return person.to_dict()

    def delete_person_subordinate(self, crsid: str, subordinate: str) -> Dict:
        """Remove a subordinate from a person."""
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        person, _subordinate = self.database.unlink_manager_subordinate(
            manager=crsid, subordinate=subordinate
        )

        return person.to_dict()

    def set_person_availability(
        self, crsid: str, availability: Optional[Dict[str, Union[str, float]]] = None
    ) -> Dict:
        """
        Set the person's availability fields, as a complete set. An unset
        value will remove the user's availability information.

        The keys should be
        * `indicator` -  the status indicator icon name
        * `text` - the message explaining the status
        * `duration` - time in seconds to keep the status active

        Parameters
        ----------
        crsid
            The identifier for the user to update.
        availability
            A dictionary with the availability data.

        Returns
        -------
        availability
            The availability that has been set.
        """
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        set_availability = self.database.set_person_availability(crsid, availability)

        return set_availability

    def delete_person(self, crsid: str) -> Dict:
        """Remove a person from the database."""
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        deleted_person = self.database.delete_person(crsid)

        if deleted_person is None:
            raise KeyError("User does not exist")

        # deleted objects should not descend into any relationships
        return deleted_person.to_dict(max_depth=0)

    def delete_person_tags(
        self, crsid: str, tags: Optional[List[str]] = None
    ) -> Dict[str, Union[dict, List[dict]]]:
        """Remove tags from a person."""
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        person = self.database.get_person(crsid)

        if person is None:
            raise KeyError("Person does not exist")

        updated_person = self.database.update_person(
            crsid, tags=tags, operation="remove"
        )

        content: Dict[str, Union[dict, List[dict]]] = {
            "meta": {},
            "data": [tag.to_dict(max_depth=0) for tag in updated_person.tags],
        }

        return content

    def add_person_tags(
        self, crsid: str, tags: Optional[List[str]] = None
    ) -> Dict[str, Union[dict, List[dict]]]:
        """Add tags to a person."""
        if not self.can_edit_crsid(crsid):
            raise PermissionError

        person = self.database.get_person(crsid)

        if person is None:
            raise KeyError("Person does not exist")

        updated_person = self.database.update_person(crsid, tags=tags, operation="add")

        content: Dict[str, Union[dict, List[dict]]] = {
            "meta": {},
            "data": [tag.to_dict(max_depth=0) for tag in updated_person.tags],
        }

        return content

    def get_person_suggested_tags(
        self,
        crsid: str,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        sort: Optional[str] = None,
    ) -> Dict[str, Union[dict, List[dict]]]:
        """
        Retrieve a list of tag suggestions for a person based on their
        tag neighbours.

        Parameters
        ----------
        crsid
            Identifier of the person to generate suggested tags for.
        limit
            Maximum number of results to return. If not set, all
            results will be returned.
        offset
            Only return results from this index onwards. If omitted,
            start from the beginning.
        sort
            Order results according to the sorting criteria. Available
            methods are `random`, `weighted-random` and `weighted`. If
            not set, will default to `weighted`.

        Returns
        -------
        tags
            A dictionary that contains a `meta` value that describes
            the query and results, and a `data` value that is a list
            of tags as dicts.
        """

        tags, meta = self.database.get_person_suggested_tags(
            crsid, limit=limit, offset=offset, sort=sort
        )

        content: Dict[str, Union[dict, List[dict]]] = {
            "meta": meta,
            "data": [
                dict(weight=weight, **tag.to_dict(max_depth=0)) for tag, weight in tags
            ],
        }

        return content

    def get_tag(self, name: str) -> Dict:
        """Get a specific tag by name."""
        tag = self.database.get_tag(name)

        if tag is None:
            raise KeyError("Tag does not exist")

        return tag.to_dict(max_depth=0)

    def get_tags(
        self,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        sort: Optional[str] = None,
        institutions: Optional[List[str]] = None,
    ) -> Dict[str, Union[dict, List[dict]]]:
        """
        Get a list of tags that match the given criteria.

        Parameters
        ----------
        query
            Text to search for in the tag name or description.
        limit
            Maximum number of results to return.
        offset
            Start the list of results from the given index.
        sort
            Apply the sorting criteria to the tags.
        institutions
            Limit the search to tags that are only represented within the
            given institution. If unset, defaults to the institutions of
            the current API context; use an an empty list ot force a full
            university scope.

        Returns
        -------
        content
            A dict that has the results as the "data" and the "meta"-data
            that describes the search performed.
        """
        # Unset institutions uses default, empty list is all institutions
        if institutions is None:
            institutions = self.institutions
        elif not institutions:
            institutions = None

        tags, meta = self.database.get_tags(
            query=query,
            limit=limit,
            offset=offset,
            sort=sort,
            institutions=institutions,
        )

        tag_dicts: List[dict] = [tag.to_dict(max_depth=0) for tag in tags]

        content: Dict[str, Union[dict, List[dict]]] = {"meta": meta, "data": tag_dicts}

        return content

    def add_tags(self, taglist: List[Dict[str, str]]) -> List[dict]:
        """
        Create any tags in the list that do not already exist.

        Parameters
        ----------
        taglist
            A list of dictionaries containing the tag names and descriptions
            to go with them.
        Returns
        -------
        created
            The list of new tags that have been created.
        """
        created_tags = []
        for tag in taglist:
            tag_name = clean_string(tag["name"])
            if not self.database.get_tag(tag_name):
                new_tag = self.database.get_tag(
                    name=tag_name, description=tag.get("description"), create=True,
                )
                created_tags.append(new_tag)

        return [tag.to_dict(max_depth=0) for tag in created_tags if tag]

    def update_tag(self, tag: str, **kwargs) -> Dict:
        """Modify the properties of a tag."""
        # TODO: is there a permissions model that works for shared items?
        if not self.is_super_admin():  # Super admins only
            raise PermissionError

        updated_tag = self.database.update_tag(tag, **kwargs)

        if updated_tag is None:
            raise KeyError("Tag does not exist")

        return updated_tag.to_dict(max_depth=0)

    def get_tag_people(
        self,
        tag: str,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        full: bool = False,
        sort: Optional[str] = None,
        institutions: Optional[List[str]] = None,
    ) -> Dict[str, Union[dict, List[dict]]]:
        """
        Get the list of people associated with a tag with the given filters
        applied.

        Parameters
        ----------
        tag
            Text string with the name of the tag.
        limit
            Maximum number of results to return. If not set, all
            results will be returned.
        offset
            Only return results from this index onwards. If omitted,
            start from the beginning.
        full
            Include all of a person's data fields in the output,
            otherwise remove the larger fields (e.g. photo, dicts...)
        sort
            Order results according to the specified quantity. Available
            methods are `surname`, `crsid`, `display_name`. Defaults to
            `surname`.
        institutions
            If given, limit results to only include the given institutions.
            If unset, defaults to the institutions of the current API
            context; use an an empty list ot force a full university
            scope.

        Returns
        -------
        people
            A dictionary that contains a `meta` value that describes
            the query and results, and a `data` value that is a list
            of people as dicts.
        """

        # Unset institutions uses default, empty list is all institutions
        if institutions is None:
            institutions = self.institutions
        elif not institutions:
            institutions = None

        people, meta = self.database.get_tag_people(
            tag, limit=limit, offset=offset, sort=sort, institutions=institutions
        )

        data = [person.to_dict(exclude=self.full_to_exclude(full)) for person in people]

        content: Dict[str, Union[dict, List[dict]]] = {"meta": meta, "data": data}

        return content

    def get_rooms(
        self,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        buildings: Optional[List[str]] = None,
    ) -> Dict[str, Union[dict, List[dict]]]:
        """
        Get a list of rooms matching a query.

        Parameters
        ----------
        query
            Search term to filter results. Searches on names and descriptions.
            If not set, results will not be filtered.
        limit
            Maximum number of results to return. If not set, all
            results will be returned.
        offset
            Only return results from this index onwards. If omitted,
            start from the beginning.
        buildings
            If given, limit results to only include rooms within the given
            buildings. If not set, fall back to the list of buildings for
            the current scope. An empty list will return results for
            all buildings.

        Returns
        -------
        people
            A dictionary that contains a `meta` value that describes
            the query and results, and a `data` value that is a list
            of people as dicts.

        """

        # Unset institutions uses default, empty list is all institutions
        if buildings is None:
            buildings = self.buildings
        elif not buildings:
            buildings = None

        rooms, meta = self.database.get_rooms(
            query, limit=limit, offset=offset, buildings=buildings
        )

        content = {
            "meta": meta,
            "data": [room.to_dict(max_depth=0) for room in rooms],
        }

        return content

    def get_room(self, identifier: int) -> Dict:
        """Get an entry for a specific room."""
        room = self.database.get_room(identifier)

        if room is None:
            raise KeyError("Room does not exist.")

        return room.to_dict(max_depth=0)

    def add_rooms(self, room_list: List[dict]) -> List[dict]:
        """Create a set of new rooms."""
        created_rooms = []
        for room in room_list:
            db_room = self.database.get_room(
                building=room["building"], number=room["number"]
            )
            if db_room is None and room["building"] in scope_buildings("all"):
                new_room = self.database.get_room(
                    number=room["number"],
                    building=room["building"],
                    description=room["description"],
                    create=True,
                )
                created_rooms.append(new_room)

        return [room.to_dict(max_depth=0) for room in created_rooms if room]

    def update_room(self, identifier: int, **kwargs) -> Dict:
        """Modify the properties of a room."""
        # TODO: site based permissions management
        if not self.is_super_admin():  # admins only
            raise PermissionError

        updated_room = self.database.update_room(identifier, **kwargs)

        if updated_room is None:
            raise KeyError("Room does not exist")

        return updated_room.to_dict(max_depth=0)

    def delete_room(self, identifier: int) -> Dict:
        """Delete a room from the database."""
        # TODO: site based permissions management
        if not self.is_super_admin():  # admins only
            raise PermissionError

        deleted_room = self.database.delete_room(identifier)

        if deleted_room is None:
            raise KeyError("Room does not exist")

        return deleted_room.to_dict(max_depth=0)

    def get_room_people(
        self,
        room: int,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        full: bool = False,
        sort: Optional[str] = None,
    ) -> Dict[str, Union[Dict, List[Dict]]]:
        """Search the people that report association with a particular room."""
        people, meta = self.database.get_room_people(
            room, limit=limit, offset=offset, sort=sort
        )

        content = {
            "meta": meta,
            "data": [
                person.to_dict(exclude=self.full_to_exclude(full)) for person in people
            ],
        }

        return content

    def get_building_rooms(
        self,
        name: str,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
    ) -> Dict[str, Union[Dict, List[Dict]]]:
        """Search rooms in a specific building."""
        rooms, meta = self.database.get_building_rooms(
            name, query=query, limit=limit, offset=offset
        )

        content = {
            "meta": meta,
            "data": [room.to_dict(max_depth=0) for room in rooms],
        }

        return content

    def get_building_people(
        self,
        name: str,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        full: bool = False,
        sort: Optional[str] = None,
    ) -> Dict[str, Union[Dict, List[Dict]]]:
        """Search people in a specific building."""
        people, meta = self.database.get_building_people(
            name, limit=limit, offset=offset, sort=sort
        )

        content = {
            "meta": meta,
            "data": [
                person.to_dict(exclude=self.full_to_exclude(full)) for person in people
            ],
        }

        return content

    # Methods that can be invoked from a deferred action on
    # database object.
    _db_actions = ["link_manager_subordinate"]

    def deferred_action(self, data: Dict) -> Any:
        """
        Carry out a deferred action that has been stored, for example
        in a token, to be carried out later.

        Parameters
        ----------
        data
            The description of the action. Must have an "action" key
            and all the required data for the action to be carried
            out successfully.

        Returns
        -------
        output
            Directly returns the output for the command carried out.
        """
        action = data["action"]
        args = data.get("args", ())
        kwargs = data.get("kwargs", {})

        if action in self._db_actions:
            return getattr(self.database, action)(*args, **kwargs)
