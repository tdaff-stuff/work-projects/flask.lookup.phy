"""
Interacting with the database.
"""

from collections import Counter
from datetime import datetime, timedelta
from random import random, shuffle
from typing import Optional, Tuple, Any, List, Dict, Union, Type

from flask import current_app
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql.expression import or_, case
from sqlalchemy.sql.functions import func

from lookup.interface.base import LookupDatabase
from lookup.model.person import Person, Room, Tag, Institution, Status
from lookup.model.person import TelephoneNumber, Title, EmailAddress, Address, URL
from lookup.model.person import AnyAttributeList, AnyAttribute
from lookup.model.person import tag_associations
from lookup.util.image import process_user_image


# Keep to preset values that have CSS icons
AVAILABILITY_INDICATORS = [
    "online",
    "idle",
    "away",
    "offline",
    "unavailable",
    "do-not-disturb",
]


def update_personal_attribute_list(
    existing_items: List,
    changed_items: Union[List[Dict], AnyAttributeList, List[str]],
    item_type: Type[AnyAttribute],
    operation: str = "add",
) -> Optional[AnyAttributeList]:
    """
    Update a list of person-specific attributes.

    Attributes are not shared so will only exist when attached to a
    single person so it's not required to check if these already exist.

    The operation determines what happens to the items. Either add new
    items, remove existing items, or replace all the items.

    Parameters
    ----------
    existing_items:
        The current list of items for the person. Must be the the original
        list (i.e. the SQLAlchemy object), not a copy of the items.
    changed_items:
        The items being operated on. This is either new items to create,
        or existing items to remove. The identifier is required for removal
        and the fields required for creation vary by attribute type.
    item_type:
        Class to create new items in the list.
    operation:
        One of "add" to add new values, "remove" to delete values,
        or "replace" to use the new values as the complete list.
    """
    # Stop immediately if there's nothing to do
    if changed_items is None:
        return None

    # Replace is just adding to an emptied list
    if operation == "replace":
        # remove all existing addresses
        for address in existing_items[:]:
            existing_items.remove(address)

    # Copy of existing items to avoid modifying in-place
    current_items = [item for item in existing_items]

    if operation == "remove":
        # Items must be removed by their specific identifiers.
        # Works for objects or dicts.
        to_remove_identifiers = []
        for item in changed_items:
            if hasattr(item, "identifier"):
                to_remove_identifiers.append(getattr(item, "identifier"))
            elif isinstance(item, dict):
                to_remove_identifiers.append(item.get("identifier"))
        for current_item in current_items:
            if current_item.identifier in to_remove_identifiers:
                existing_items.remove(current_item)

    elif operation in ["add", "replace"]:
        # Items will be constructed from the data, either a dictionary or
        # just a string?
        for item in changed_items:
            db_item = item_type()
            if isinstance(item, dict):
                db_item.update(**item)
            elif isinstance(item, str):
                db_item.update(value=item)
            else:
                db_item.update(**vars(item))
            existing_items.append(db_item)

    return existing_items


class PeopleDatabase:
    """Abstraction querying a database."""

    # Fields to treat as normal single values
    known_fields = [
        "cancelled",
        "display_name",
        "pronouns",
        "headline",
        "hidden",
        "profile",
        "registered_name",
        "first_name",
        "surname",
        "visible_name",
        "lookup_cam",
    ]

    # fields with special handling, will not be in kwargs
    special_fields = [
        "crsid",
        "email_addresses",
        "identifier",
        "institutions",
        "line_managers",
        "linked_services",
        "photo",
        "rooms",
        "status",
        "subordinates",
        "tags",
        "telephone_numbers",
        "titles",
        "addresses",
        "urls",
        "thumbnail",
        "availability_indicator",
        "availability_text",
        "availability_expiry",
    ]

    def __init__(self, database: LookupDatabase):
        """Initialise against an active database connection."""
        self.database = database
        self.session = database.Session()

    def __del__(self):
        """Clear any active sessions."""
        self.database.Session.remove()

    def add_person(self, crsid: str, **kwargs: Any) -> Person:
        """
        Create a new person and add the given data. Does not import data
        from lookup.cam, which will likely get pulled in later during a
        sync. Can create people that do not exist in the lookup.cam
        database (bad). It's probably better to use add_from_lookup to
        initialise a real person from their data.

        Parameters
        ----------
        crsid
            The CRSid of the person to be created.
        kwargs
            Any properties to add to the person

        Returns
        -------
        new_person
            The newly created person

        Raises
        ------
        ValueError
            If person already exists
        """

        # don't do anything if person already exists
        if self.session.query(Person).get(crsid) is not None:
            raise ValueError("Person already exists with CRSid {}".format(crsid))

        person = Person(crsid)
        self.session.add(person)
        self.session.commit()

        return self.update_person(crsid, **kwargs)

    def update_person(
        self,
        crsid: str,
        replace: bool = False,
        create: bool = False,
        operation: str = "add",
        rooms=None,
        tags=None,
        linked_services=None,
        status=None,
        email_addresses=None,
        telephone_numbers=None,
        titles=None,
        addresses=None,
        urls=None,
        photo=None,
        thumbnail=None,
        **kwargs
    ):
        """
        Update an existing person.

        Return None if person does not exist.

        Data is cleaned with the following rules:
            * Unknown values for status are dropped
            * Unknown fields are ignored
            * managers and subordinates are ignored as they should
              be managed using the appropriate methods

        replace
            Replace the existing person with a new person containing only
            the included data.
        create
            If the person does not exist, create them first.
        operation
            For multiple value fields, if 'add' new values will be appended
            to the current values, if 'replace' all existing values will be
            removed before new values are added, if 'remove' then listed
            values will be removed if present.

        """

        person = self.session.query(Person).get(crsid)

        if person is None and create:
            person = Person(crsid)
            self.session.add(person)
        elif person is None:  # doesn't exist
            raise KeyError("Person does not exist: {}.".format(crsid))

        if replace:
            self.session.delete(person)
            person = Person(crsid)

        # process kwargs for all known fields
        for kwarg in kwargs:
            if kwarg in self.known_fields:
                setattr(person, kwarg, kwargs[kwarg])

        # Tags are shared so need to get each of them
        if tags is not None:
            if operation == "replace":
                for tag in person.tags[:]:
                    person.tags.remove(tag)
            if operation == "remove":
                for tag in tags:
                    db_tag = self.get_tag(tag, create=False)
                    if db_tag in person.tags:
                        person.tags.remove(db_tag)
            elif operation in ["add", "replace"]:
                for tag in tags:
                    db_tag = self.get_tag(tag, create=True)
                    person.tags.append(db_tag)

        if status is not None:
            if operation == "replace":
                for this_status in person.status[:]:
                    person.status.remove(this_status)
            if operation == "remove":
                for this_status in status:
                    db_status = self.get_status(this_status, create=False)
                    if db_status in person.status:
                        person.status.remove(db_status)
            elif operation in ["add", "replace"]:
                for this_status in status:
                    db_status = self.get_status(this_status, create=True)
                    if db_status is None:
                        # Don't add if it's a not valid one
                        continue
                    person.status.append(db_status)

        self.update_rooms(person, rooms, operation)
        # Personal attribute lists updated as required
        update_personal_attribute_list(
            existing_items=person.titles,
            changed_items=titles,
            item_type=Title,
            operation=operation,
        )
        update_personal_attribute_list(
            existing_items=person.email_addresses,
            changed_items=email_addresses,
            item_type=EmailAddress,
            operation=operation,
        )
        update_personal_attribute_list(
            existing_items=person.telephone_numbers,
            changed_items=telephone_numbers,
            item_type=TelephoneNumber,
            operation=operation,
        )
        update_personal_attribute_list(
            existing_items=person.addresses,
            changed_items=addresses,
            item_type=Address,
            operation=operation,
        )
        update_personal_attribute_list(
            existing_items=person.urls,
            changed_items=urls,
            item_type=URL,
            operation=operation,
        )
        self.update_linked_services(person, linked_services, operation)

        # don't allow setting the thumbnail manually
        if thumbnail is not None:
            pass

        # Generate a thumbnail when setting the photo
        if photo is not None:
            photo, thumbnail = process_user_image(photo)
            person.photo = photo
            person.thumbnail = thumbnail

        self.session.add(person)
        self.session.commit()

        return person

    def replace_person_attribute(
        self, crsid: str, attr: str, identifier: int, data: dict
    ) -> AnyAttribute:
        """
        In-place update of an entry in a multi-attribute field.

        The entry is replaced completely with the new value and returned
        as whichever type of object it is in the database.

        Parameters
        ----------
        crsid
            The identifier of the person that must own the attribute.
        attr
            The name of the attribute type being replaced as it is on
            the person object, e.g. email_addresses.
        identifier
            Identifier of the the attribute in the database.
        data
            Full representation of the attribute to be used for the update.

        Returns
        -------
        item
            The updated attribute object.
        """
        # Works for any multi-attribute
        obj_type: Type[AnyAttribute]

        if attr == "email_addresses":
            obj_type = EmailAddress
        elif attr == "telephone_numbers":
            obj_type = TelephoneNumber
        elif attr == "titles":
            obj_type = Title
        elif attr == "addresses":
            obj_type = Address
        elif attr == "urls":
            obj_type = URL
        else:
            raise ValueError("Unknown attribute")

        obj = self.session.query(obj_type).get(identifier)

        if obj is None:
            raise KeyError("Unknown attribute.")

        if not obj.person_identifier == crsid:
            raise PermissionError("Wrong attribute owner.")

        # Update methods will only use the known attributes
        obj.update(**data)

        self.session.add(obj)
        self.session.commit()
        return obj

    def link_manager_subordinate(
        self, manager: str, subordinate: str
    ) -> Tuple[Person, Person]:
        """
        Create a manager -> subordinate relationship between two
        people in the database. This assumes that the relationship
        has been agreed by both parties or an admin.

        Parameters
        ----------
        manager
            The CRSid of the manager in the relationship.
        subordinate
            The CRSid of the subordinate in the relationship.

        Returns
        -------
        manager, subordinate
            Both members of the relationship
        """

        # Get from the database
        db_manager = self.session.query(Person).get(manager)
        db_subordinate = self.session.query(Person).get(subordinate)

        if db_manager is None:
            raise KeyError("Manager does not exist.")
        if db_subordinate is None:
            raise KeyError("Subordinate does not exist.")

        if db_manager not in db_subordinate.line_managers:
            db_subordinate.line_managers.append(db_manager)
            self.session.add(db_subordinate)
            self.session.commit()

        return db_manager, db_subordinate

    def check_manager_subordinate(
        self, manager: str, subordinate: str
    ) -> Tuple[Optional[Person], Optional[Person], bool]:
        """
        Check if both people exist in the database and whether they have
        a manager -> subordinate relationship.

        Parameters
        ----------
        manager
            The CRSid of the potential manager.
        subordinate
            The CRSid of the potential subordinate.

        Returns
        -------
        manager, subordinate, exists
            The manager and subordinate entries from the database, if they
            exist and whether they have the given relationship.
        """
        exists = False
        db_manager = self.session.query(Person).get(manager)
        db_subordinate = self.session.query(Person).get(subordinate)

        if db_manager is not None and db_subordinate is not None:
            exists = db_manager in db_subordinate.line_managers

        return db_manager, db_subordinate, exists

    def unlink_manager_subordinate(
        self, manager: str, subordinate: str
    ) -> Tuple[Person, Person]:
        """
        Remove a manager -> subordinate relationship between a pair
        of connected people.

        Parameters
        ----------
        manager
            The CRSid of the manager in the relationship.
        subordinate
            The CRSid of the subordinate in the relationship.

        Returns
        -------
        manager, subordinate
            Both members of the relationship
        """

        # Get from the database
        db_manager = self.session.query(Person).get(manager)
        db_subordinate = self.session.query(Person).get(subordinate)

        if db_manager is None:
            raise KeyError("Manager does not exist.")
        if db_subordinate is None:
            raise KeyError("Subordinate does not exist.")

        if db_manager in db_subordinate.line_managers:
            db_subordinate.line_managers.remove(db_manager)
            self.session.add(db_subordinate)
            self.session.commit()

        return db_manager, db_subordinate

    @staticmethod
    def update_linked_services(
        person: Person,
        linked_services: Optional[Dict[str, Dict]],
        operation: str = "add",
    ):
        """
        Update the linked_services associated with a person. Services are
        uniquely defined by their name which is their dictionary key, and
        have associated data that can vary with the service.



        Parameters
        ----------
        person
            The person from the database to update.
        linked_services
            Dictionary of {"service_name": {"service_data": "values"}}.
        operation
            One of "add" to add new values, "remove" to delete values,
            or "replace" to use the new values as the complete list.
        """
        # Ensure all services have a hidden value
        default = {"hidden": False}

        if linked_services is None:
            # do this before purge so we don't clear by accident
            return None

        if operation == "replace":
            # empty the value
            person.linked_services = {}

        if operation == "remove":
            for service in linked_services:
                linked_services.pop(service, None)
        elif operation == "update":
            for service, data in linked_services.items():
                new_data = person.linked_services.get(service, default.copy())
                new_data.update(data)
                person.linked_services[service] = new_data
        else:  # replace, add
            for service, data in linked_services.items():
                new_data = default.copy()
                new_data.update(data)
                person.linked_services[service] = new_data

    def update_rooms(self, person, rooms, operation="add"):
        """
        Update the list of room numbers. Rooms can have a building and
        a description; one room can be shared with multiple people.

        Parameters
        ----------
        person: Person
            The person from the database to update.
        rooms: (list of dict) | (list of Room) | (list of str)
            List of new rooms, accepted formats are dicts or Room like
            objects. If an identifier field is present, it will be
            used to find a known room, otherwise building and number
            are used to find an existing room or add a new one. Plain str
            will be converted to a number with no building.
        operation: str
            One of "add" to add new values, "remove" to delete values,
            or "replace" to use the new values as the complete list.
        """
        if rooms is None:
            # do this before purge so we don't clear numbers by accident
            return None

        if operation == "replace":
            # remove all existing addresses
            for room in person.rooms[:]:
                person.rooms.remove(room)

        for src_room in rooms:
            if hasattr(src_room, "identifier"):  # exact room
                db_room = self.get_room(src_room.identifier)
            elif hasattr(src_room, "keys") and "identifier" in src_room:
                db_room = self.get_room(src_room["identifier"])
            elif hasattr(src_room, "number"):  # room by description object
                db_room = self.get_room(
                    number=getattr(src_room, "number", None),
                    building=getattr(src_room, "building", None),
                    description=getattr(src_room, "description", None),
                    create=True,
                )
            elif hasattr(src_room, "keys") and "number" in src_room:  # dict
                db_room = self.get_room(
                    number=src_room.get("number", None),
                    building=src_room.get("building", None),
                    description=src_room.get("description", None),
                    create=True,
                )
            else:
                db_room = self.get_room(number=src_room, create=True)

            if db_room is not None:
                if operation == "remove":
                    if db_room in person.rooms:
                        person.rooms.remove(db_room)
                elif operation in ["add", "replace"]:
                    # duplicates don't matter so can just append
                    person.rooms.append(db_room)

        return person.rooms

    def update_institutions(
        self,
        person: Person,
        institutions: List[Union[str, Institution, dict]],
        operation: str = "replace",
    ) -> List[Institution]:
        """
        Set the institution list for the given person. Only supports
        complete replacement of the list.

        Parameters
        ----------
        person
            The person whose institution list will be set.
        institutions
            The list of institutions to add the user to. Should be the short
            codes, the actual institutions, or a list of the dictionary
            representations of them.
        operation
            Only valid as "replace" to update the whole list.

        Returns
        -------
        institutions
            The final list of institutions.
        """

        if operation != "replace":
            raise NotImplementedError

        # Get the list of database objects, creation is fine as this only
        # gets called from scraping functions
        new_institutions = [
            self.get_institution(institution, create=True)
            for institution in institutions
            if institution
        ]

        # Set directly as the user's associations
        person.institutions = new_institutions

        self.session.add(person)
        self.session.commit()

        return person.institutions

    def set_person_availability(
        self, crsid: str, availability: Optional[Dict[str, Union[str, float]]]
    ) -> Dict[str, Union[str, datetime]]:
        """
        Set the availability of a person using the given data. Only
        supports setting the set of values as a whole (or removing them
        all). The incoming data should have a duration, in seconds,
        from which the expiry will be derived and returned.

        Parameters
        ----------
        crsid
            Identifier for the person to update.
        availability
            Complete set of availability information to use to update
            the person. Based on duration of the status, not expiry.

        Returns
        -------
        availability
            The availability information that has been set, including
            the expiry information.
        """
        if not crsid:
            raise ValueError("Invalid CRSid.")

        person = self.session.query(Person).get(crsid)

        if person is None:
            raise KeyError("Person does not exist.")

        if availability is None:
            # Unset all the availability info
            person.availability_indicator = None
            person.availability_text = None
            person.availability_expiry = None
        else:
            # Some validation of the data
            indicator = availability.get("indicator")
            if indicator not in AVAILABILITY_INDICATORS:
                raise ValueError("Invalid indicator.")
            text = availability.get("text")  # Can be null
            # Duration is in seconds. Clear the microseconds so that they are
            # not printed when using the date in templates.
            duration = float(availability.get("duration", 0))
            expiry = datetime.now().replace(microsecond=0) + timedelta(seconds=duration)
            # Set all the values
            person.availability_indicator = indicator
            person.availability_text = text
            person.availability_expiry = expiry

        self.session.commit()

        return person.availability

    def clean_availability_expiry(self) -> int:
        """
        Check everyone in the database and see if their availability
        expiry has passed and remove the date if it has.

        Means that an existence check can be used instead of the date
        on the availability hybrid_property when the data is very
        stale.

        Returns
        -------
        cleaned
            Number of people that have had their expiry cleaned.
        """
        cleaned = (
            self.session.query(Person)
            .filter(Person.availability_expiry < datetime.now())
            .update({Person.availability_expiry: None}, synchronize_session=False)
        )
        self.session.commit()
        return cleaned

    def get_person(self, crsid, create=False):
        """
        Get a person. Option to create one if they don't exist.
        """

        if not crsid:
            return None

        # Get a person by crsid == identifier
        person = self.session.query(Person).get(crsid)

        if person is None and create:
            person = Person(crsid)
            self.session.add(person)
        elif person is None:
            return None  # doesn't exist!

        self.session.commit()
        return person

    def delete_person(self, crsid: str) -> Optional[Person]:
        """
        Remove a person.
        """

        person = self.session.query(Person).get(crsid)

        if person is None:
            return None

        self.session.delete(person)
        self.session.commit()
        return person

    def get_people_similarity(
        self,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        sort: Optional[str] = None,
        institutions: Optional[List[str]] = None,
    ) -> Tuple[List[Person], Dict[str, Any]]:
        """
        Get a list of people that match the given criteria, using a fuzzy
        search.
        """

        meta: Dict[str, Any] = {"query": query, "fuzzy": True, "sort": sort}

        similarity_score = func.greatest(
            func.word_similarity(query, Person.display_name),
            func.word_similarity(query, Person.registered_name),
            func.similarity(query, Person.identifier),
            func.similarity(query, Person.first_name),
            func.similarity(query, Person.surname),
        ).label("similarity_score")

        person_query = (
            self.session.query(Person)
            .filter(Person.hidden.isnot(True))
            .filter(Person.cancelled.isnot(True))
            .filter(similarity_score > 0.3)
        )

        # Limit to only show the required institution
        if institutions is not None:
            person_query = person_query.join(Person.institutions).filter(
                Institution.instid.in_(institutions)
            )

        meta["total"] = person_query.count()

        # return in specified order
        if sort == "surname":
            person_query = person_query.order_by(Person.surname)
        elif sort == "crsid":
            person_query = person_query.order_by(Person.crsid)
        elif sort == "display_name":
            person_query = person_query.order_by(Person.display_name)
        else:
            # fall back to ranked results
            person_query = person_query.order_by(similarity_score.desc())

        if offset is None:
            offset = 0  # set start position at beginning anyway

        meta["offset"] = offset

        if limit is None:
            person_query = person_query.offset(offset)
        else:  # value for limit set
            person_query = person_query.slice(offset, offset + limit)

        meta["limit"] = limit

        return person_query.all(), meta

    def get_people(
        self,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        sort: Optional[str] = None,
        institutions: Optional[List[str]] = None,
        hidden: bool = False,
        cancelled: bool = False,
    ) -> Tuple[List[Person], Dict[str, Any]]:
        """
        Get a list of people that match the given criteria, using exact
        filtering search.
        """

        meta: Dict[str, Any] = {"query": query, "fuzzy": False, "sort": sort}

        person_query = self.session.query(Person)

        # Should only be used for internal calls, not exposed to the API
        if not hidden:
            person_query = person_query.filter(Person.hidden.isnot(True))
        if not cancelled:
            person_query = person_query.filter(Person.cancelled.isnot(True))

        # filter to only the results that we require
        if query is not None:
            person_query = person_query.filter(
                or_(
                    Person.identifier.ilike("%{}%".format(query)),  # crsid
                    Person.display_name.ilike("%{}%".format(query)),
                    Person.registered_name.ilike("%{}%".format(query)),
                    Person.first_name.ilike("%{}%".format(query)),
                    Person.surname.ilike("%{}%".format(query)),
                )
            )

        # Limit to only show the required institutions
        if institutions is not None:
            person_query = person_query.join(Person.institutions).filter(
                Institution.instid.in_(institutions)
            )

        meta["total"] = person_query.count()

        # return in specified order
        if sort == "crsid":
            person_query = person_query.order_by(Person.crsid)
        elif sort == "display_name":
            person_query = person_query.order_by(Person.display_name)
        else:
            # fall back to sorting by surname results
            person_query = person_query.order_by(Person.surname)

        if offset is None:
            offset = 0  # set start position at beginning anyway

        meta["offset"] = offset

        if limit is None:
            person_query = person_query.offset(offset)
        else:  # value for limit set
            person_query = person_query.slice(offset, offset + limit)

        meta["limit"] = limit

        return person_query.all(), meta

    def get_rooms(
        self,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        buildings: Optional[List[str]] = None,
    ) -> Tuple[List[Room], Dict[str, Any]]:
        """
        List all rooms that match a query.
        """
        rooms_query = self.session.query(Room)
        meta = {"query": query}

        # Simple case-insensitive name matching
        if query is None:
            rooms_query = rooms_query.filter(Room.building != "External")
        if query is not None:
            for word in query.split():
                rooms_query = rooms_query.filter(
                    or_(
                        Room.number.ilike("%{}%".format(word)),
                        Room.building.ilike("%{}%".format(word)),
                        Room.description.ilike("%{}%".format(word)),
                    )
                )

        # Only showing results for the requested building
        if buildings is not None:
            rooms_query = rooms_query.filter(Room.building.in_(buildings))

        meta["total"] = rooms_query.count()

        # alphabetical order
        rooms_query = rooms_query.order_by(
            case({"External": "ZZ"}, value=Room.building, else_=Room.building),
            Room.number,
        )

        if offset is None:
            offset = 0  # set start position at beginning anyway

        meta["offset"] = offset

        if limit is None:
            rooms_query = rooms_query.offset(offset)
        else:
            rooms_query = rooms_query.slice(offset, offset + limit)

        meta["limit"] = limit

        return rooms_query.all(), meta

    def get_room(
        self,
        identifier=None,
        number=None,
        building=None,
        description=None,
        floor=None,
        longitude=None,
        latitude=None,
        create=False,
    ):
        """
        Find a room or create one if necessary.
        """
        if identifier is not None:
            exact_room = self.session.query(Room).get(identifier)
            return exact_room

        query_room = (
            self.session.query(Room)
            .filter(Room.building == building)
            .filter(Room.number == number)
            .first()
        )

        if query_room is None and create is True:
            new_room = Room(
                number=number,
                building=building,
                description=description,
                floor=floor,
                longitude=longitude,
                latitude=latitude,
            )
            self.session.add(new_room)
            self.session.commit()
            return new_room
        else:
            return query_room

    def update_room(
        self,
        identifier,
        building=None,
        number=None,
        description=None,
        floor=None,
        longitude=None,
        latitude=None,
        create=False,
    ):
        """
        Update the data of a room, arguments are the fields of the room
        object: ``building``, ``number`` and ``description``.
        """
        db_room = self.session.query(Room).get(identifier)

        if not db_room and create is True:
            if not number:
                return None  # no blank tags
            else:
                db_room = Room(number, building, description)
        elif not db_room:
            return None

        if number:
            db_room.number = number

        if building:
            db_room.building = building

        if description:
            db_room.description = description

        if floor:
            db_room.floor = floor

        if longitude:
            db_room.longitude = longitude

        if latitude:
            db_room.latitude = latitude

        self.session.add(db_room)
        self.session.commit()

        return db_room

    def delete_room(self, identifier):
        """
        Remove a room from the database completely.
        """
        if not identifier:
            return None

        db_room = self.session.query(Room).get(identifier)

        if db_room is not None:
            self.session.delete(db_room)
            self.session.commit()

        return db_room

    def get_room_people(self, identifier, limit=None, offset=None, sort=None):
        """
        Find all people that are in a given room
        """
        db_room = self.session.query(Room).get(identifier)

        # identifier as common url_for parameter
        meta = {"identifier": identifier, "limit": limit}

        if offset is None:
            offset = 0

        meta["offset"] = offset

        if db_room is None:  # not a vaild room
            meta["total"] = 0
            return [], meta

        # identification for the room
        meta.update(db_room.to_dict(max_depth=0))  # no relationships

        meta["total"] = len(db_room.people)

        # return in specified order
        if sort == "crsid":

            def sort_key(x):
                return x.crsid or "\uFFFF"

        elif sort == "display_name":

            def sort_key(x):
                return x.display_name or "\uFFFF"

        else:

            def sort_key(x):
                return x.surname or "\uFFFF"

        ordered_people = sorted(db_room.people, key=sort_key)

        if limit is None:
            return ordered_people[offset:], meta
        else:
            return ordered_people[offset : limit + offset], meta

    def get_building_rooms(self, name, query=None, limit=None, offset=None):
        """
        Find all rooms that are in a particular building.

        """
        rooms_query = self.session.query(Room).filter(Room.building == name)

        # identifier as common url_for parameter
        meta = {"name": name, "limit": limit, "query": query}

        if query is not None:
            rooms_query = rooms_query.filter(
                or_(
                    Room.number.ilike("%{}%".format(query)),
                    Room.building.ilike("%{}%".format(query)),
                    Room.description.ilike("%{}%".format(query)),
                )
            )

        if offset is None:
            offset = 0

        meta["offset"] = offset

        if rooms_query is None:  # non existent building
            meta["total"] = 0
            return [], meta

        meta["total"] = rooms_query.count()

        # Numerical orderings
        rooms_query = rooms_query.order_by(Room.number)

        if limit is None:
            rooms_query.offset(offset)
        else:
            rooms_query = rooms_query.slice(offset, offset + limit)

        return rooms_query.all(), meta

    def get_building_people(self, name, limit=None, offset=None, sort=None):
        """
        Find all people that have rooms in a particular building.

        """
        db_people = self.session.query(Person).filter(Person.rooms.any(building=name))

        # identifier as common url_for parameter
        meta = {"name": name, "limit": limit}

        if offset is None:
            offset = 0

        meta["offset"] = offset

        if db_people is None:  # non existent building
            meta["total"] = 0
            return [], meta

        meta["total"] = db_people.count()

        # return in specified order
        if sort == "crsid":
            db_people = db_people.order_by(Person.crsid)
        elif sort == "display_name":
            db_people = db_people.order_by(Person.display_name)
        else:
            # fall back to sorting by surname results
            db_people = db_people.order_by(Person.surname)

        if limit is None:
            db_people.offset(offset)
        else:
            db_people = db_people.slice(offset, offset + limit)

        return db_people.all(), meta

    def get_tags(
        self,
        query: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        sort: Optional[str] = None,
        institutions: Optional[List[str]] = None,
    ) -> Tuple[List[Tag], Dict[str, Union[int, str, None]]]:
        """
        List all tags that fit the criteria.

        Order None for no re-ordering, 'count' to get most frequent tags
        first.
        """

        meta: Dict[str, Union[int, str, None]] = {"query": query, "sort": sort}

        tags_query = self.session.query(Tag).filter(Tag.people_count > 0).group_by(Tag)

        if institutions is not None:
            # The group_by is required for count and slices to work
            tags_query = tags_query.join(Tag.people, Person.institutions).filter(
                Institution.instid.in_(institutions)
            )

        if sort == "count":
            tags_query = tags_query.order_by(Tag.people_count.desc())
        elif sort == "new":
            # Assume id is an increasing value and
            tags_query = tags_query.order_by(Tag.identifier.desc())
        elif sort == "trending":
            tags_query = tags_query.order_by(Tag.modified.desc())
        else:
            tags_query = tags_query.order_by(Tag.name)

        # search for the specified text in any text field
        if query is not None:
            tags_query = tags_query.filter(
                or_(
                    Tag.name.ilike("%{}%".format(query)),
                    Tag.description.ilike("%{}%".format(query)),
                )
            )

        meta["total"] = tags_query.count()

        if offset is None:
            offset = 0  # set start position at beginning anyway

        meta["offset"] = offset

        if limit is None:
            tags_query = tags_query.offset(offset)
        else:
            tags_query = tags_query.slice(offset, offset + limit)

        meta["limit"] = limit

        return tags_query.all(), meta

    def get_tag(
        self, name: str, description: Optional[str] = None, create: bool = False
    ) -> Optional[Tag]:
        """
        Find a tag by the tag name, or create one if necessary.

        Parameters
        ----------
        name
            The text name of the tag
        description
            The extra text describing the tag, only required to create
            a new tag.
        create
            If True, a new tag will be created if one does not exist.
        """
        if not name:  # don't trip on empty value
            return None
        db_tag = self.session.query(Tag).filter(Tag.name == name).one_or_none()
        if not db_tag and create is True:
            new_tag = Tag(name, description)
            self.session.add(new_tag)
            self.session.commit()
            return new_tag
        else:
            return db_tag

    def get_tag_by_identifier(self, identifier: int) -> Optional[Tag]:
        """
        Retrieve a specific tag using the database identifier.

        Parameters
        ----------
        identifier
            Database identifier field

        Returns
        -------
        tag
            The Tag object if it exists, otherwise None
        """
        return self.session.query(Tag).get(identifier)

    def update_tag(self, tag, name=None, description=None, create=False):
        """
        Update the data of a tag, arguments are the fields of the tag
        object: ``name`` and ``description``.
        """
        db_tag = self.session.query(Tag).filter(Tag.name == tag).one_or_none()

        if not db_tag and create is True:
            if not name:
                return None  # no blank tags
            else:
                db_tag = Tag(name, description)
        elif not db_tag:
            return None

        if name:
            db_tag.name = name

        if description:
            db_tag.description = description

        try:
            self.session.add(db_tag)
            self.session.commit()
        except IntegrityError:
            self.session.rollback()
            raise KeyError("Tag with that name already exists")

        return db_tag

    def sync_tag_count(self, identifier: int):
        """
        Update the member count of a tag and ensure it's consistent
        with the apparent number of members.

        Parameters
        ----------
        identifier
            Database identifier of the tag

        """
        tag = self.session.query(Tag).get(identifier)
        tag.people_count = len(tag.people)
        self.session.add(tag)
        self.session.commit()

    def sync_all_tags(self, delete_orphans: bool = True) -> None:
        """
        Update the member count of all tags, delete any that are orphaned.

        Should be run regularly to catch tags that don't have any non-cancelled
        members any more.
        """
        for tag in self.session.query(Tag):
            tag.people_count = len(tag.people)
            if tag.people_count == 0 and delete_orphans:
                self.session.query(tag_associations).filter(
                    tag_associations.c.tag_identifier == tag.identifier
                ).delete(synchronize_session=False)
                self.session.delete(tag)
            else:
                self.session.add(tag)

        self.session.commit()

    def get_tag_people(
        self,
        tag: str,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        sort: Optional[str] = None,
        institutions: Optional[List[str]] = None,
    ) -> Tuple[List[Person], Dict[str, Any]]:
        """
        Find all people that have the given tag.

        Parameters
        ----------
        tag
            The text name of the tag, used to identify it.
        limit
            Maximum number of people to return.
        offset
            Return a subset starting at this result.
        sort
            Whether to sort the results, default is to sort by surname,
            other implemented sorts are `crsid`, `display_name`.
        institutions
            If set, only find people from the given institutions.

        Returns
        -------
        people
            The list of people associated with the tag
        meta
            The metadata for the query performed
        """

        # name as common url_for parameter
        meta: Dict[str, Any] = {"name": tag, "limit": limit}

        person_query = (
            self.session.query(Person)
            .filter(Person.hidden.isnot(True))
            .filter(Person.cancelled.isnot(True))
        )

        # Limit to only show the required institutions
        if institutions is not None:
            person_query = person_query.join(Person.institutions).filter(
                Institution.instid.in_(institutions)
            )

        # Query the tag
        person_query = person_query.join(Person.tags).filter(Tag.name == tag)

        meta["total"] = person_query.count()

        # return in specified order
        if sort == "crsid":
            person_query = person_query.order_by(Person.crsid)
        elif sort == "display_name":
            person_query = person_query.order_by(Person.display_name)
        else:
            # fall back to sorting by surname results
            person_query = person_query.order_by(Person.surname)

        if offset is None:
            offset = 0  # set start position at beginning anyway

        meta["offset"] = offset

        if limit is None:
            person_query = person_query.offset(offset)
        else:  # value for limit set
            person_query = person_query.slice(offset, offset + limit)

        meta["limit"] = limit

        return person_query.all(), meta

    def get_person_suggested_tags(
        self,
        crsid: str,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        sort: Optional[str] = None,
    ) -> Tuple[List[Tuple[Tag, int]], dict]:
        """
        Generate a list of suggested tags for a person that is calculated
        from tags shared with other people. Since the complete list is
        always generated, there is no performance advantage to requesting
        a partial.

        Parameters
        ----------
        crsid
            Identifier of the person for which to generate suggested tags.
        limit
            Maximum number of tags to return.
        offset
            Return a subset starting at this result.
        sort
            Whether to sort the results, default is to sort by weight,
            other implemented sorts are `random`, `weighted-random` and
            `weighted`.

        Returns
        -------
        tags, meta
            A list of tags that match the criteria, and metadata describing
            the tags.

        """

        db_person = self.get_person(crsid)

        if db_person is not None:
            # Compile a list of all the tags that people who share my tags
            # have weighted by how often tags are shared
            tag_counter = Counter(
                [
                    leaf_tag
                    for db_person_tag in db_person.tags
                    for leaf_person in db_person_tag.people
                    if leaf_person != db_person
                    for leaf_tag in leaf_person.tags
                    if leaf_tag not in db_person.tags
                ]
            )
        else:
            # empty list if nothing to suggest
            tag_counter = Counter()

        if offset is None:
            offset = 0

        # name as common url_for parameter
        meta = {
            "name": crsid,
            "limit": limit,
            "offset": offset,
            "total": len(tag_counter),
            "sort": sort,
        }

        # return in specified order
        if sort == "random":
            ordered_tags = list(tag_counter.items())
            shuffle(ordered_tags)

        elif sort == "weighted-random":
            ordered_tags = sorted(
                tag_counter.most_common(), key=lambda item: -item[1] * random()
            )

        else:
            # default, or unknown sort, just use weights
            meta["sort"] = "weight"
            ordered_tags = tag_counter.most_common()

        if limit is None:
            return ordered_tags[offset:], meta
        else:
            return ordered_tags[offset : limit + offset], meta

    def get_status(self, status, create=False):
        """
        Find a status or create one if necessary.
        """
        if not status:  # don't trip on empty value
            return None

        statuses = current_app.config["STATUSES"]

        if hasattr(status, "name"):
            name = status.name
        elif hasattr(status, "keys") and "name" in status:
            name = status["name"]
        else:
            name = status

        if name not in statuses:
            # Invalid value for status
            return None

        db_status = self.session.query(Status).get(name)

        if db_status is None and create is True and name:
            db_status = Status(name)
            self.session.add(db_status)
            self.session.commit()

        return db_status

    def get_institution(
        self, institution: Union[str, Institution, dict], create: bool = False
    ) -> Optional[Institution]:
        """
        Find an institution or create one if necessary. Newly created
        Institutions should have their information synced with lookup.cam
        later.

        Parameters
        ----------
        institution
            Either a string with the institution's id, an Institution object
            or the dict representation of one.
        create
            If True, create an empty Institution (to be updated later).

        Returns
        -------
        institution
            An Institution object from the database.
        """
        if isinstance(institution, Institution):
            return institution
        elif isinstance(institution, dict):
            instid = institution.get("instid", None)
        else:
            instid = institution.strip()

        if not instid:
            return None

        db_institution = self.session.query(Institution).get(instid)

        if db_institution is None and create is True:
            db_institution = Institution(instid)
            self.session.add(db_institution)
            self.session.commit()

        return db_institution

    def get_institutions(self) -> List[Institution]:
        """
        Get all institutions that match the specified criteria.

        Returns
        -------
        institutions
            A list of all the requested institutions
        """
        return self.session.query(Institution).all()
