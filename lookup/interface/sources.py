"""
Import and sync data with external sources.

Data from the lookup API is transformed into nested dict subclasses so that
it can be used directly as a JSON-able object in the database.
"""

import base64
from collections import defaultdict
from typing import Dict, Any, Optional, List, Callable, Iterable, DefaultDict

from ibisclient import IbisPerson, IbisIdentifier, IbisAttribute
from ibisclient import IbisInstitution, IbisGroup
from ibisclient import createConnection, PersonMethods


from lookup.interface import PeopleDatabase
from lookup.model.person import Person, Institution, Status

FETCH_PERSON = "all_attrs,all_insts,all_groups,direct_groups,all_identifiers"
FETCH_ALL_MEMBERS = (
    "all_members,"
    "all_members.all_attrs,"
    "all_members.all_insts,"
    "all_members.all_groups,"
    "all_members.direct_groups,"
    "all_members.all_identifiers"
)

PHONE_TYPES = {
    "instPhone": "Institution number",
    "landlinePhone": "Landline phone number",
    "mobexPhone": "Mobex phone number",
    "mobilePhone": "Mobile phone number",
    "universityPhone": "University phone number",
}


def optional_map(
    function: Callable, iterable: Optional[Iterable[Any]], *iterables: Iterable[Any]
) -> Optional[List]:
    """A map-like function that passes "None" through unchanged and returns a list."""
    if iterable is None:
        return None
    return list(map(function, iterable, *iterables))


def lookup_api_get_crsid(crsid: str) -> IbisPerson:
    """
    Get a person's data from the lookup.cam API.
    """

    # Lookup API
    api = PersonMethods(createConnection())

    person = api.getPerson(scheme="crsid", identifier=crsid, fetch=FETCH_PERSON)

    if person is None:
        raise KeyError("Unknown person {}".format(person))

    return person


class CamIdentifier(dict):
    """It's just a roundabout way of having a CRSid"""

    def __init__(self, identifier: IbisIdentifier) -> None:
        super().__init__(scheme=identifier.scheme, value=identifier.value)


class CamAttribute(dict):
    """
    A generic attribute from lookup.cam from one of several "schemes".

    Depending on the scheme, different members of the object will
    be relevant.
    """

    def __init__(self, attribute: IbisAttribute) -> None:
        super().__init__(
            attrid=attribute.attrid,
            scheme=attribute.scheme,
            value=attribute.value,
            comment=attribute.comment,
            instid=attribute.instid,
            visibility=attribute.visibility,
            effectiveFrom=attribute.effectiveFrom,
            effectiveTo=attribute.effectiveTo,
            owningGroupid=attribute.owningGroupid,
        )
        # Convert bytes to base64 so that json encoder
        # can cope with the data.
        if attribute.binaryData is None:
            self["binaryData"] = None
        else:
            self["binaryData"] = base64.b64encode(attribute.binaryData).decode("utf-8")

    def has_changed(self, previous: "CamAttribute") -> bool:
        """Check if any value-type properties have changed."""
        for key in ["scheme", "value", "comment", "binaryData"]:
            if self[key] != previous[key]:
                return True
        return False


class CamInstitution(dict):
    """
    An Institution in lookup.cam, like PHY - Department of Physics.

    Most of the attributes ignored as they are assumed to have not
    been fetched and are not useful for updating a person's properties.
    """

    def __init__(self, institution: IbisInstitution) -> None:
        # All other attributes are lists of fetched objects,
        # and not required here, so skipped.
        super().__init__(
            cancelled=institution.cancelled,
            instid=institution.instid,
            name=institution.name,
            acronym=institution.acronym,
        )


class CamGroup(dict):
    """
    A lookup group, e.g. for memberships and permissions stuff.

    Most of the attributes ignored as they are assumed to have not
    been fetched and are not currently used for updating a person's
    properties. A group-like system will be implemented in the future.
    """

    def __init__(self, group: IbisGroup) -> None:
        # All other attributes are lists of fetched objects,
        # and not required here, so skipped.
        super().__init__(
            cancelled=group.cancelled,
            groupid=group.groupid,
            name=group.name,
            title=group.title,
            description=group.description,
            email=group.email,
        )


class CamPerson(dict):
    """
    A person from lookup.

    Any missing fetched lists will be None so they can be ignored if missing
    rather than presumed empty. For best results ensure that you fetch
    everything: "all_attrs,all_insts,all_groups,direct_groups,all_identifiers".
    """

    def __init__(self, person: IbisPerson) -> None:
        super().__init__(
            cancelled=person.cancelled,
            identifier=CamIdentifier(person.identifier),
            displayName=person.displayName,
            registeredName=person.registeredName,
            surname=person.surname,
            visibleName=person.visibleName,
            misAffiliation=person.misAffiliation,
            identifiers=optional_map(CamIdentifier, person.identifiers),
            attributes=optional_map(CamAttribute, person.attributes),
            institutions=optional_map(CamInstitution, person.institutions),
            groups=optional_map(CamGroup, person.groups),
            directGroups=optional_map(CamGroup, person.directGroups),
            id=person.id,
        )
        # Some attributes represent single values, so convert them
        # to members here. (Remove if added to API).
        first_name = None
        for attribute in self["attributes"]:
            if attribute["scheme"] == "firstName":
                first_name = attribute["value"]
        self["firstName"] = first_name


def attribute_lists(
    attribute: CamAttribute, collection: DefaultDict[str, list]
) -> None:
    """
    Process an attribute based on its scheme and add it to the corresponding list.

    Parameters
    ----------
    attribute
        An attribute from the lookup API converted to a dict-like object.
        Must contain the "scheme" key and the appropriate "value".
    collection
        A dictionary for the attribute lists. Will append to the relevant key,
        so use a defaultdict that will initialise lists for new keys.
    """

    if attribute["scheme"] == "address":
        collection["addresses"].append(
            {"address": attribute["value"], "description": attribute["comment"]}
        )
    elif attribute["scheme"] == "email":
        collection["email_addresses"].append(
            {"address": attribute["value"], "description": attribute["comment"]}
        )
    elif attribute["scheme"] == "departingEmail":
        collection["email_addresses"].append(
            {
                "address": attribute["value"],
                "description": attribute["comment"] or "Departing Email",
            }
        )
    elif attribute["scheme"] in PHONE_TYPES:
        collection["telephone_numbers"].append(
            {
                "number": attribute["value"],
                "description": attribute["comment"] or PHONE_TYPES[attribute["scheme"]],
            }
        )
    elif attribute["scheme"] == "jpegPhoto":
        collection["photos"].append(attribute)
    elif attribute["scheme"] == "labeledURI":
        collection["urls"].append(
            {"url": attribute["value"], "description": attribute["comment"]}
        )
    elif attribute["scheme"] == "tag":
        collection["tags"].append(attribute["value"])
    elif attribute["scheme"] == "title":
        collection["titles"].append(attribute["value"])
    else:
        # "manager" is ignored as we require confirmation of the relationship
        # "jdCollege", "jdInstid" not useful
        # "displayName", "misAffiliation", "registeredName", "surname",
        # "firstName", "jdRegistration" are processed elsewhere
        pass


class PeopleDatabaseSources(PeopleDatabase):
    """Database including methods to pull in data from remote sources."""

    def add_from_lookup(self, crsid: str):
        """
        Create a new person from their lookup.cam data.

        Parameters
        ----------
        crsid
            The CRSid of the person to be created.

        Returns
        -------
        new_person
            The newly created person

        Raises
        ------
        ValueError
            If person already exists
        KeyError
            Person does not exist in lookup.cam
        AttributeError
            Person is not a member of a required institution

        """

        # don't do anything if person already exists
        if self.session.query(Person).get(crsid) is not None:
            raise ValueError("Person already exists with CRSid {}".format(crsid))

        # Grab lookup.cam data first, so we don't end up with an
        # empty person. Fails with a KeyError if person doesn't
        # exist (they must be Raven anyway).
        lookup_data = lookup_api_get_crsid(crsid)

        # Fine to create a new person
        person = Person(crsid)
        self.session.add(person)
        self.session.commit()

        # Update with source data
        return self.update_from_lookup(lookup_data)

    def update_from_lookup(
        self,
        ibis_person: IbisPerson,
        create: bool = True,
        changes: Optional[Dict] = None,
    ) -> Person:
        """
        Update the person with any changes in their data as observed in the
        lookup API.

        Parameters
        ----------
        ibis_person
            The raw person data from the lookup API.
        create
            Whether to create a new person if one doesn't already exist.
        changes
            If passed a dictionary, it will be updated with any changes made to
            the person.

        Returns
        -------
        person
            The updated person
        """
        # Throwaway dictionary if it's not requested
        if changes is None:
            changes = {}

        # Dictionary format, the complete record saved on the person
        target = CamPerson(ibis_person)

        person = self.get_person(target["identifier"]["value"], create=create)

        if person is None:
            raise KeyError("Unknown person: {}".format(target))

        # Check institutions and change if required.
        # Always set to current value, regardless of previously scraped data.
        institutions_from = sorted([inst.instid for inst in person.institutions])
        institutions_to = sorted([inst["instid"] for inst in target["institutions"]])
        if institutions_to != institutions_from:
            # Record the change
            changes["institutions"] = {"from": institutions_from, "to": institutions_to}
            self.update_institutions(person, target["institutions"])

        # Check the rest of the values in the scrape to see if they've been
        # changed there and update accordingly.
        # Previous scrape recorded on the user, used for diff
        previous = person.lookup_cam

        # Collect items that have changed for the user
        updated_single_values: Dict[str, Any] = {"lookup_cam": target}
        added_attributes: DefaultDict[str, List] = defaultdict(list)

        # Check cancellation every time
        if target["cancelled"] != person.cancelled:
            updated_single_values["cancelled"] = target["cancelled"]

        # Compare the individual values to see if they've changed
        def update_single(name: str, destination: str) -> None:
            if target[name] != previous.get(name):
                updated_single_values[destination] = target[name]

        update_single("displayName", "display_name")
        update_single("registeredName", "registered_name")
        update_single("firstName", "first_name")
        update_single("surname", "surname")
        update_single("visibleName", "visible_name")

        # Existing attributes
        existing = {
            attribute["attrid"]: attribute
            for attribute in previous.get("attributes", [])
        }

        # Count all photos in case there are multiple modified values
        total_photos = 0
        # Jackdaw Registration information
        jd_registration = None
        for attribute in target["attributes"]:
            # Check for attributes with changed values and new attributes
            if attribute["attrid"] in existing:
                if attribute.has_changed(existing[attribute["attrid"]]):
                    attribute_lists(attribute, added_attributes)
            else:
                attribute_lists(attribute, added_attributes)

            if attribute["scheme"] == "jpegPhoto":
                total_photos += 1
            elif attribute["scheme"] == "jdRegistration":
                jd_registration = attribute["value"]

        # Staff, student and visitor status get set based on lookup
        status_from = sorted([status.name for status in person.status])
        self.set_lookup_status(
            person,
            mis_affiliation=target["misAffiliation"],
            jd_registration=jd_registration,
        )
        status_to = sorted([status.name for status in person.status])
        if status_to != status_from:
            changes["status"] = {"from": status_from, "to": status_to}

        # Photos are single valued here, unlike lookup.cam so just take one
        # if it seems like the main photo has been updated.
        if "photos" in added_attributes:
            # Remove from the multiple value collector
            changed_photos = added_attributes.pop("photos")
            # If all photos have changed take one, otherwise assume it's
            # just a secondary one that's changed and ignore it
            if len(changed_photos) == total_photos:
                updated_single_values["photo"] = changed_photos[0]["binaryData"]

        # The alembic migration will have populated the source field with
        # a placeholder, rather than an empty dict. On the first run after
        # migration, drop any values that would have been synced via ldap
        # to avoid duplication.
        if previous.get("migrated"):
            updated_single_values.pop("display_name", None)
            updated_single_values.pop("surname", None)
            updated_single_values.pop("registered_name", None)
            updated_single_values.pop("photo", None)
            added_attributes.pop("titles", None)
            added_attributes.pop("email_addresses", None)
            added_attributes.pop("telephone_numbers", None)

        # Record what's changed so the calling process can inspect
        changes["updated_single_values"] = updated_single_values
        changes["added_attributes"] = added_attributes

        return self.update_person(
            crsid=person.crsid, **updated_single_values, **added_attributes
        )

    def set_lookup_status(
        self,
        person: Person,
        mis_affiliation: Optional[str],
        jd_registration: Optional[str],
    ) -> List[Status]:
        """
        Staff and Student status set exactly according to lookup.cam rules.

        As of April 2020, use the following rules based on the misAffiliation:
            Cancelled people have no status
            Students have misAffiliation of "student" or "staff,student"
            Otherwise person is staff

        As of August 2020, jd_registration can also explicitly set someone
        as a visitor or staff when no misAffiliation is set.

        Parameters
        ----------
        person
            The Person in the database whose status to update.
        mis_affiliation
            The value from lookup API for the misAffiliation, usually
            a string with the statuses. Can also be None which apparently
            also means staff.
        jd_registration
            The value of the jdRegistration attribute from the lookup
            API. Will be present if there is no misAffiliation

        Returns
        -------
        status
            The updated status list
        """

        # Cancelled people can have no status
        if person.cancelled:
            if person.status:
                person.status = []
                self.session.add(person)
                self.session.commit()
            return person.status

        # The status values are objects in the database
        staff = self.get_status("staff", create=True)
        student = self.get_status("student", create=True)
        visitor = self.get_status("visitor", create=True)

        if not mis_affiliation or "staff" in mis_affiliation:
            if staff not in person.status:
                person.status.append(staff)
        else:
            if staff in person.status:
                person.status.remove(staff)

        if mis_affiliation and "student" in mis_affiliation:
            if student not in person.status:
                person.status.append(student)
        else:
            if student in person.status:
                person.status.remove(student)

        # jdRegistration explicitly sets the value
        # from pre-registration if not otherwise set.
        if jd_registration == "Visitor":
            if visitor not in person.status:
                person.status.append(visitor)
        elif jd_registration == "Staff":
            if staff not in person.status:
                person.status.append(staff)

        self.session.add(person)
        self.session.commit()

        return person.status

    def update_institution_from_lookup(
        self, ibis_institution: IbisInstitution, create: bool = False,
    ) -> Optional[Institution]:
        """
        Update an institution's details from the lookup API.

        Parameters
        ----------
        ibis_institution
            The raw institution object obtained from the lookup API.
        create
            If the institution does not exist, create it.

        Returns
        -------
        institution
            The updated institution, or None if it doesn't exist and will
            not be created.
        """

        db_institution = self.get_institution(ibis_institution.instid, create=create)

        if db_institution is None:
            return None

        db_institution.name = ibis_institution.name
        db_institution.cancelled = ibis_institution.cancelled

        # Save any changes
        self.session.add(db_institution)
        self.session.commit()

        return db_institution
