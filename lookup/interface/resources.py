"""
Non RESTful methods adding to the API. Mixin expects to be subclassed by
lookup.interface.api.Api

"""

import json
from os import path

from lookup.util.geometry import midpoint

# Tolerance for change in coordinates since
# comparing floats stored in the database is bad
DELTA = 1e-12


class MapsMixin:
    def get_merged_map(self, building, floor, maps_dir="."):
        """
        Combine a floorplan in GeoJSON format with
        information from the database.

        Floor must be a string as it is stored as a
        string in the database and 0 leaves empty
        values.
        """
        source_map = path.join(maps_dir, "{}_{}.geojson".format(building, floor))

        if not path.exists(source_map):
            return {"type": "FeatureCollection", "features": []}  # empty map

        with open(source_map) as source_geojson:
            geojson = json.load(source_geojson)

        for feature in geojson["features"]:
            # Only modifying properties
            properties = feature["properties"]
            # Identify the room in the database
            number = properties["number"]
            description = properties["description"]
            # location information
            longitude, latitude = midpoint(feature["geometry"]["coordinates"])
            # query database
            db_room = self.database.get_room(number=number, building=building)

            if db_room is None:
                if number in [None, False, "", "N/A"]:
                    # Not a real room
                    continue  # go to the next feature
                # otherwise room is not in the database, create it
                db_room = self.database.get_room(
                    number=number,
                    building=building,
                    description=description,
                    floor=floor,
                    longitude=longitude,
                    latitude=latitude,
                    create=True,
                )
            elif db_room.floor != floor or any(
                [
                    db_room.longitude - longitude > DELTA,
                    db_room.latitude - latitude > DELTA,
                ]
            ):
                # Update room only if something has changed; expensive!
                db_room = self.database.update_room(
                    db_room.identifier,
                    floor=floor,
                    longitude=longitude,
                    latitude=latitude,
                )

            # Description has been edited in the database
            if db_room.description and db_room.description != description:
                properties["description"] = db_room.description

            # Link to the room in the database
            properties["identifier"] = db_room.identifier

            # Add other interesting properties
            properties["people"] = db_room.to_dict()["people"]

        return geojson
