"""
Interacting with the database.
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from lookup.model import Base


class LookupDatabase:
    """Connection to the database."""

    def __init__(self, db_url: str):
        # Properties of the database
        self.url = db_url
        self.base = Base
        # Engine manages connection to the database
        self.engine = create_engine(db_url, pool_size=4, max_overflow=4)
        # Bind a session instance to the instance -- easier to query itself
        self.Session = scoped_session(sessionmaker(bind=self.engine, autoflush=True))

    def __del__(self):
        # Stop lingering connections when using database
        self.Session.remove()

    def initialise(self):
        """Populate some data for a testing setup."""
        # Create the tables when starting from scratch
        Base.metadata.create_all(self.engine)
        # Require pg_trgm so it can be assumed to exist in all cases
        self.Session().execute("CREATE EXTENSION IF NOT EXISTS pg_trgm")
