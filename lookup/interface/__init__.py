"""
Top level interfaces to the database and import helpers.
"""

from flask import Flask

from lookup.interface.base import LookupDatabase
from lookup.interface.people import PeopleDatabase
from lookup.interface.sources import PeopleDatabaseSources
from lookup.interface.tokens import TokenDatabase
from lookup.interface.users import UserDatabase


def config_database(app: Flask) -> Flask:
    app.config["database"] = LookupDatabase(app.config["LOOKUP_DB"])
    return app
