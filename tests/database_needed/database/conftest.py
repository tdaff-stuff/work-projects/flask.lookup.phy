"""

Configuration for tests requiring a raw database to work.

Uses dbfixtures to create a temporary PostgreSQL database. Database
is destroyed at the end of the tests.

Defines:

- A ``database`` which is the python wrapper around SQLAlchemy interface.

"""

import pytest

from pytest_postgresql.factories import postgresql_proc

from lookup.interface import LookupDatabase, PeopleDatabase


@pytest.fixture(scope="module")
def database(postgresql_proc: postgresql_proc):
    """
    Create a temporary Postgres database for testing. Using the proc
    factory gives a database with a module level lifetime.
    """
    db_url = "{db.user}@{db.host}:{db.port}".format(db=postgresql_proc)
    db_url = "postgresql+psycopg2://{db_url}/".format(db_url=db_url)
    # Initialise the database and give it to the tests
    database = LookupDatabase(db_url)
    database.initialise()
    yield PeopleDatabase(database)
    # ensure a clean break before it gets destroyed
    database.Session.close()
    del database
