"""
Tests of very simple database functions.

"""


def test_person(database):
    """Simple creation of a person"""
    new_person = database.update_person(
        "test_person", create=True, display_name="Mr Test"
    )
    assert new_person is not None  # success
    assert database.get_person("test_person").display_name == "Mr Test"


def test_person_many(database):
    """Person with a few more attributes"""
    new_person = database.update_person(
        "test_person_2",
        create=True,
        display_name="Mr Test",
        pronouns="he/him",
        surname="Test",
        rooms=[{"building": "Bragg", "number": "123"}],
        tags=["tag1", "tag2"],
    )
    assert new_person is not None  # success
    new_person_db = database.get_person("test_person_2")
    assert new_person_db.crsid == "test_person_2"
    assert new_person_db.pronouns == "he/him"
    assert new_person_db.surname == "Test"
    assert len(new_person_db.tags) == 2
    assert len(new_person_db.rooms) == 1
