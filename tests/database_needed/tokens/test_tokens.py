"""
Test of token management.
"""

import pytest


def test_create_token(database):
    """Simple create token"""
    in_data = {"key": "value", "another": [1, 2, 3, 4]}
    key = database.generate_token(in_data)
    assert key is not None  # created


def test_token_retrieval(database):
    """Extract data from the token"""
    in_data = {"key": "value", "another": [1, 2, 3, 4]}
    key = database.generate_token(in_data)
    out_data = database.get_value(key)
    assert out_data == in_data


def test_token_expiry(database):
    in_data = {"key": "value", "another": [1, 2, 3, 4]}
    key = database.generate_token(in_data, max_uses=2)
    # Once
    out_data = database.get_value(key)
    assert out_data == in_data
    # Twice
    out_data = database.get_value(key)
    assert out_data == in_data
    # Expired
    with pytest.raises(KeyError):
        _gone = database.get_value(key)


def test_manual_expiry(database):
    in_data = {"key": "value", "another": [1, 2, 3, 4]}
    key = database.generate_token(in_data, max_uses=None, valid_hours=None)
    # Token active
    out_data = database.get_value(key)
    assert out_data == in_data
    # Manual expiry
    assert database.expire_token(key) == 1
    with pytest.raises(KeyError):
        _gone = database.get_value(key)
