"""
Configuration for tests that require a flask app
(including those that need a database).

Defines:

- An ``app`` for use with flask tests, with an initialised database.

"""
import pytest

from pytest_postgresql.factories import postgresql_proc

from lookup.config import defaults
from lookup.__main__ import create_app
from lookup.interface.base import LookupDatabase


@pytest.fixture
def app(postgresql_proc: postgresql_proc):
    """
    Fixture required by pytest-flask that provides
    the main app interface to any classes that require it.
    """
    db_url = "{db.user}@{db.host}:{db.port}".format(db=postgresql_proc)
    db_url = "postgresql+psycopg2://{db_url}/".format(db_url=db_url)
    # Initialise the database, then destroy that interface
    database = LookupDatabase(db_url)
    database.initialise()
    del database
    # patch the default URL used to connect as it's too
    # late to use environment variables
    defaults.LOOKUP_DB = db_url
    flask_app = create_app()
    flask_app.debug = True
    yield flask_app
