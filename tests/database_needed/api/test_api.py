"""Test API as sevred by flask."""

from flask import url_for

# All tests use flask app fixture, defined in conftest.py


def test_up(app, client):
    """Test that flask app is able to run."""
    test_url = url_for("web.index")
    res = client.get(test_url)
    assert res.status_code == 200


def test_404_error(app, client):
    """Non existing URL."""
    test_url = "nothing_here"
    res = client.get(test_url)
    assert res.status_code == 404


def test_people_get(app, client):
    """Simple get request, no data."""
    login_url = url_for("web.login")
    login = client.get(login_url, headers={"X-AAPrincipal": "abc123"})
    test_url = url_for("api.people_get")
    res = client.get(test_url)
    assert res.status_code == 200
    assert res.mimetype == "application/json"
