"""
Session lifetime fixtures.

Fixtures that apply globally to all tests:
* Celery is neutered here to send tasks to a sinkhole.
"""

from celery import current_app

import pytest


@pytest.fixture(scope="session", autouse=True)
def celery_skip() -> None:
    """
    Force the current celery instance to send tasks to a memory
    based broker, i.e. nowhere. This is to prevent stuff like the
    sync tasks hanging when updating tag lists.

    This applies before a testing session begins and kills any way
    of using the tasks during that session. Good luck to anyone that
    gets around to implementing testing with functional Celery tasks.
    """
    current_app.conf.broker_url = "memory://"
