"""
Parsing the common arguments from a request.

"""

from flask import Request

from lookup.util.web import get_common_args


def test_all_args():
    """Values provided for all arguments."""
    req = Request(
        {"QUERY_STRING": "query=test&limit=7&offset=13" "&full=t&recursive=false"}
    )

    args = get_common_args(
        req, query="default", limit=10, offset=300, full=False, recursive=True
    )

    assert args["query"] == "test"
    assert args["limit"] == 7
    assert args["offset"] == 13
    assert args["full"] is True
    assert args["recursive"] is False


def test_no_args():
    """Empty query string should be None for everything."""
    req = Request({"QUERY_STRING": ""})
    args = get_common_args(req)

    assert args["query"] is None
    assert args["limit"] is None
    assert args["offset"] is None
    assert args["full"] is None
    assert args["recursive"] is None


def test_defaults():
    """Use default values if not in query string."""
    """Values provided for all arguments."""
    req = Request({"QUERY_STRING": ""})

    args = get_common_args(
        req, query="default", limit=10, offset=300, full=False, recursive=True
    )

    assert args["query"] == "default"
    assert args["limit"] == 10
    assert args["offset"] == 300
    assert args["full"] is False
    assert args["recursive"] is True


def test_invalid_args():
    """Args that are the wrong type are ignored."""
    req = Request({"QUERY_STRING": "limit=xxx&offset=xxx" "&full=xxx&recursive=xxx"})

    args = get_common_args(req)

    assert args["limit"] is None
    assert args["offset"] is None
    assert args["full"] is True
    assert args["recursive"] is True
