"""
Test generation of pages from query data.

"""

from lookup.util.web import generate_pages, pagination_args


def test_pagination():
    """Don't error with valid inputs."""
    meta = {"query": "txt", "limit": 10, "offset": 40, "total": 557}

    pagination = generate_pages(meta)
    assert "all_pages" in pagination
    assert pagination["active_page"] == 5


def test_no_offset():
    """Offset is 0 or None. first and prev not included."""
    meta = {"query": "txt", "limit": 10, "total": 557}

    pagination = generate_pages(meta)
    assert "prev" not in pagination
    assert "first" not in pagination


def test_no_limit():
    """Limit not defined, just give link to first page."""
    meta = {"query": "txt", "offset": 30, "limit": 0, "total": 557}

    pagination = generate_pages(meta)
    assert pagination["first"] == 0
    assert not pagination["limit"]


def test_zero_total():
    """Don't do pages for 0 results"""
    meta = {"query": "txt", "offset": 0, "limit": 10, "total": 0}

    pagination = generate_pages(meta)
    assert pagination["all_pages"] == [(1, 0)]
    assert "first" not in pagination
    assert "prev" not in pagination
    assert "next" not in pagination
    assert "last" not in pagination


def test_args_trimmer():
    """Pass only required args"""
    in_map = {
        "name": "testname",
        "identifier": 12,
        "query": None,
        "offset": 0,
        "fuzzy": False,
        "ignored": True,
    }

    out_map = pagination_args(in_map)

    assert out_map["name"] == "testname"
    assert out_map["identifier"] == 12
    assert "query" not in out_map
    assert out_map["offset"] == 0
    assert "fuzzy" not in out_map
    assert "ignored" not in out_map


def test_args_trimmer_kwargs():
    """Override with kwargs"""
    in_map = {
        "name": "testname",
        "identifier": 12,
        "query": None,
        "offset": 0,
        "fuzzy": False,
        "ignored": True,
    }

    out_map = pagination_args(in_map, name="new", identifier=None, fuzzy=True)

    assert out_map["name"] == "new"
    assert "identifier" not in out_map
    assert "query" not in out_map
    assert out_map["offset"] == 0
    assert out_map["fuzzy"] is True
    assert "ignored" not in out_map
