"""
Parsing the remote user from headers.

"""

from flask import Request

from lookup.util.web import get_remote_user


def test_x_aaprincipal():
    """Default header from raven authentication"""
    req = Request({"HTTP_X_AAPRINCIPAL": "hashhash user123"})
    assert get_remote_user(req) == "user123"


def test_x_aaprincipal_nokey():
    """Default header from raven authentication (no header key)."""
    req = Request({"HTTP_X_AAPRINCIPAL": "user456"})
    assert get_remote_user(req) == "user456"


def test_no_user():
    """No headers found."""
    req = Request({})
    assert get_remote_user(req) is None
