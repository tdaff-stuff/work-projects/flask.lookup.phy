"""
Functions for processing text.

"""

import pytest

from lookup.util.text import string_to_bool


def test_string_to_bool_strict():
    """Interpreting booleans in URI"""
    cases = {
        "true": True,
        "True": True,
        "T": True,
        "1": True,
        "yes": True,
        "on": True,
        True: True,
        1: True,
        "false": False,
        "False": False,
        "F": False,
        "0": False,
        "no": False,
        "off": False,
        False: False,
        0: False,
    }

    for text, result in cases.items():
        assert string_to_bool(text, strict=True) == result


def test_string_to_bool_strict_failures():
    """Cases where a value cannot be converted."""
    failures = ["not", 3, {"no": "no"}]
    for text in failures:
        with pytest.raises(ValueError):
            string_to_bool(text, strict=True)


def test_string_to_bool_no_strict():
    """Interpreting booleans in URI"""
    cases = {
        "true": True,
        "True": True,
        "T": True,
        "1": True,
        "yes": True,
        "on": True,
        True: True,
        1: True,
        "non-empty": True,
        "false": False,
        "False": False,
        "F": False,
        "0": False,
        "no": False,
        "off": False,
        False: False,
        0: False,
        "": False,
    }

    for text, result in cases.items():
        assert string_to_bool(text, strict=False) == result
