#!/usr/bin/env python
"""
Run the server behind a profiler reporting the most expensive
functions from each request.
"""

from werkzeug.middleware.profiler import ProfilerMiddleware
from lookup.http.app import create_app

app = create_app()
app.config["PROFILE"] = True
app.wsgi_app = ProfilerMiddleware(
    app.wsgi_app, sort_by=("cumtime", "time"), restrictions=[30]
)
app.run(debug=True, port=5001)
