`lookup.phy <https://lookup.phy.cam.ac.uk>`_
============================================

How do we find people in the Cavendish Laboratory with particular skills
and interests? It might be an area of research, expertise in a
particular piece of equipment, ability in a language, or even an
interest such as cycling or yoga. This problem of 'knowledge discovery'
was discussed earlier this year, recognising that benefits accrue from
being able to share our collective talents and knowledge.

`lookup.phy <https://lookup.phy.cam.ac.uk>`_ is a search engine for
people within the Cavendish. It uses the concept of 'tags' to enable
people to record their skills and interests, and become searchable based
on these. We want lookup.phy to facilitate collaboration through
knowledge and skill discovery, helping to create communities of
interest. In future, we hope that tags can be used to auto-generate
mailing lists, helping communications within the Cavendish to become
more targeted and relevant.

The system also supports fuzzy search and some semantic inference. You
shouldn't have to know the exact spelling of someone's name to find
them. Search should continue to improve over time.

`lookup.phy <https://lookup.phy.cam.ac.uk>`_ also contains location
information. You can search for rooms, and - subject to the data
existing in the system - see who is associated with that room and a
map of the location.


Implementation
--------------

The current software stack consists of:

* Python 3
* flask
* sqlalchemy
* PostgreSQL
* Celery

And is hosted on the Physics Department's code repository
`codeshare.phy <https://codeshare.phy.cam.ac.uk/itservices.phy/lookup.phy/flask.lookup.phy>`_.
