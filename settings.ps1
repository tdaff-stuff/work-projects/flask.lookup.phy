$env:FLASK_APP="lookup.http.app"
$env:LOOKUP_SETTINGS="$PSScriptRoot\settings.cfg"
$env:LOOKUP_SECRETS="$PSScriptRoot\secrets.cfg"
Write-Host "LOOKUP_SETTINGS=$env:LOOKUP_SETTINGS"
Write-Host "LOOKUP_SECRETS=$env:LOOKUP_SECRETS"
if (Test-Path "$PSScriptRoot\venv\Scripts\Activate.ps1" -Type Leaf)
{
    &$PSScriptRoot\venv\Scripts\Activate.ps1
    Write-Host "VIRTUAL_ENV=$env:VIRTUAL_ENV"
}
elseif (Test-Path "$PSScriptRoot\.venv\Scripts\Activate.ps1" -Type Leaf )
{
    &$PSScriptRoot\.venv\Scripts\Activate.ps1
    Write-Host "VIRTUAL_ENV=$env:VIRTUAL_ENV"
}