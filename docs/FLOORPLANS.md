# Creating Floorplans

 1.  Open PDF in GIMP, 600 ppi =  9000 x 7000 pixels for Bragg
 2.  Crop layers, transparent background
 3.  Export png
 4.  QGIS -> OpenLayers Plugin -> OpenStreetMap
 6.  Raster -> Georeferencer
 7.  Import Raster
 8.  Pick points on raster and choose "From map canvas"
 9.  Save GCP points
 10. Transform Settings
   - Polynomial 2
   - Cubic Spline
   - Target pseudo mercator
 11. Start georeferencing
 13. Add vector polygon layer from existing geojson file
 14. Activate advanced digitizing panel
 15. Draw rooms as polygons
   - name = room number
   - description = description
   - type = room, corridor
 16. Do the same for line layers (doors)
 17. Do the same for marker layers (points of interest)